<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href='Assets/fullcalendar/lib/main.css' rel='stylesheet' />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script type="text/javascript" src="https://checkout.wompi.co/widget.js"></script>

</head>

<body>
    <style>
        body {
            margin: 40px 10px;
            padding: 0;
            font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
            font-size: 14px;
        }

        #loading {
            display: none;
            position: absolute;
            top: 10px;
            right: 10px;
        }

        #calendar {
            max-width: 1100px;
            margin: 0 auto;
        }
    </style>


    <div id="app">

        <div id='loading'>Cargando...</div>

        <div id='calendar'></div>

        <div class="modal Mcalendar" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Nuevo agendamiento</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 ">
                                <div class="card text-center">
                                    <div class="card-header">
                                        Fecha
                                    </div>
                                    <div class="card-body float-center ">
                                    <h5 class="card-title"> <span class="badge badge-info">
                                        {{newScheduling.date}}</span></h5>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        Desde
                                    </div>
                                    <div class="card-body float-center ">
                                    <h5 class="card-title"> <span class="badge badge-info">
                                        {{newScheduling.start}}</span></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        Hasta
                                    </div>
                                    <div class="card-body float-center ">
                                    <h5 class="card-title"> <span class="badge badge-info">
                                        {{newScheduling.end}}</span></h5>
                                    </div>
                                </div>
                            </div>

                            <div v-if="newScheduling.product_selected" class="col-md-12 mb-3 mt-3">
                                <img class="img-fluid" :src="newScheduling.product_img" alt="">
                            </div>
                            <div class="col-12">
                                <img v-if="loader" width="50px" src="Assets/dist/img/laoder.gif" alt="" class="">
                                <select @change="setImgProductSelected()" v-if="!loader" v-model="newScheduling.product_selected" name="" class="form-control mb-4" id="">
                                    <option v-for="prod in DBProducts" :value="prod">{{prod.name}}</option>
                                </select>
                                <p v-if="errors.product" class="text-danger mb-3">{{errors.product}}</p>
                            </div>
                            <div class="col-md-6">
                                <label for="">Nombres</label>
                                <input v-model="newScheduling.first_name" type="text" class="form-control">
                                <p v-if="errors.first_name" class="text-danger">{{errors.first_name}}</p>
                            </div>
                            <div class="col-md-6">
                                <label for="">Apellidos</label>
                                <input v-model="newScheduling.last_name" type="text" class="form-control">
                                <p v-if="errors.last_name" class="text-danger">{{errors.last_name}}</p>
                            </div>
                            <div class="col-md-6">
                                <label for="">Correo electrónico</label>
                                <input v-model="newScheduling.email" type="email" class="form-control">
                                
                                <p v-if="errors.email" class="text-danger">{{errors.email}}</p>
                            </div>
                            <div class="col-md-6">
                                <label for="">Teléfono/whatsapp</label>
                                <input v-model="newScheduling.phone" type="number" class="form-control">
                                <p v-if="errors.phone" class="text-danger">{{errors.phone}}</p>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button @click="Save()" type="button" class="btn btn-primary">Guardar y continuar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src='Assets/fullcalendar/lib/main.js'></script>
    <script src='Assets/fullcalendar/lib/locales-all.js'></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src='Assets/Axios.js'></script>
    <script src="app.js"></script>
</body>

</html>
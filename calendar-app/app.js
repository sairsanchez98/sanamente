var app = new Vue({
    el: "#app",
    data: {
        newScheduling : {
            date : "",
            start: "",
            end: "",
            product_selected : [],
            product_img : null,
            first_name: "",
            last_name : "",
            phone: "",
            email:""

        },
        loader : false,
        DBProducts : [],
        errors : [],
        errorSpaces : false
        
    },

    mounted() {
        this.OpenCalendar();
    },

    methods: {
        OpenCalendar(initialDate) {
            let url = "CalendarController.php?Schedule=true";

            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'timeGridDay',
            timeZone : "America/Bogota",
            
            timeFormat: 'h(:mm)A',
            locale: "es",
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            
            initialDate: initialDate ,
            navLinks: true, // can click day/week names to navigate views
            businessHours: true, // display business hours
            editable: false,
            selectable: true,
            events: {
                url: url,
                cache: true
            },
            select: function(arg) {
               

                if(arg.view.type == "dayGridMonth"){
                 console.log(arg);
                 app.OpenCalendar( arg.startStr );
                }else {
                    
                    app.newScheduling.date = arg.startStr.split('T')[0];
                    app.newScheduling.start = arg.startStr.split('T')[1];
                    app.newScheduling.end =  arg.endStr.split('T')[1];
                    
                    app.validateTime(
                        app.newScheduling.end , app.newScheduling.start , app.newScheduling.date
                    );
                        
                    if(!app.errorSpaces){
                        app.loader = true;
                        
                        $(".Mcalendar").modal("show");
                            let urlApp  =  window.location.origin;
                            axios.get("CalendarController.php?AllProducts&urlApp="+urlApp)
                            .then((res)=> {
                                app.DBProducts = res.data;
                                setTimeout(() => app.loader = false, 500);
                        })
                    }
                   
                    
                }

             

                /*if (title) {
                  calendar.addEvent({
                    title: title,
                    start: arg.start,
                    end: arg.end,
                    //allDay: arg.allDay
                  })
                }*/
                calendar.unselect()
              },
            });

            calendar.render();

         
        },


        Save (){
            let formdata = new FormData();
            formdata.append("Schedule" , JSON.stringify(app.newScheduling));
            axios.post("CalendarController.php" , formdata)
            .then((res)=> {
                if (!res.data.errors) {
                    $(".Mcalendar").modal("hide");
                    app.wompi(res.data.id);    
                }else {
                   app.errors= res.data.errors;
                   if (app.errors.Spaces) {

                   }
                }
                
            })
            
        },


        setImgProductSelected(){
            app.newScheduling.product_img = app.newScheduling.product_selected.images[0].src ;
        },

        validateTime(TimeStart, TimeEnd, date){
            
            let formdata = new FormData()
            formdata.append("ValidaSpaces" , JSON.stringify(app.newScheduling));
            axios.post("CalendarController.php" , formdata).then(res=> {
                if(res.data == "false"){
                    $(".Mcalendar").modal("hide");
                    swal({
                        title: "Horario no disponible.",
                        text: "Selecciona un horario que no se cruce con el de alguién más.",
                        icon: "warning",
                        buttons: {
                            cancel: "Aceptar"
                        },
                        dangerMode: true,
                    })
                    
                }
            })


            
            if (TimeStart < "08:30" || TimeStart >= "17:30" ||   TimeEnd >= "17:30") {
                    
                    $(".Mcalendar").modal("hide");
                    swal({
                        title: "Horario no disponible.",
                        text: "Selecciona un horario entre las 08:00 am y 05:00 pm",
                        icon: "warning",
                        buttons: {
                            cancel: "Aceptar"
                        },
                        dangerMode: true,
                    })
                
            }
           

           
        },


        

        wompi(ordernumber){
            
            checkout = new WidgetCheckout({
                currency: 'COP',
                amountInCents: app.newScheduling.product_selected.price + "00",
                reference: app.newScheduling.product_selected.name+ " & "+ Math.random() + " & " + app.newScheduling.first_name ,
                publicKey: 'pub_test_jbsKq4dG9XmlmdoKVkBRne41eM5FeKiY',
                //redirectUrl: 'https://transaction-redirect.wompi.co/check', // Opcional
                taxInCents: { // Opcional
                vat: 0,
                consumption: 800
                },
                customerData: { // Opcional
                    email:app.newScheduling.email,
                    fullName: app.newScheduling.first_name + ' '+ app.newScheduling.last_name,
                    phoneNumber: app.newScheduling.phone,
                    phoneNumberPrefix: '+57',
                    legalId: '0',
                    legalIdType: 'CC'
                }
            })

            checkout.open(function ( result ) {
                var transaction = result.transaction
                let formdata = new FormData();

                formdata.append("ScheduleUpdate" , JSON.stringify(
                    {
                        ordernumber : ordernumber , 
                        status: transaction.status
                    }) )

                formdata.append("newScheduling" , JSON.stringify(app.newScheduling) )
                axios.post("CalendarController.php" , formdata)
                .then((res)=> {
                    console.log('Transaction ID: ', transaction.id)
                    console.log('Transaction object: ', transaction)
                   parent.location.reload();
                })
                
            })
        }

      
        

    }
})
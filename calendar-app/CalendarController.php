<?php
    session_start();
    use Automattic\WooCommerce\Client;   
    require __DIR__ . '/vendor/autoload.php';
    require "CalendarModel.php";
    

    class CalendarController {
        static public function GetSchedule()
        {
            $Results = Calendar::GetSchedule();
            echo json_encode($Results);
        }   

        static public function dataOrder ($data)
        {
            $data = [
                'payment_method' => 'bacs',
                'payment_method_title' => 'Payu',
                'set_paid' => true,
                'billing' => [
                    'first_name' => $data["first_name"],
                    'last_name' => $data["last_name"],
                    'address_1' => 'SIN DIRECCIÓN',
                    'address_2' => '',
                    'city' => 'SIN CIUDAD',
                    'state' => 'SIN DEPARTAMENTO',
                    'postcode' => 'SIN CODIGO POSTAL',
                    'country' => 'SIN PAÍS',
                    'email' => $data["email"],
                    'phone' => $data["phone"]
                ],
                'shipping' => [
                    'first_name' => $data["first_name"],
                    'last_name' => $data["last_name"],
                    'address_1' => 'SIN DIRECCIÓN',
                    'address_2' => '',
                    'city' => 'SIN CIUDAD',
                    'state' => 'SIN DEPARTAMENTO',
                    'postcode' => 'SIN CODIGO POSTAL',
                    'country' => 'SIN PAÍS',
                ],
                'line_items' => [
                    [
                        'product_id' => $data["product_selected"]["id"],
                        'quantity' => 1
                    ]
                ],
                'shipping_lines' => [
                    [
                        'method_id' => 'no',
                        'method_title' => ' ',
                        'total' => '0'
                    ]
                ],
                "status" => "processing"
            ];

            return $data;
            
        }


        static public function validateData($data)
        {
            $errors = [];
            
            if (!isset($data["product_selected"]["id"])) {
                $errors[ "product"] = "Por favor escoja un servicio.";
            }

            if(!preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $data["first_name"]) || empty($data["first_name"])){
                $errors[ "first_name"] = "Este campo es obligatorio y debe contener solo texto.";
            }

            if(!preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/', $data["last_name"]) || empty($data["last_name"])){
                $errors[ "last_name"] = "Este campo es obligatorio y debe contener solo texto.";
            }

        
            if(!filter_var($data["email"], FILTER_VALIDATE_EMAIL)){
                $errors[ "email"] = "Por favor proporcione un email válido";
            }
            
            if(!is_numeric( $data["phone"] ) || strlen($data["phone"] ) !== 10){
                $errors["phone"] = "Por favor proporcione un número de celular válido";
            }



            return $errors;
        }


        static public function ValidateSpaces ($data)
        {
            
            //> valida fechas
            $start = $data["date"]. " ". $data["start"];
            $end =  $data["date"]. " ". $data["end"];
            $validateSpaces = Calendar::ValidateSpaces ($start , $end);
            
            if(count($validateSpaces) == 0){
                return "ok";
            }else {
                return "false";
            }
        }

        static public function SaveSchedule($data)
        {
            $validator = CalendarController::validateData($data);
            if(count ($validator) > 0){
                return ["errors" => $validator];
            }

            $order = CalendarController::Woo()->post("orders"  , CalendarController::dataOrder($data) );
            return $order;
        }

        static public function updateSchedule ($dataOrder , $newScheduling)
        {
            Calendar::SaveSchedule([
                "start" => $newScheduling["date"]. " ". $newScheduling["start"], 
                "end" => $newScheduling["date"]. " ". $newScheduling["end"], 
                "id_product"  =>$newScheduling["product_selected"]["id"],
                "status" => "No disponible", 
                "created" => date("Ymd"),
            ]);

            $status =  $dataOrder->status =="APPROVED" ? "completed" : "failed";
            $order = CalendarController::Woo()->put('orders/'.$dataOrder->ordernumber, ["status" => $status]);

            echo "ok";
        }

        static public function Woo(){
            $woocommerce = new Client(
                $_SESSION["urlApp"], 
                'ck_8018f1a544531a4bb404d055359d2b50fe913a86', 
                'cs_34047c228258bc9ba3bb89aabc1c4ff2a9f00e40',
                [
                    'wp_api' => true,
                    'version' => 'wc/v3',
                ]
            );

            return $woocommerce;
        }

        
    }


    if (isset($_GET["Schedule"]))
    {
        CalendarController::GetSchedule();
    }
    
    if (isset($_POST["Schedule"]))
    { 
        $new = json_decode($_POST["Schedule"] , true);
        $quey = CalendarController::SaveSchedule($new);
        echo json_encode(  $quey, true);
    }

    if (isset($_GET["AllProducts"]) )
    {
        $_SESSION["urlApp"] = $_GET["urlApp"];
        $productos = CalendarController::Woo()->get('products') ;
        echo json_encode($productos, true);
    }

    if(isset($_POST["ScheduleUpdate"]))
    {
        $dataOrder = json_decode($_POST["ScheduleUpdate"] );
        $newScheduling = json_decode($_POST["newScheduling"] , true);
        var_dump($newScheduling);
        CalendarController::updateSchedule($dataOrder , $newScheduling);
    }

    if (isset($_POST["ValidaSpaces"])) {
        $new = json_decode($_POST["ValidaSpaces"] , true);
        $quey = CalendarController::ValidateSpaces($new);
        echo json_encode(  $quey, true);
    }
?>
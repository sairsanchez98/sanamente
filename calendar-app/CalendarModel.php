<?php
require "ConnectionModel.php";
    class Calendar {
        static public function GetSchedule()
        {
            $conn = Conection::DB()->prepare("SELECT *, status as title         
            FROM  calendar WHERE status LIKE 'No disponible' ORDER BY id  DESC");
            if($conn -> execute()){
                return $conn->fetchAll();
            }else{
                return false;
            }
        }

        static public function ValidateSpaces($start , $end)
        {
            $end_ = new DateTime($end);
            $end_ ->add(new DateInterval("P1D"));
            $endmasuno = $end_->format("Y-m-d");
            $stmt = Conection::DB()->prepare("SELECT * FROM calendar 
            WHERE calendar.start BETWEEN '$start' AND '$end' || calendar.end BETWEEN '$start' AND '$end'  ");

            if ($stmt -> execute()) {
                return $stmt -> fetchAll();
            }else{
                die(print_r($stmt->errorInfo(), true));
            }
        }


        static public function SaveSchedule($data)
        {
            $stmt = Conection::DB()->prepare("INSERT INTO calendar 
            (start, end, id_product, status, created) 
            VALUES (:start, :end, :id_product, :status, :created)");
          
            
            $stmt->bindParam(":start", $data["start"], PDO::PARAM_STR);
            $stmt->bindParam(":end", $data["end"], PDO::PARAM_STR);
            $stmt->bindParam(":id_product", $data["id_product"], PDO::PARAM_STR);
            $stmt->bindParam(":status", $data["status"], PDO::PARAM_STR);
            $stmt->bindParam(":created", $data["created"], PDO::PARAM_STR);
            
            if($stmt->execute()){
                return true;
            }else{
                //die(print_r($stmt->errorInfo(), true));
                return false;
            }
        }
    }

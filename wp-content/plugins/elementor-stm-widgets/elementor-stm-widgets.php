<?php
/**
 * Plugin Name: STM Widgets
 * Description: Elementor Widgets by StylemixThemes.
 * Plugin URI:  https://themeforest.net/user/stylemixthemes
 * Version:     1.0.9
 * Author:      StylemixThemes
 * Author URI:  https://themeforest.net/user/stylemixthemes
 * Text Domain: elementor-stm-widgets
 */


if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
define( 'ELEMENTOR_SM_WIDGETS_VERSION', ( WP_DEBUG ) ? time() : '1.0' );
define( 'ELEMENTOR_SM_WIDGETS__FILE__', __FILE__ );
define( 'ELEMENTOR_SM_WIDGETS__DIR__', dirname( __FILE__ ) );
define( 'ELEMENTOR_SM_WIDGETS__PLUGINURL__', plugins_url( '/', ELEMENTOR_SM_WIDGETS__FILE__ ) );
require_once 'inc/add-options.php';
require_once 'inc/elementor-hooks.php';
require_once 'wp-widgets/recent-posts-widget.php';
require_once 'inc/actions/load-more-portfolio.php';
require_once 'importer/importer.php';


add_action( 'woocommerce_loaded', function () {
	if ( class_exists( 'WooCommerce' ) ) {
		require_once 'wp-widgets/wc-filter-by-attribute.php';
	}
} );


/**
 * Load Sm Widgets
 *
 * Load the plugin after Elementor (and other plugins) are loaded.
 *
 * @since 1.0.0
 */
function sm_widgets_load()
{
	// Load localization file
    if (!is_textdomain_loaded('elementor-stm-widgets')) {
        load_plugin_textdomain(
            'elementor-stm-widgets',
            false,
            'languages/'
        );
    }
	// Notice if the Elementor is not active
	if ( !did_action( 'elementor/loaded' ) ) {
		add_action( 'admin_notices', 'sm_widgets_fail_load' );
		return;
	}

	// Check required version
	$elementor_version_required = '1.8.0';
	if ( !version_compare( ELEMENTOR_VERSION, $elementor_version_required, '>=' ) ) {
		add_action( 'admin_notices', 'sm_widgets_fail_load_out_of_date' );
		return;
	}

	// Require the main plugin file
	require( __DIR__ . '/plugin.php' );
}

add_action( 'plugins_loaded', 'sm_widgets_load' );

function sm_widgets_fail_load()
{
	?>
	<div class="notice notice-error">
		<p><?php esc_html_e( 'STM Widgets is not working because Elementor is disabled.', 'elementor-stm-widgets' ) ?></p>
	</div>
	<?php
}

function sm_widgets_fail_load_out_of_date()
{
	if ( !current_user_can( 'update_plugins' ) ) {
		return;
	}

	$file_path = 'elementor/elementor.php';

	$upgrade_link = wp_nonce_url( self_admin_url( 'update.php?action=upgrade-plugin&plugin=' ) . $file_path, 'upgrade-plugin_' . $file_path );
	$message = '<p>' . __( 'STM Widgets is not working because you are using an old version of Elementor.', 'elementor-stm-widgets' ) . '</p>';
	$message .= '<p>' . sprintf( '<a href="%s" class="button-primary">%s</a>', $upgrade_link, __( 'Update Elementor Now', 'elementor-stm-widgets' ) ) . '</p>';

	echo '<div class="error">' . $message . '</div>';
}


// FRONTEND // After Elementor registers all styles.
add_action( 'elementor/frontend/after_register_styles', 'theme_after_frontend' );

function theme_after_frontend()
{
	wp_enqueue_style( 'theme-icons', plugins_url( 'assets/fonts/icons.css', __FILE__ ), array(), '1.0' );
	wp_enqueue_style( 'slick-css', plugins_url( 'assets/css/slick.css', __FILE__ ), array(), '1.8.1' );
	wp_enqueue_style( 'slick-theme-css', plugins_url( 'assets/css/slick-theme.css', __FILE__ ), array(), '1.8.1' );

	// JS
	wp_enqueue_script( 'editor-scripts', plugins_url( 'assets/js/editor_scripts.js', __FILE__ ), array( 'jquery' ), '0.6.30' );
	wp_enqueue_script( 'theme-js', plugins_url( 'assets/js/scripts.js', __FILE__ ), array( 'jquery' ), time() );
	wp_enqueue_script( 'jquery-slick', plugins_url( 'assets/js/slick.min.js', __FILE__ ), array( 'jquery' ), '1.8.1' );

}

// EDITOR // Before the editor scripts enqueuing.
add_action( 'elementor/editor/before_enqueue_scripts', 'theme_before_editor' );

function theme_before_editor()
{
	wp_enqueue_style( 'theme-icons', plugins_url( 'assets/fonts/icons.css', __FILE__ ), array(), '1.0' );
	wp_enqueue_style( 'theme-widgets-style', plugins_url( 'assets/widgets_style.css', __FILE__ ), array(), '1.0' );
	wp_enqueue_style( 'theme-admin-style', plugins_url( 'assets/admin_style.css', __FILE__ ), array(), '1.0' );
	wp_enqueue_style( 'slick-css', plugins_url( 'assets/css/slick.css', __FILE__ ), array(), '1.8.1' );
	wp_enqueue_style( 'slick-theme-css', plugins_url( 'assets/css/slick-theme.css', __FILE__ ), array(), '1.8.1' );

	// JS for the Editor
	wp_enqueue_script( 'editor-scripts', plugins_url( 'assets/js/editor_scripts.js', __FILE__ ), array( 'jquery' ), '0.6.30' );
	wp_enqueue_script( 'theme-js', plugins_url( 'assets/js/scripts.js', __FILE__ ), array( 'jquery' ), time() );
	wp_enqueue_script( 'jquery-slick', plugins_url( 'assets/js/slick.min.js', __FILE__ ), array( 'jquery' ), '1.8.1' );
}

if ( !function_exists( 'betop_add_widget_styles' ) ) {
	function betop_add_widget_styles( $widget )
	{
		if ( file_exists( get_theme_file_path( '/assets/css/widgets/' . $widget . '/' . $widget . '.css' ) ) ) {
			wp_enqueue_style( 'betop-' . $widget, get_theme_file_uri( '/assets/css/widgets/' . $widget . '/' . $widget . '.css' ), array( ), ELEMENTOR_SM_WIDGETS_VERSION, 'all' );
		}
		if ( file_exists( get_theme_file_path( '/assets/js/widgets/' . $widget . '/' . $widget . '.js' ) ) ) {
			wp_enqueue_script( 'betop-' . $widget . '-js', get_theme_file_uri( '/assets/js/widgets/' . $widget . '/' . $widget . '.js' ), array( 'jquery' ), ELEMENTOR_SM_WIDGETS_VERSION, true );
		}
	}
}

// Menu

function menu_choices()
{
	$menus = wp_get_nav_menus();
	$items = array();
	$i = 0;
	foreach ( $menus as $menu ) {
		if ( $i == 0 ) {
			$default = $menu->slug;
			$i++;
		}
		$items[ $menu->slug ] = $menu->name;
	}
	return $items;
}


<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class TrainingSlider extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-training-slider';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Trainings Slider', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'fa fa-sliders';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'training-slider' );

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'block_title',
			[
				'name' => 'block_title',
				'label' => __( 'Block title', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$this->add_control(
			'block_description',
			[
				'name' => 'block_description',
				'label' => __( 'Block description', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);
		$this->add_control(
			'post_number',
			[
				'name' => 'post_number',
				'label' => __( 'Post number', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label_block' => true,
				'default' => 9,
				'description' => __( 'Set -1 to show all trainings', 'elementor-stm-widgets' ),
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();
		$post_count = $settings[ 'post_number' ] ? $settings[ 'post_number' ] : -1;
		$args = array(
			'post_type' => 'product',
			'numberposts' => $post_count,
			'post_status' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy' => 'product_type',
					'field' => 'slug',
					'terms' => 'grouped',
				),
			),
		);
		$trainings = get_posts( $args );
		?>
		<div class="container training-slider">
			<div class="row">
				<div class="col-lg-3 slider-content-area">
					<?php if ( !empty( $settings[ 'block_title' ] ) ): ?>
						<h2><?php esc_html_e( $settings[ 'block_title' ] ); ?></h2>
					<?php endif; ?>
					<?php if ( !empty( $settings[ 'block_description' ] ) ): ?>
						<p><?php esc_html_e( $settings[ 'block_description' ] ); ?></p>
					<?php endif; ?>
					<?php if ( $trainings ): ?>
						<div class="custom-navs"></div>
					<?php endif; ?>
				</div>
				<div class="col-lg-9 slider-slider-area ">
					<div class="training-slider-wrap owl-carousel">
						<?php
						foreach ( $trainings as $training ) {
							setup_postdata( $training );
							$id = $training->ID;
							require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/training-slider/training-slider.php' );

						}
						wp_reset_postdata();
						?>
					</div>
				</div>
			</div>

		</div>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class LoginButton extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-login-button';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Login button', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-button';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'login-button' );
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Colors', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'inline',
			[
				'label' => __( 'Display Inline', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::SWITCHER,
			]
		);
		$this->start_controls_tabs( 'button_colors' );
		$this->start_controls_tab(
			'button_normal',
			[
				'label' => __( 'Normal', 'elementor' ),
			]
		);

		$this->add_control(
			'background_color',
			[
				'name' => 'background_color',
				'label' => __( 'Background Color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .login-button' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'border_color',
			[
				'name' => 'border_color',
				'label' => __( 'Border color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .login-button' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'text_color',
			[
				'name' => 'text_color',
				'label' => __( 'Text color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .login-button' => 'color: {{VALUE}};',
					'{{WRAPPER}} .login-button:before' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab();

		$this->start_controls_tab(
			'button_hover',
			[
				'label' => __( 'Hover', 'elementor' ),
			]
		);
		$this->add_control(
			'background_color_hover',
			[
				'name' => 'background_color_hover',
				'label' => __( 'Background Color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .login-button:hover' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'border_color_hover',
			[
				'name' => 'border_color_hover',
				'label' => __( 'Border color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .login-button:hover' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'text_color_hover',
			[
				'name' => 'text_color_hover',
				'label' => __( 'Text color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .login-button:hover' => 'color: {{VALUE}};',
					'{{WRAPPER}} .login-button:hover:before' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();


	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings_for_display();
		?>
		<?php if ( is_user_logged_in() ) { ?>
		<span class="login-btn-wrap">
				<a href="<?php echo esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ); ?>"
				   title="<?php _e( 'My Account', 'elementor-stm-widgets' ); ?>" class="login-button btn"><i
							class="fa fa-user-circle-o"></i><?php _e( 'My Account', 'elementor-stm-widgets' ); ?></a>
				<ul class="dropdown-account">
					<li>
						<a href="<?php echo esc_url( wc_customer_edit_account_url() ); ?>"
						   class="pc-hv"><?php _e( 'Account details', 'elementor-stm-widgets' ); ?></a>
					</li>
					<li>
						<a href="<?php echo esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) . 'orders' ); ?>"
						   class="pc-hv"><?php _e( 'Orders', 'elementor-stm-widgets' ); ?></a>
					</li>
					<li>
						<a href="<?php echo esc_url( wp_logout_url() ); ?>"
						   title="<?php _e( 'Logout', 'elementor-stm-widgets' ); ?>"
						   class="pc-hv"><?php _e( 'Logout', 'elementor-stm-widgets' ); ?></a>
					</li>
				</ul>
			</span>
	<?php } else { ?>
		<a href="<?php echo esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ); ?>"
		   title="<?php _e( 'Login', 'woothemes' ); ?>" class="login-button btn logout"><i
					class="fa fa-user-circle-o"></i><?php _e( 'Login', 'elementor-stm-widgets' ); ?></a>
	<?php } ?>

		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




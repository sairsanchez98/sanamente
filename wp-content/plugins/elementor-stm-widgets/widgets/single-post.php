<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class SinglePost extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-single-post';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Single Post', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'fa fa-clipboard';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}

	public function stm_get_posts()
	{
		$args = array(
			'post_type' => 'post',
			'numberposts' => -1,
			'post_status' => 'publish'
		);
		$posts = get_posts( $args );
		$options = array();
		foreach ( $posts as $post ) {
			setup_postdata( $post );
			$options[ $post->ID ] = $post->post_title;
		}
		wp_reset_postdata();
		return $options;
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'single-post' );
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'view_type',
			[
				'name' => 'view_type',
				'label' => __( 'View type', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => array(
					'simple' => __( 'Simple', 'elementor-stm-widgets' ),
					'with_image' => __( 'With Image', 'elementor-stm-widgets' ),
				),
				'default' => 'simple'
			]
		);
		$this->add_control(
			'post_id',
			[
				'name' => 'post_id',
				'label' => __( 'Select Post', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => $this->stm_get_posts(),
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();
		$post_id = $settings['post_id'];
		if(!empty($post_id)){
			require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/single-post/' . $settings[ 'view_type' ] . '.php' );
		}
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




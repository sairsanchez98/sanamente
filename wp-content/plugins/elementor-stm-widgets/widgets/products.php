<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Products extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-products';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Products', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'mc-shopping-bag';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'products' );
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'post_number',
			[
				'name' => 'post_number',
				'label' => __( 'Post number', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label_block' => true,
				'default' => 9,
				'description' => __( 'Set -1 to show all trainings', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'title_color',
			[
				'name' => 'title_color',
				'label' => __( 'Title color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'selectors' => [
					'{{WRAPPER}} .title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();
		$post_count = $settings[ 'post_number' ] ? $settings[ 'post_number' ] : -1;
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => $post_count,
			'post_status' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy' => 'product_visibility',
					'terms' => array( 'exclude-from-catalog', 'exclude-from-search' ),
					'field' => 'name',
					'operator' => 'NOT IN',
					'include_children' => false,
				),
			),
		);
		$query = new \WP_Query( $args );

		?>
		<div class="container products-widget">
			<div class="row">
				<?php
				if ( $query->have_posts() ):
					while ( $query->have_posts() ):
						$query->the_post();
						global $product;
						?>
						<div class="col-6 col-xs-6 col-sm-6 col-md-4 col-lg-3">
							<div class="product-single">
								<div class="product-single-image">
									<a href="<?php the_permalink(); ?>">
										<?php if ( has_post_thumbnail( get_the_ID() ) ) {
											$image_id = get_post_thumbnail_id( get_the_ID() ); ?>
											<?php
											betop_get_the_post_thumbnail( $image_id, 'product-list-img' );
										} ?>
										<div class="product-single-content">
											<div class="title head-font">
												<?php the_title(); ?>
											</div>
											<div class="product-price pbcc head-font">
												<?php echo wp_kses_post($product->get_price_html()); ?>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					<?php
					endwhile;
					wp_reset_postdata();
				endif;
				?>
			</div>
		</div>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class CoachSlider extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-coach-slider';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Coach Slider', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-image-box';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
//		betop_add_widget_styles( 'coach-slider');


		$this->start_controls_section(
			'sm_coach_slides',
			[
				'label' => __( 'Coach Slides', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'view_style',
			[
				'name' => 'view_style',
				'label' => __( 'View style', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => array(
					'style_1' => __( 'Style 1', 'elementor-stm-widgets' ),
					'style_2' => __( 'Style 2', 'elementor-stm-widgets' )
				),
				'label_block' => true,
				'default' => 'style_1',
			]
		);
		$args = array(
			'post_type' => 'product',
			'numberposts' => -1,
			'post_status' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy' => 'product_type',
					'field' => 'slug',
					'terms' => 'grouped',
				),
			),
		);
		$trainings = get_posts( $args );
		$coach_list = array();
		foreach ( $trainings as $training ) {
			$coach_list[ $training->ID ] = $training->post_title;

		}
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'coach_slide', [
				'label' => __( 'Trainings', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'label_block' => true,
				'options' => $coach_list
			]
		);

		$this->add_control(
			'coach_slides',
			[
				'label' => __( 'Slides', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ coach_slide }}}',
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();

		?>
		<?php if ( !empty( $settings[ 'coach_slides' ] ) ): ?>
		<?php
		$produc_list = array();
		foreach ( $settings[ 'coach_slides' ] as $item ) {
			$produc_list[] = $item[ 'coach_slide' ];
		}
		$args = array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'orderby' => 'post__in',
			'post__in' => $produc_list,
		);
		$coachs = get_posts( $args );
		?>
		<?php if ( $settings[ 'view_style' ] == 'style_1' ): ?>
			<div class="coach-slider owl-carousel <?php echo esc_attr( $settings[ 'view_style' ] ); ?>">
				<?php foreach ( $coachs as $slide ): setup_postdata( $slide ); ?>
					<?php
					global $product;
					$post_id = $slide->ID;
					?>
					<div class="coach-slide">
						<div class="coach-slide__excerpt container">
							<div class="excerpt-text">
								<?php echo !empty( get_the_excerpt( $post_id ) ) ? get_the_excerpt( $post_id ) : ''; ?>
							</div>
							<div class="coach-slide__excerpt__price pbcc head-font">
								<?php if ( !empty( $product->get_price() ) ): ?>
									<div class="coach-price ">
										<a href="<?php echo get_the_permalink( $post_id ); ?>">
											<div class="coach-price-title sc">
												<?php echo !empty( get_the_title( $post_id ) ) ? get_the_title( $post_id ) : ''; ?>
											</div>
										</a>
										<div class="coach-price-wrap">
											<?php echo wp_kses_post($product->get_price_html()); ?>
											<span class="price-affix"><?php esc_html_e( '/ m', 'elementor-stm-widgets' ); ?></span>
										</div>
										<a href="<?php echo get_the_permalink( $post_id ); ?>"
										   class="btn dark white-bc-hv white-br-hv">
											<?php esc_html_e( 'Details', 'elementor-stm-widgets' ); ?>
										</a>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="coach-slide__image">
							<?php if ( has_post_thumbnail( $post_id ) ) {
								$image_id = get_post_thumbnail_id( $post_id ); ?>
								<?php
								betop_get_the_post_thumbnail( $image_id, 'full' );
							} ?>
						</div>
					</div>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		<?php else: ?>
			<div class="big-coach-slider-wrap <?php echo esc_attr( $settings[ 'view_style' ] ); ?>">
				<div class="owl-carousel coach-big-slider <?php echo esc_attr( $settings[ 'view_style' ] ); ?>">
					<?php foreach ( $coachs as $slide ): setup_postdata( $slide ); ?>
						<?php
						$post_id = $slide->ID; ?>
						<div class="coach-big-slide"
						     style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url( $post_id, 'full' ) ); ?>)">
							<div class="container">
								<div class="slide-content">
									<h2 class="white-c"><?php echo get_the_title( $post_id ); ?></h2>
									<?php if ( has_excerpt( $post_id ) ): ?>
										<div class="slider-description white-c">
											<?php echo esc_html( get_the_excerpt( $post_id ) ); ?>
										</div>
									<?php endif; ?>
									<div class="slide-link-wrap">
										<a href="<?php the_permalink( $post_id ); ?>"
										   class="btn pbcc pbrc white-c white-bc-hv pc-hv"><?php esc_html_e( 'Book an appointment', 'elementor-stm-widgets' ); ?></a>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>

		<?php endif; ?>
	<?php endif; ?>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




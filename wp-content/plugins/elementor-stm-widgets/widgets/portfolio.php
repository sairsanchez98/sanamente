<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Portfolio extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-portfolio';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Post type list', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-posts-grid';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'portfolio' );
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'post_number',
			[
				'name' => 'post_number',
				'label' => __( 'Post number', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label_block' => true,
				'default' => 6
			]
		);
		$this->add_control(
			'view_type',
			[
				'name' => 'view_type',
				'label' => __( 'View type', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => array(
					'grid' => __( 'Grid', 'elementor-stm-widgets' ),
					'grid_2' => __( 'Grid style 2', 'elementor-stm-widgets' ),
					'grid_3' => __( 'Grid style 3', 'elementor-stm-widgets' ),
					'grid_4' => __( 'Grid style 4', 'elementor-stm-widgets' ),
					'grid_5' => __( 'Grid style 5', 'elementor-stm-widgets' ),
					'masonry_grid' => __( 'Masonry grid', 'elementor-stm-widgets' ),
					'masonry_tiles' => __( 'Masonry tiles', 'elementor-stm-widgets' ),
					'carousel' => __( 'Carousel', 'elementor-stm-widgets' ),
				),
				'default' => 'grid'
			]
		);
		$this->add_control(
			'per_row',
			[
				'name' => 'per_row',
				'label' => __( 'Items per row', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'default' => 4,
				'condition' => [
					'view_type' => 'carousel'
				]
			]
		);
		$this->add_control(
			'post_type',
			[
				'name' => 'post_type',
				'label' => __( 'Post type', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => array(
					'portfolio' => __( 'Portfolio', 'elementor-stm-widgets' ),
					'events' => __( 'Events', 'elementor-stm-widgets' ),
					'services' => __( 'Services', 'elementor-stm-widgets' ),
					'post' => __( 'Posts', 'elementor-stm-widgets' ),
				),
				'default' => 'portfolio'
			]
		);
		$this->add_control(
			'hide_load_more',
			[
				'name' => 'hide_load_more',
				'label' => __( 'Hide load more button', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_block' => true,
			]
		);
		$this->add_control(
			'button_text',
			[
				'name' => 'button_text',
				'label' => __( 'Contact Button Text', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'Contact me', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'button_url',
			[
				'name' => 'button_url',
				'label' => __( 'Contact Button URL', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::URL,
				'label_block' => true,
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();
		$count = intval( $settings[ 'post_number' ] ) ? intval( $settings[ 'post_number' ] ) : 6;
		$post_status = $settings[ 'post_type' ] == 'events' ? 'future, publish' : 'publish';
		$args = array(
			'post_type' => $settings[ 'post_type' ],
			'numberposts' => $count,
			'post_status' => $post_status
		);
		$portfolios = get_posts( $args );
		$post_count = wp_count_posts( $settings[ 'post_type' ] );
		$post_count = $post_count->publish;
		$load_more = $post_count > $count ? true : false;
		$row_classes = '';

		if ( !empty( $settings[ 'view_type' ] ) && $settings[ 'view_type' ] == 'carousel' ) $row_classes = 'owl-carousel post_type_carousel';
		?>
		<div class="portfolio-wrap portfolio-<?php echo esc_attr( $settings[ 'view_type' ] );
		echo ' post-type-widget-' . esc_attr( $settings[ 'post_type' ] ); ?>">
			<div class="row <?php echo esc_attr( $row_classes ); ?>"
			     data-per-row="<?php echo !empty( $settings[ 'per_row' ] ) ? esc_attr( $settings[ 'per_row' ] ) : ''; ?>">
				<?php
				$i = 0;
				foreach ( $portfolios as $portfolio ) {
					setup_postdata( $portfolio );
					$id = $portfolio->ID;
					$i++;
					$i = $i > 8 ? 1 : $i;
					require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/post-type-templates/' . $settings[ 'post_type' ] . '-' . $settings[ 'view_type' ] . '.php' );
				}
				wp_reset_postdata();
				?>
			</div>
			<div class="text-center">
				<?php if ( $load_more && $settings[ 'hide_load_more' ] != 'yes' ): ?>
					<a href="#" class="load-more-portfolio btn sbrc sbcc white-c text-upper pbcc-hv pbrc-hv sc-hv"
					   data-count="<?php echo esc_attr( $post_count ); ?>"
					   data-per-page="<?php echo esc_attr( $count ); ?>"
					   data-offset="<?php echo esc_attr( $count ); ?>"
					   data-post-type="<?php echo esc_attr( $settings[ 'post_type' ] ); ?>"
					   data-view-type="<?php echo esc_attr( $settings[ 'view_type' ] ); ?>">
						<?php esc_html_e( 'Load more', 'elementor-stm-widgets' ); ?>
					</a>
				<?php endif; ?>
				<?php if ( $settings[ 'button_url' ][ 'url' ] ): ?>
					<a href="<?php echo esc_url( $settings[ 'button_url' ][ 'url' ] ); ?>"
					   class="btn sc text-upper pbcc white-c-hv black-bg-hv black-br-hv">
						<?php echo esc_html( $settings[ 'button_text' ] ); ?></a>
				<?php endif; ?>
			</div>
		</div>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




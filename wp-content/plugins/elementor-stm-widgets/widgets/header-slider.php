<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class HeaderSlider extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-header-slider';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Header Slider', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-slider-push';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'header-slider' );
		$this->start_controls_section(
			'content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);


		$this->add_responsive_control(
			'slider_height',
			[
				'label' => __( 'Slider Height', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
					]
				],
				'selectors' => [
					'{{WRAPPER}} .header-slide' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'slide_name', [
				'label' => __( 'Slide name', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'button_text', [
				'label' => __( 'Button text', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
			]
		);

		$repeater->add_control(
			'button_url', [
				'label' => __( 'Button url', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::URL,
			]
		);

		$repeater->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'label' => __( 'Background', 'elementor-stm-widgets' ),
				'types' => [ 'classic', 'gradient', 'video' ],
				'selector' => '{{WRAPPER}} {{CURRENT_ITEM}}',
			]
		);

		$this->add_control(
			'slides',
			[
				'label' => __( 'Slides', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ slide_name }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'style',
			[
				'label' => __( 'Style', 'elementor-stm-widgets' ),
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => __( 'Title typography', 'elementor-stm-widgets' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_3,
				'selector' => '{{WRAPPER}} .slide-title',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'label' => __( 'Button typography', 'elementor-stm-widgets' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .btn',
			]
		);

		$this->add_control(
			'title_color', [
				'label' => __( 'Title color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .slide-title' => 'color: {{VALUE}};'
				]
			]
		);

		$this->start_controls_tabs( 'button_colors' );

		$this->start_controls_tab(
			'button_colors_normal',
			[
				'label' => __( 'Normal', 'elementor' ),
			]
		);

		$this->add_control(
			'button_color', [
				'label' => __( 'Button color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .btn' => 'color: {{VALUE}};'
				]
			]
		);

		$this->add_control(
			'button_bg', [
				'label' => __( 'Button background color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#fccf33',
				'selectors' => [
					'{{WRAPPER}} .btn' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .owl-nav div:hover:before' => 'color: {{VALUE}};',
				]
			]
		);
		$this->add_control(
			'button_border', [
				'label' => __( 'Button border color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#fccf33',
				'selectors' => [
					'{{WRAPPER}} .btn' => 'border-color: {{VALUE}};',
				]
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'button_bg_hover',
			[
				'label' => __( 'Hover', 'elementor' ),
			]
		);

		$this->add_control(
			'button_color_hv', [
				'label' => __( 'Button color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .btn:hover' => 'color: {{VALUE}};'
				]
			]
		);

		$this->add_control(
			'button_bg_hv', [
				'label' => __( 'Button background color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#fccf33',
				'selectors' => [
					'{{WRAPPER}} .btn:hover' => 'background-color: {{VALUE}};'
				]
			]
		);

		$this->add_control(
			'button_border_hv', [
				'label' => __( 'Button border color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#fccf33',
				'selectors' => [
					'{{WRAPPER}} .btn:hover' => 'border-color: {{VALUE}};',
				]
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings(); ?>
		<div class="header-slider-wrapper">
			<div class="header-slider owl-carousel">
				<?php foreach ( $settings[ 'slides' ] as $slide ): ?>
					<div class="header-slide elementor-repeater-item-<?php echo esc_attr($slide[ '_id' ]); ?>">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<div class="slide-title">
										<?php echo wp_kses_post( $slide[ 'slide_name' ] ); ?>
									</div>
									<?php if ( !empty( $slide[ 'button_url' ] ) && !empty( $slide[ 'button_text' ] ) ): ?>
										<div class="slide-button">
											<a href="<?php echo esc_url( $slide[ 'button_url' ][ 'url' ] ); ?>"
											   class="btn"><?php echo esc_html( $slide[ 'button_text' ] ); ?></a>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<script>
			(function ($) {
				"use strict"; // Start of use strict
				$(window).on('load', function () {
					$('.header-slider-wrapper .header-slider').each(function () {
						$(this).owlCarousel({
							nav: true,
							items: 1,
							dots: false,
							loop: true,
							autoplay: true,
							autoplayTimeout: 6000,
							autoplayHoverPause: true,
						});
					});
				});
			})(jQuery);
		</script>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




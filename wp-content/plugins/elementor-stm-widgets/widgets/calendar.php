<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Calendar extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-calendar';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Coach Calendar', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'mc-calendar';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'calendar' );
		$this->start_controls_section(
			'sm_calendar',
			[
				'label' => __( 'Coach calendar', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'calendar_title', [
				'label' => __( 'Title', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'Schedule', 'elementor-stm-widgets' )
			]
		);
		$this->add_control(
			'link_text', [
				'label' => __( 'Link text', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'Download schedule', 'elementor-stm-widgets' )
			]
		);
		$this->add_control(
			'link_url', [
				'label' => __( 'Link url', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::URL,
				'label_block' => true,
			]
		);
		$this->add_control(
			'link_file', [
				'label' => __( 'File to download', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::MEDIA,
				'label_block' => true,
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'time', [
				'label' => __( 'Time', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'sunday', [
				'label' => __( 'Sunday', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'monday', [
				'label' => __( 'Monday', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'tuesday', [
				'label' => __( 'Tuesday', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'wednesday', [
				'label' => __( 'Wednesday', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'thursday', [
				'label' => __( 'Thursday', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'friday', [
				'label' => __( 'Friday', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'saturday', [
				'label' => __( 'Saturday', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$this->add_control(
			'coach_calendar',
			[
				'label' => __( 'Time', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ time }}}',
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();
		$btn_link = '';
		if ( !empty( $settings[ 'link_url' ] ) ) {
			$btn_link = $settings[ 'link_url' ];
		}
		if ( !empty( $settings[ 'link_file' ] ) ) {
			$btn_link = $settings[ 'link_file' ][ 'url' ];
		}
		?>
		<div class="coach-calendar">
			<div class="title-btn">
				<h2><?php echo esc_html( $settings[ 'calendar_title' ] ); ?></h2>
				<a href="<?php echo esc_url( $btn_link ); ?>"
				   class="btn dark"><?php echo esc_html( $settings[ 'link_text' ] ) ?></a>
			</div>
			<div class="coach-calendar__table">
				<table>
					<thead>
					<tr>
						<th class="no-bg"></th>
						<th><?php esc_html_e( 'Sun', 'elementor-stm-widgets' ); ?></th>
						<th><?php esc_html_e( 'Mon', 'elementor-stm-widgets' ); ?></th>
						<th><?php esc_html_e( 'Tue', 'elementor-stm-widgets' ); ?></th>
						<th><?php esc_html_e( 'Wed', 'elementor-stm-widgets' ); ?></th>
						<th><?php esc_html_e( 'Thu', 'elementor-stm-widgets' ); ?></th>
						<th><?php esc_html_e( 'Fri', 'elementor-stm-widgets' ); ?></th>
						<th><?php esc_html_e( 'Sat', 'elementor-stm-widgets' ); ?></th>
					</tr>
					</thead>
					<tbody>
					<?php if ( !empty( $settings[ 'coach_calendar' ] ) ): ?>
						<?php foreach ( $settings[ 'coach_calendar' ] as $time ): ?>
							<tr>
								<td class="pbcc"><?php echo !empty( $time[ 'time' ] ) ? esc_html( $time[ 'time' ] ) : ''; ?></td>
								<td><?php echo !empty( $time[ 'sunday' ] ) ? esc_html( $time[ 'sunday' ] ) : ''; ?></td>
								<td><?php echo !empty( $time[ 'monday' ] ) ? esc_html( $time[ 'monday' ] ) : ''; ?></td>
								<td><?php echo !empty( $time[ 'tuesday' ] ) ? esc_html( $time[ 'tuesday' ] ) : ''; ?></td>
								<td><?php echo !empty( $time[ 'wednesday' ] ) ? esc_html( $time[ 'wednesday' ] ) : ''; ?></td>
								<td><?php echo !empty( $time[ 'thursday' ] ) ? esc_html( $time[ 'thursday' ] ) : ''; ?></td>
								<td><?php echo !empty( $time[ 'friday' ] ) ? esc_html( $time[ 'friday' ] ) : ''; ?></td>
								<td><?php echo !empty( $time[ 'saturday' ] ) ? esc_html( $time[ 'saturday' ] ) : ''; ?></td>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
		<table>

		</table>

		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




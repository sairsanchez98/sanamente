<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;
use Elementor\Scheme_Color;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Call_To_Action extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-icon-title';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Call to action', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-favorite';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'call_to_action' );
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'text',
			[
				'name' => 'text',
				'label' => __( 'Text', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'label_block' => true,
				'default' => __( 'Any text', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'button_text',
			[
				'name' => 'button_text',
				'label' => __( 'Button text', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'Send me a message', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'button_link',
			[
				'name' => 'button_link',
				'label' => __( 'Button link', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$this->add_control(
			'icon',
			[
				'label' => __( 'Icon', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::ICON,
				'default' => 'fa fa-star',
			]
		);

		$this->add_control(
			'contact_text',
			[
				'name' => 'contact_text',
				'label' => __( 'Contact text', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( '+989 552 2000', 'elementor-stm-widgets' ),
			]
		);


		$this->add_control(
			'bg_color',
			[
				'label' => __( 'Section background color', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .call-to-action' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'padding',
			[
				'label' => __( 'Padding', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .call-to-action' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}',
				],
			]
		);
		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings_for_display();
		?>
		<div class="call-to-action full-width-container">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12">
						<?php if ( $settings[ 'text' ] ): ?>
							<?php echo wp_kses_post($settings[ 'text' ]); ?>
						<?php endif; ?>
					</div>
					<div class="col-lg-8 col-md-12">
						<?php if ( $settings[ 'button_link' ] ): ?>
							<a href="<?php echo esc_url($settings[ 'button_link' ]); ?>" class="btn">
								<?php echo wp_kses_post($settings[ 'button_text' ]); ?>
							</a>
						<?php endif; ?>
						<span class="contact-data">
							<?php if ( $settings[ 'icon' ] ): ?>
								<i class="<?php echo wp_kses_post($settings[ 'icon' ]); ?>"></i>
							<?php endif; ?>
							<?php if ( $settings[ 'contact_text' ] ): ?>
								<span class="contact-text"><?php echo wp_kses_post($settings[ 'contact_text' ]); ?></span>
							<?php endif; ?>
						</span>
					</div>
				</div>
			</div>
		</div>

		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Css_Filter;
use Elementor\Modules\DynamicTags\Module as TagsModule;

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Elementor google maps widget.
 *
 * Elementor widget that displays an embedded google map.
 *
 * @since 1.0.0
 */
class GoogleMapStyled extends Widget_Base
{

	/**
	 * Get widget name.
	 *
	 * Retrieve google maps widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'google_maps';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve google maps widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Google Maps Styled', 'elementor' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve google maps widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-google-maps';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the google maps widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * @since 2.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Get widget keywords.
	 *
	 * Retrieve the list of keywords the widget belongs to.
	 *
	 * @since 2.1.0
	 * @access public
	 *
	 * @return array Widget keywords.
	 */
	public function get_keywords()
	{
		return [ 'google', 'map', 'embed' ];
	}

	/**
	 * Register google maps widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'google-maps' );
		$this->start_controls_section(
			'section_map',
			[
				'label' => __( 'Map', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'lat',
			[
				'label' => __( 'Latitude', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => 51.507351,
				'default' => 51.507351,
				'label_block' => true,
			]
		);

		$this->add_control(
			'long',
			[
				'label' => __( 'Longitude', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'placeholder' => -0.127758,
				'default' => -0.127758,
				'label_block' => true,
			]
		);

		$this->add_control(
			'zoom',
			[
				'label' => __( 'Zoom', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 10,
				],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 20,
					],
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'height',
			[
				'label' => __( 'Height', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 40,
						'max' => 1440,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .google-maps-styled' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'image',
			[
				'name' => 'marker',
				'label' => __( 'Map marker', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'label_block' => true,
			]
		);

		$this->add_control(
			'prevent_scroll',
			[
				'label' => __( 'Prevent Scroll', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$this->add_control(
			'info_window', [
				'label' => __( 'Info Window Text', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
			]
		);

		$this->add_control(
			'map_styles', [
				'label' => __( 'Map Styles', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'description' => __( 'Here you can get custom styles https://mapstyle.withgoogle.com', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render google maps widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings_for_display();
		if ( empty( $settings[ 'lat' ] ) || empty( $settings[ 'long' ] ) ) {
			return;
		}
		$lat = $settings[ 'lat' ];
		$lng = $settings[ 'long' ];
		$map_zoom = $settings[ 'zoom' ][ 'size' ];
		$disable_mouse_whell = $settings[ 'prevent_scroll' ] ? true : false;
		$info_window = $settings[ 'info_window' ];
		$marker = $settings[ 'image' ][ 'url' ];
		$map_custom_style = '';
		if ( !empty( $settings[ 'map_styles' ] ) ) {
			$map_styles = $settings[ 'map_styles' ];
			$map_custom_style = "styles : {$map_styles}";
		}


		ob_start();

		require_once( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/js/gmap.php' );

		wp_add_inline_script( 'betop-main-scripts', ob_get_clean() );
		wp_enqueue_script( 'betop-stm-gmap' );

		?>
		<div class="google-maps-styled" id="google-maps-styled"></div>

		<?php
	}

	/**
	 * Render google maps widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _content_template()
	{
	}
}

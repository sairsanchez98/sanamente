<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class TrainingBlock extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-training-block';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Trainings Block', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'fa fa-table';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'training-block' );
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'view_type',
			[
				'name' => 'view_type',
				'label' => __( 'View type', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => array(
					'list' => __( 'List', 'elementor-stm-widgets' ),
					'slider' => __( 'Slider', 'elementor-stm-widgets' ),
				),
				'default' => 'list'
			]
		);
		$this->add_control(
			'post_number',
			[
				'name' => 'post_number',
				'label' => __( 'Post number', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label_block' => true,
				'default' => 9,
				'description' => __( 'Set -1 to show all trainings', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'title',
			[
				'name' => 'title',
				'label' => __( 'Title', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'Pricing', 'elementor-stm-widgets' ),
				'condition' => [
					'view_type' => 'slider',
				],
			]
		);

		$this->add_control(
			'view_style',
			[
				'name' => 'view_style',
				'label' => __( 'Slider type', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => array(
					'style_1' => __( 'Style 1', 'elementor-stm-widgets' ),
					'style_2' => __( 'Style 2', 'elementor-stm-widgets' ),
					'style_3' => __( 'Style 3', 'elementor-stm-widgets' ),
				),
				'label_block' => true,
				'default' => 'style_1',
				'condition' => [
					'view_type' => 'slider',
				],
			]
		);

		$this->add_control(
			'sub_title',
			[
				'name' => 'sub_title',
				'label' => __( 'Subtitle', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'condition' => [
					'view_style' => 'style_3',
				],
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();
		$post_count = $settings[ 'post_number' ] ? $settings[ 'post_number' ] : -1;
		$args = array(
			'post_type' => 'product',
			'numberposts' => $post_count,
			'post_status' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy' => 'product_type',
					'field' => 'slug',
					'terms' => 'grouped',
				),
			),
		);
		$trainings = get_posts( $args );
		?>
		<?php if ( $settings[ 'view_type' ] == 'slider' ): ?>
		<?php if ( $settings[ 'view_style' ] == 'style_1' ): ?>
			<div class="container-fluid training-block-slider-wrap <?php echo esc_attr( $settings[ 'view_style' ] ); ?>">
				<div class="row">
					<div class="col-md-4 trainings-title-wrap">
						<div class="trainings-title">
							<?php if ( $settings[ 'title' ] ): ?>
								<h2><?php echo esc_html( $settings[ 'title' ] ); ?></h2>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-8">
						<div class="training-block-slider owl-carousel">
							<?php
							foreach ( $trainings as $training ) {
								setup_postdata( $training );
								$id = $training->ID;
								require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/trainings/single-training-slider.php' );
							}
							wp_reset_postdata();
							?>
						</div>
					</div>
				</div>
			</div>
		<?php elseif($settings[ 'view_style' ] == 'style_2'): ?>
			<div class="training-block-slider-style_2 <?php echo esc_attr( $settings[ 'view_style' ] ); ?>">
				<div class="trainings-title container">
					<?php if ( $settings[ 'title' ] ): ?>
						<h2><?php echo esc_html( $settings[ 'title' ] ); ?></h2>
					<?php endif; ?>
				</div>
				<div class="training-slider owl-carousel">
					<?php
					foreach ( $trainings as $training ) {
						setup_postdata( $training );
						$id = $training->ID;
						require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/trainings/single-training-slider-style_2.php' );
					}
					wp_reset_postdata();
					?>
				</div>
			</div>
		<?php elseif($settings[ 'view_style' ] == 'style_3'): ?>
			<div class="training-block-slider-style_3 <?php echo esc_attr( $settings[ 'view_style' ] ); ?>">
				<div class="trainings-title container">
					<?php if ( $settings[ 'title' ] ): ?>
						<h2><?php echo esc_html( $settings[ 'title' ] ); ?></h2>
					<?php endif; ?>
				</div>
				<div class="training-slider owl-carousel">
					<?php
					foreach ( $trainings as $training ) {
						setup_postdata( $training );
						$id = $training->ID;
						require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/trainings/single-training-slider-style_2.php' );
					}
					wp_reset_postdata();
					?>
				</div>
			</div>
		<?php endif; ?>
	<?php elseif ( $settings[ 'view_type' ] == 'list' ): ?>
		<div class="training-table">
			<?php
			foreach ( $trainings as $training ) {
				setup_postdata( $training );
				$id = $training->ID;
				require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/trainings/single-training-list.php' );
			}
			wp_reset_postdata();
			?>
		</div>
	<?php endif; ?>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




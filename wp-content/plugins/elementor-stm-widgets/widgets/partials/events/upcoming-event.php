<div class="upcoming-event-wrap">
	<div class="upcoming-event">
		<a href="<?php echo esc_url(get_post_permalink($id)); ?>">
			<div class="upcoming-event__image <?php echo has_post_thumbnail($id) ? 'has-image' : 'no-image'; ?>">
				<?php
				$attachment_id = get_post_thumbnail_id($id);
				betop_get_the_post_thumbnail($attachment_id, 'upcoming-event-list');
				?>
				<div class="upcoming-event-meta">
					<div class="event-date head-font pbcc">
						<span class="day"><?php echo esc_html(get_the_date('d', $id)); ?></span>
						<span class="month"><?php echo esc_html(get_the_date('M', $id)); ?></span>
						<span class="year"><?php echo esc_html(get_the_date('o', $id)); ?></span>

					</div>
					<div class="event-title head-font">
						<?php echo esc_html(get_the_title($id)); ?>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>
<div class="stm-single-post simple">
	<div class="stm-single-post-date head-font pbcc">
		<div class="day">
			<?php echo esc_html( get_the_date( 'j', $post_id ) ); ?>
		</div>
		<div class="month">
			<?php echo esc_html( get_the_date( 'M', $post_id ) ); ?>
		</div>
	</div>
	<div class="stm-single-post-content">
		<h5>
			<a href="<?php the_permalink( $post_id ) ?>">
				<?php echo esc_html( get_the_title( $post_id ) ); ?>
			</a>
		</h5>
		<div class="excerpt">
			<?php echo esc_html( get_the_excerpt( $post_id ) ); ?>
		</div>
	</div>
</div>

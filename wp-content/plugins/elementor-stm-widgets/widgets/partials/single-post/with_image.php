<?php
$image_id = get_post_thumbnail_id( $post_id );
?>
<div class="stm-single-post with_image">
	<div class="image-wrap">
		<?php if ( $image_id ): ?>
			<a href="<?php the_permalink( $post_id ); ?>">
				<?php betop_get_the_post_thumbnail( $image_id, 'single-post-image' ); ?>
			</a>
		<?php endif; ?>
	</div>
	<div class="content-wrap">
		<div class="stm-single-post-date head-font pbcc">
			<div class="day">
				<?php echo esc_html( get_the_date( 'j', $post_id ) ); ?>
			</div>
			<div class="month">
				<?php echo esc_html( get_the_date( 'M', $post_id ) ); ?>
			</div>
		</div>
		<div class="stm-single-post-content">
			<h5>
				<a href="<?php the_permalink( $post_id ) ?>">
					<?php echo esc_html( get_the_title( $post_id ) ); ?>
				</a>
			</h5>
		</div>
	</div>
</div>

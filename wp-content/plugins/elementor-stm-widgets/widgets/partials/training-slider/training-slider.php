<?php
global $product;
?>
<div class="single-training-slide">
	<div class="single-training-image">
		<a href="<?php echo esc_url(get_the_permalink($id)); ?>">
			<?php if ( has_post_thumbnail( $id ) ) {
				$image_id = get_post_thumbnail_id( $id ); ?>
				<?php
				betop_get_the_post_thumbnail( $image_id, 'single-training-slide' );
			} ?>
			<div class="single-training-content">
				<div class="price head-font">
					<?php echo wp_kses_post($product->get_price_html()); ?>
				</div>
				<div class="training-title head-font">
					<?php echo esc_html( get_the_title( $id ) ); ?>
				</div>
			</div>
		</a>
	</div>
</div>
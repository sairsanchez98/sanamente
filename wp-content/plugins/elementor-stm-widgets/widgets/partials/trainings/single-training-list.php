<?php
global $product;
?>
<div class="single-training-table">
	<div class="single-training-table__title single-training-table__column">
		<div class="training-label pbcc-before head-font">
			<?php esc_html_e('Class', 'elementor-stm-widgets'); ?>
		</div>
		<?php if(!empty(get_the_title($id))): ?>
			<a href="<?php echo esc_url(get_the_permalink($id)); ?>">
				<h5><?php echo get_the_title($id); ?></h5>
			</a>
		<?php endif; ?>
		<?php
		$terms = wp_get_post_terms( $id, 'coach', array('hide_empty' => false) );
		foreach ($terms as $term):
			?>
			<div class="training-category">
				<a href="<?php echo esc_url(get_term_link($term->term_id, 'coach')) ?>" class="sc"><?php echo esc_html($term->name); ?></a>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="single-training-table__time single-training-table__column">
		<div class="training-label head-font pbcc-before">
			<?php esc_html_e('Time', 'elementor-stm-widgets'); ?>
		</div>
		<div class="child-products">
			<?php
			$children = $product->get_children();
			foreach ( $children as $child ):
			?>
			<div>
				<?php echo esc_html(get_the_title($child)); ?>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="single-training-table__price single-training-table__column">
		<div class="training-label head-font pbcc-before">
			<?php esc_html_e('Price', 'elementor-stm-widgets'); ?>
		</div>
		<div class="trainig-buttons">
			<?php if( !empty($product->get_price()) ): ?>
				<div class="training-price bottom-item pbcc">
					<div class="head-font">
						<?php echo wp_kses_post($product->get_price_html()); ?>
					</div>
					<div class="price-affix">
                        <?php betop_duration_products($id, true); ?>
					</div>
				</div>
			<?php endif; ?>
			<a href="<?php echo esc_url(get_the_permalink($id)); ?>" class="bottom-item join head-font dark dark-color-hv pbcc-hv pbrc-hv">
				<?php esc_html_e('View details', 'elementor-stm-widgets'); ?>
			</a>
		</div>
	</div>
</div>
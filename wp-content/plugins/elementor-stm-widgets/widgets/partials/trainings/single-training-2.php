<?php
$image_id = get_post_thumbnail_id( $id );
global $product;
?>
<div class="col-sm-6 col-md-6 col-lg-4 single-training-wrap">
	<div class="single-training">
		<div class="training-image">
			<a href="<?php echo esc_url( get_the_permalink( $id ) ); ?>">
				<?php if ( $image_id ) {
					betop_get_the_post_thumbnail( $image_id, 'training-style-2' );
				}
				?>
				<?php if ( !empty( $product->get_price() ) ): ?>
					<div class="training-price head-font sbcc">
						<div class="price-wrap">
							<?php echo wp_kses_post($product->get_price_html()); ?>
						</div>
						<div class="price-affix">
                            <?php betop_duration_products($id, true); ?>
						</div>
					</div>
				<?php endif; ?>
			</a>
		</div>
		<div class="training-content">
			<?php if ( !empty( get_the_title( $id ) ) ): ?>
				<a href="<?php echo esc_url( get_the_permalink( $id ) ); ?>">
					<h5><?php echo get_the_title( $id ); ?></h5>
				</a>
			<?php endif; ?>
			<?php if ( !empty( get_the_excerpt( $id ) ) ): ?>
				<div class="excerpt"><?php echo get_the_excerpt( $id ); ?></div>
			<?php endif; ?>
			<div class="training-button-wrap">
				<a href="<?php echo esc_url( get_the_permalink( $id ) ); ?>" class="btn">
					<?php esc_html_e( 'Read More', 'elementor-stm-widgets' ); ?>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="training-wrap container style_2">
	<div class="text-center training-block-title">
		<?php if ( !empty( $settings[ 'page_subtitle' ] ) ): ?>
			<h2><?php echo esc_html( $settings[ 'page_subtitle' ] ); ?></h2>
		<?php endif; ?>
	</div>
	<div class="row">
		<?php
		foreach ( $trainings as $training ) {
			setup_postdata( $training );
			$id = $training->ID;
			require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/trainings/single-training-2.php' );
		}
		wp_reset_postdata();
		?>
	</div>
	<div class="text-center training-block-button">
		<?php if ( !empty( $settings[ 'button_url' ][ 'url' ] ) && !empty( $settings[ 'button_text' ] ) ): ?>
			<a href="<?php echo esc_url( $settings[ 'button_url' ][ 'url' ] ); ?>" class="btn pbcc pbrc white-c sc-hv white-bc-hv">
				<?php echo esc_html( $settings[ 'button_text' ] ); ?>
			</a>
		<?php endif; ?>
	</div>
</div>
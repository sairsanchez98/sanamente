<div class="training-wrap">
	<div class="row subtitle-wrap">
		<div class="col-md-6 col-lg-8">
			<?php if ( !empty( $settings[ 'page_subtitle' ] ) ): ?>
				<h2><?php echo esc_html( $settings[ 'page_subtitle' ] ); ?></h2>
			<?php endif; ?>
		</div>
		<div class="col-md-6 col-lg-4">
			<?php if ( !empty( $settings[ 'button_url' ][ 'url' ] ) && !empty( $settings[ 'button_text' ] ) ): ?>
				<a href="<?php echo esc_url( $settings[ 'button_url' ][ 'url' ] ); ?>" class="btn dark">
					<?php echo esc_html( $settings[ 'button_text' ] ); ?>
				</a>
			<?php endif; ?>
		</div>
	</div>
	<div class="row">
		<?php
		foreach ( $trainings as $training ) {
			setup_postdata( $training );
			$id = $training->ID;
			require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/trainings/single-training.php' );
		}
		wp_reset_postdata();
		?>
	</div>
</div>
<?php
$image_id = get_post_thumbnail_id($id);
global $product;
?>
<div class="col-sm-6 col-md-6 col-lg-4 single-training-wrap">
	<div class="single-training">
		<div class="training-image">
			<a href="<?php echo esc_url(get_the_permalink($id)); ?>">
				<?php betop_get_the_post_thumbnail($image_id, 'training-grid-img'); ?>
			</a>
		</div>
		<div class="training-content">
			<?php if(!empty(get_the_title($id))): ?>
			<a href="<?php echo esc_url(get_the_permalink($id)); ?>">
				<h5><?php echo get_the_title($id); ?></h5>
			</a>
			<?php endif; ?>
			<?php if(!empty(get_the_excerpt($id))): ?>
				<div class="excerpt"><?php echo get_the_excerpt($id); ?></div>
			<?php endif; ?>
			<div class="trainig-buttons">
				<?php if( !empty($product->get_price()) ): ?>
					<div class="training-price bottom-item pbcc">
						<div class="head-font">
							<?php echo wp_kses_post($product->get_price_html()); ?>
						</div>
						<div class="price-affix">
							<?php betop_duration_products($id, true); ?>
						</div>
					</div>
				<?php endif; ?>
				<a href="<?php echo esc_url(get_the_permalink($id)); ?>" class="bottom-item join head-font dark dark-color-hv pbcc-hv pbrc-hv">
					<?php esc_html_e('Join Now', 'elementor-stm-widgets'); ?>
				</a>
				<a href="<?php echo esc_url(get_the_permalink($id)); ?>" class="bottom-item read-more head-font dark-color read-more pbrc pbcc-hv">
					<?php esc_html_e('Read More', 'elementor-stm-widgets'); ?>
				</a>
			</div>
		</div>

	</div>
</div>
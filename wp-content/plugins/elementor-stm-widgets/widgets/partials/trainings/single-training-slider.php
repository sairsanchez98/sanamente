<?php
global $product;
?>
<div class="single-training-wrap">
	<div class="single-training">
		<div class="training-content">
			<?php if(!empty(get_the_title($id))): ?>
			<a href="<?php echo esc_url(get_the_permalink($id)); ?>">
				<h5><?php echo get_the_title($id); ?></h5>
			</a>
			<?php endif; ?>
			<?php
				$terms = wp_get_post_terms( $id, 'coach', array('hide_empty' => false) );
				foreach ($terms as $term):
			?>
			<div class="training-category">
				<a href="<?php echo esc_url(get_term_link($term->term_id, 'coach')) ?>" class="sc"><?php echo esc_html($term->name); ?></a>
			</div>
			<?php endforeach; ?>
			<?php if(!empty(get_the_excerpt($id))): ?>
				<div class="excerpt"><?php echo get_the_excerpt($id); ?></div>
			<?php endif; ?>
			<div class="trainig-buttons">
				<?php if( !empty($product->get_price()) ): ?>
					<div class="training-price bottom-item pbcc">
						<div class="head-font">
							<?php echo wp_kses_post($product->get_price_html()); ?>
						</div>
						<div class="price-affix">
                            <?php betop_duration_products($id, true); ?>
						</div>
					</div>
				<?php endif; ?>
				<a href="<?php echo esc_url(get_the_permalink($id)); ?>" class="bottom-item join head-font dark dark-color-hv pbcc-hv pbrc-hv">
					<?php esc_html_e('Join Now', 'elementor-stm-widgets'); ?>
				</a>
			</div>
		</div>

	</div>
</div>
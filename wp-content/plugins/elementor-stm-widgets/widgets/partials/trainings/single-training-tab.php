<?php
global $product;
?>
<div class="row ">
	<div class="col-md-12 col-lg-6">
		<div class="coach-img-wrap">
			<?php if( has_post_thumbnail( $coach_id ) ){
				$image_id = get_post_thumbnail_id($coach_id); ?>
			<a href="<?php echo get_the_permalink( $coach_id ); ?>">
			<?php
				betop_get_the_post_thumbnail($image_id, 'training-tab-img');
			} ?>
			</a>
			<?php if( !empty($product->get_price()) ): ?>
			<div class="coach-price pbcc">
				<div class="head-font">
					<?php echo wp_kses_post($product->get_price_html()); ?>
				</div>
				<div class="price-affix">
                    <?php betop_duration_products($coach_id, true); ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="col-md-12 col-lg-6">
		<div class="coach-content">
			<a href="<?php echo get_the_permalink( $coach_id ); ?>">
				<h2><?php echo get_the_title($coach_id); ?></h2>
			</a>
			<div class="coach-content__excerpt">
				<?php echo get_the_excerpt($coach_id); ?>
			</div>
			<div class="join-btn">
				<a href="<?php echo esc_url(get_the_permalink($coach_id)); ?>" class="btn head-font dark dark-color-hv">
					<?php esc_html_e('Join Now', 'elementor-stm-widgets'); ?>
				</a>
			</div>
		</div>

	</div>
</div>
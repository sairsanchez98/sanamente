<div class="testimonials-widget style-1">
	<div class="testimonial-image-carousel">
		<?php foreach ($settings['testimonials'] as $testimonial): ?>
			<div class="testimonial-image">
				<?php echo wp_get_attachment_image($testimonial['image']['id'], 'medium'); ?>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="testimonial-content-carousel">
		<?php foreach ($settings['testimonials'] as $testimonial): ?>
			<div class="testimonial-content">
				<div class="testimonial-title head-font">
					<?php echo esc_html($testimonial['testimonials_title']); ?>
				</div>
				<div class="testimonial-sub-title head-font">
					<?php echo esc_html($testimonial['testimonials_sub_title']); ?>
				</div>
				<div class="testimonial-text">
					<?php echo wp_kses_post($testimonial['testimonials_content']); ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<script>
	(function ($) {
		"use strict"; // Start of use strict
		$(document).ready(function () {
			$('.testimonials-widget.style-1 .testimonial-image-carousel').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				asNavFor: '.testimonials-widget.style-1 .testimonial-content-carousel',
				dots: false,
				centerMode: true,
				arrows: true,
				centerPadding: '0',
				variableWidth: true,
				infinite: true,
				responsive: [
					{
						breakpoint: 600,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						}
					}
				]
			});
			$('.testimonials-widget.style-1 .testimonial-content-carousel').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				asNavFor: '.testimonials-widget.style-1 .testimonial-image-carousel',
				dots: false,
				arrows: false,
				infinite: true
			});
		});
	})(jQuery);
</script>
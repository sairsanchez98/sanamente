<div class="testimonials-widget style-2">
	<div class="testimonial-image-carousel">
		<?php foreach ($settings['testimonials'] as $testimonial): ?>
		<div class="testimonial-slide">
			<div class="testimonial-slide-top">
				<div class="testimonial-image">
					<?php echo wp_get_attachment_image($testimonial['image']['id'], 'medium'); ?>
				</div>
				<div class="testimonial-position">
					<div class="testimonial-title head-font">
						<?php echo esc_html($testimonial['testimonials_title']); ?>
					</div>
					<div class="testimonial-sub-title head-font">
						<?php echo esc_html($testimonial['testimonials_sub_title']); ?>
					</div>
				</div>
			</div>
			<div class="testimonial-slide-bottom">
				<?php echo wp_kses_post($testimonial['testimonials_content']); ?>
			</div>
		</div>

		<?php endforeach; ?>
	</div>
</div>

<script>
	(function ($) {
		"use strict"; // Start of use strict
		$(document).ready(function () {
			$('.testimonials-widget.style-2 .testimonial-image-carousel').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: false,
				arrows: true,
				infinite: true,
				responsive: [
					{
						breakpoint: 600,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						}
					}
				]
			});
		});
	})(jQuery);
</script>
<div class="testimonials-widget style-4">
	<i class="mc-quote-left tc"></i>
	<div class="testimonials-carousel owl-carousel">
		<?php foreach ( $settings[ 'testimonials' ] as $testimonial ): ?>
			<div class="testimonial-slide">
				<?php if ( !empty( $testimonial[ 'testimonials_content' ] ) ): ?>
					<div class="testimonial-content">
						<?php echo wp_kses_post( $testimonial[ 'testimonials_content' ] ); ?>
					</div>
				<?php endif; ?>
				<?php if ( !empty( $testimonial[ 'testimonials_title' ] ) ): ?>
					<div class="testimonial-title head-font">
						<?php echo esc_html( $testimonial[ 'testimonials_title' ] ); ?>
					</div>
				<?php endif; ?>
				<?php if ( !empty( $testimonial[ 'testimonials_sub_title' ] ) ): ?>
					<div class="testimonial-sub-title head-font">
						<?php echo esc_html( $testimonial[ 'testimonials_sub_title' ] ); ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="testimonials-dots" id="testimonials-dots">
		<?php foreach ( $settings[ 'testimonials' ] as $testimonial ): ?>
			<?php if ( !empty( $testimonial['image']['url'] ) ): ?>
				<img src="<?php echo esc_url($testimonial['image']['url']); ?>" alt="testimonial" />
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
</div>
<script>
	'use strict';

	(function ($) {
		$(window).load(function () {
			betop_testimonials_carousel();
		});

		function betop_testimonials_carousel() {
			var owlRtl = false;

			if ($('body').hasClass('rtl')) {
				owlRtl = true;
			}

			$('.testimonials-carousel').each(function () {

				var $this = $(this);

				var owl = $this.owlCarousel({
					rtl: owlRtl,
					nav: true,
					dots: true,
					dotsContainer: '#testimonials-dots',
					items: 1,
					autoplay: true,
					autoplayHoverPause: true,
					loop: false,
					slideBy: 1,
					animateOut: 'fadeOut',
					animateIn: 'fadeIn'
				});

				$('#testimonials-dots img').on('click', function () {
					owl.trigger('to.owl.carousel', [$(this).index(), 300]);
				});
			});
		}
	})(jQuery);
</script>

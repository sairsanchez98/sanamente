<div class="testimonials-widget style-3">
	<?php if (!empty($settings['slider_title'])): ?>
		<div class="container slider-title">
			<div class="row">
				<div class="col-md-4 pbcc">
					<h3>
						<?php echo wp_kses_post($settings['slider_title']); ?>
					</h3>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="testimonials-widget-wrapper">
		<div class="container">
			<div class="row">
				<div class="testimonial-thumbnails">
					<?php foreach ($settings['testimonials'] as $testimonial): ?>
						<div class="single-testimonial"
						     style="background-image: url(<?php echo esc_url($testimonial['image']['url']); ?>);">
							<div class="slide-content">
								<div class="testimonial-title head-font">
									<?php echo esc_html($testimonial['testimonials_title']); ?>
								</div>
								<div class="testimonial-sub-title head-font">
									<?php echo esc_html($testimonial['testimonials_sub_title']); ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="testimonials-main">
			<?php foreach ($settings['testimonials'] as $testimonial): ?>
				<div class="single-testimonial"
				     style="background-image: url(<?php echo esc_url($testimonial['image']['url']); ?>);">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
							</div>
							<div class="col-md-8">
								<div class="testimonial-text">
									<?php echo wp_kses_post($testimonial['testimonials_content']); ?>
								</div>
								<div class="testimonial-title head-font">
									<?php echo esc_html($testimonial['testimonials_title']); ?>
								</div>
								<div class="testimonial-sub-title head-font">
									<?php echo esc_html($testimonial['testimonials_sub_title']); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>

	</div>
</div>
<script>
	(function ($) {
		"use strict"; // Start of use strict
		$(document).ready(function () {
			$('.testimonials-widget.style-3 .testimonial-thumbnails').slick({
				slidesToShow: 3,
				slidesToScroll: 1,
				asNavFor: '.testimonials-widget.style-3 .testimonials-main',
				dots: false,
				arrows: true,
				vertical: true,
				verticalSwiping: true,
				centerMode: true,
				centerPadding: '0',
			});
			$('.testimonials-widget.style-3 .testimonials-main').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				asNavFor: '.testimonials-widget.style-3 .testimonial-thumbnails',
				dots: false,
				arrows: false,
			});
		});
	})(jQuery);
</script>

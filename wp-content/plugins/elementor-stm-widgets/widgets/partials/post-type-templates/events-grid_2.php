<div class="portfolio-single post-single col-md-4 col-sm-6">
	<?php if ( get_post_thumbnail_id( $id ) ): ?>
		<div class="portfolio-img">
			<a href="<?php the_permalink( $id ); ?>">
				<?php
				$attachment_id = get_post_thumbnail_id( $id );
				betop_get_the_post_thumbnail( $attachment_id, 'stm-portfolio-masonry-grid-8' );
				?>
			</a>
		</div>
	<?php endif; ?>
	<div class="portfolio-content">
		<div class="grid-2-date head-font">
			<div class="day">
				<?php echo esc_html( get_the_date( 'd', $id ) ); ?>
			</div>
			<div class="month pbcc">
				<?php echo esc_html( get_the_date( 'M', $id ) ); ?>
			</div>
		</div>
		<a href="<?php the_permalink( $id ); ?>" class="sc">
			<div class="head-font dark-color post-title pc-hv">
				<?php echo esc_html( get_the_title( $id ) ); ?>
			</div>
			<div class="head-font post-sub-title">
				<?php echo esc_html( get_the_excerpt( $id ) ); ?>
			</div>
		</a>
	</div>
</div>
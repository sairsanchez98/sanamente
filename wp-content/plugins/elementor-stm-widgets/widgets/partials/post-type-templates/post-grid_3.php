<div class="portfolio-single post-single col-md-3 col-sm-4 col-6">
	<?php if ( get_post_thumbnail_id( $id ) ): ?>
		<div class="portfolio-img">
			<a href="<?php the_permalink( $id ); ?>">
				<?php
				$attachment_id = get_post_thumbnail_id( $id );
				betop_get_the_post_thumbnail( $attachment_id, 'post-grid-3' );
				?>
			</a>
		</div>
	<?php endif; ?>
	<div class="portfolio-content">
		<a href="<?php the_permalink( $id ); ?>" class="sc">
			<div class="head-font post-title pc-hv">
				<?php echo esc_html( get_the_title( $id ) ); ?>
			</div>
		</a>
	</div>
</div>
<?php
$post = get_post( $id );
$author_id = $post->post_author;
$author_name = get_the_author_meta( 'display_name', $author_id );
?>
<div class="portfolio-single post-single col-lg-4 col-md-6 col-sm-6">
	<?php if ( get_post_thumbnail_id( $id ) ): ?>
		<div class="portfolio-img">
			<a href="<?php the_permalink( $id ); ?>">
				<?php
				$attachment_id = get_post_thumbnail_id( $id );
				betop_get_the_post_thumbnail( $attachment_id, 'stm-portfolio-masonry-grid-8' );
				?>
			</a>
		</div>
	<?php endif; ?>
	<div class="portfolio-content">
		<a href="<?php the_permalink( $id ); ?>" class="sc">
			<div class="head-font dark-color post-title pc-hv">
				<?php echo esc_html( get_the_title( $id ) ); ?>
			</div>
		</a>
		<div class="post-meta">
			<?php if ( !empty( $author_name ) ): ?>
				<span><?php echo esc_html( $author_name ); ?></span>
			<?php endif; ?>
			<?php if ( comments_open( $id ) ): ?>
				<a href="<?php echo esc_url( get_the_permalink( $id ) . '#comments' ); ?>" class="sc pc-hv">
				<span>
						<?php
						$comment_text = _n( 'Comment', 'Comments', get_comments_number( $id ), 'elementor-stm-widgets' );
						echo sprintf( esc_html__( '%s ' . $comment_text, 'elementor-stm-widgets' ), get_comments_number( $id ) );
						?>
					</span>
				</a>
			<?php endif; ?>
		</div>
	</div>
</div>
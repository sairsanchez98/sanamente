<div class="portfolio-single col-sm-6 col-md-4 service-single">
	<?php if( get_post_thumbnail_id($id) ): ?>
		<div class="portfolio-img">
			<a href="<?php the_permalink($id); ?>">
				<?php
				$attachment_id = get_post_thumbnail_id($id);
				betop_get_the_post_thumbnail($attachment_id, 'stm-portfolio-grid');
				?>
			</a>
			<?php
				$icon_id = get_post_meta($id, 'stmt_service_icon', true);
				if($icon_id && !empty($icon_id)): ?>
				<div class="service-icon">
					<div class="service-icon-wrap pbcc">
						<img src="<?php echo esc_url(wp_get_attachment_url($icon_id)); ?>" alt="<?php esc_html_e('icon', 'elementor-stm-widgets'); ?>" />
					</div>
				</div>
			<?php endif; ?>

		</div>
	<?php endif; ?>
	<div class="portfolio-content">
		<a href="<?php the_permalink($id); ?>" class="sc">
			<div class="head-font sc pc-hv">
				<?php echo get_the_title($id); ?>
			</div>
			<div class="portfolio-description sc">
				<?php echo get_the_excerpt($id); ?>
			</div>
		</a>
	</div>
</div>
<div class="portfolio-single col-md-6 col-lg-4">
	<?php if( get_post_thumbnail_id($id) ): ?>
		<div class="portfolio-img">
			<a href="<?php the_permalink($id); ?>">
				<?php
				$attachment_id = get_post_thumbnail_id($id);
				betop_get_the_post_thumbnail($attachment_id, 'stm-portfolio-masonry-big');
				?>
			</a>
			<div class="event-date head-font pbcc">
				<span class="day"><?php echo esc_html(get_the_date('d', $id)); ?></span>
				<span class="month"><?php echo esc_html(get_the_date('M', $id)); ?></span>
				<span class="year"><?php echo esc_html(get_the_date('o', $id)); ?></span>
			</div>
			<?php $status = get_post_status($id) == 'future' ? __('Upcoming', 'elementor-stm-widgets') : __('Passed', 'elementor-stm-widgets'); ?>
			<span class="time-label head-font <?php echo get_post_status($id) == 'future' ? 'pbcc sc' : 'dark'; ?>">
			<?php echo esc_html($status); ?>
			</span>
		</div>
	<?php endif; ?>
	<div class="portfolio-content">
		<a href="<?php the_permalink($id); ?>" class="sc">
			<div class="head-font pc-hv">
				<?php echo get_the_title($id); ?>
			</div>
			<div class="portfolio-description">
				<?php echo get_the_excerpt($id); ?>
			</div>
		</a>
	</div>
</div>
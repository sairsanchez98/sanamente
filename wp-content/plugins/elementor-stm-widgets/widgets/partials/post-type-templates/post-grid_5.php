<?php
$post_category = wp_get_post_categories( $id );
?>
<div class="portfolio-single post-single col-md-6 col-sm-6">
	<?php if ( get_post_thumbnail_id( $id ) ): ?>
		<div class="portfolio-img">
			<a href="<?php the_permalink( $id ); ?>">
				<?php
				$attachment_id = get_post_thumbnail_id( $id );
				betop_get_the_post_thumbnail( $attachment_id, 'stm-portfolio-grid' );
				?>
			</a>
		</div>
	<?php endif; ?>
	<div class="portfolio-content">
		<div class="date head-font">
			<div class="day">
				<?php echo esc_html( get_the_date( 'd', $id ) ); ?>
			</div>
			<div class="month">
				<?php echo esc_html( get_the_date( 'M y', $id ) ); ?>
			</div>
		</div>
		<div class="post-content">
			<div class="post-cats">
				<?php foreach ( $post_category as $category ): ?>
					<a href="<?php echo get_term_link( $category, 'category' ); ?>" class="head-font tc pc-hv">
						<?php echo esc_html( get_cat_name( $category ) ); ?>
					</a>
				<?php endforeach; ?>
			</div>
			<a href="<?php the_permalink( $id ); ?>" class="sc pc-hv">
				<div class="head-font post-title">
					<?php echo esc_html( get_the_title( $id ) ); ?>
				</div>
			</a>
		</div>
	</div>
</div>
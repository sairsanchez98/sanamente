<div class="portfolio-single post-single col-md-6 col-sm-6">
	<?php if ( get_post_thumbnail_id( $id ) ): ?>
		<div class="portfolio-img">
			<a href="<?php the_permalink( $id ); ?>">
				<?php
				$attachment_id = get_post_thumbnail_id( $id );
				betop_get_the_post_thumbnail( $attachment_id, 'stm-portfolio-grid' );
				?>
			</a>
		</div>
	<?php endif; ?>
	<div class="portfolio-content">
		<div class="date head-font">
			<div class="day">
				<?php echo esc_html( get_the_date( 'd', $id ) ); ?>
			</div>
			<div class="month">
				<?php echo esc_html( get_the_date( 'M y', $id ) ); ?>
			</div>
		</div>
		<div class="post-content">
			<a href="<?php the_permalink( $id ); ?>" class="sc pc-hv">
				<div class="head-font post-title">
					<?php echo esc_html( get_the_title( $id ) ); ?>
				</div>
			</a>
		</div>
	</div>
</div>
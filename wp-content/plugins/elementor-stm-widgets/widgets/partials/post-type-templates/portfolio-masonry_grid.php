<div class="portfolio-single col-md-6 col-lg-4">
	<?php if( get_post_thumbnail_id($id) ): ?>
		<div class="portfolio-img">
			<a href="<?php the_permalink($id); ?>">
				<?php
				$attachment_id = get_post_thumbnail_id($id);
				betop_get_the_post_thumbnail($attachment_id, 'stm-portfolio-masonry-big');
				?>
			</a>
		</div>
	<?php endif; ?>
	<div class="portfolio-content">
		<a href="<?php the_permalink($id); ?>" class="sc">
			<div class="head-font pc-hv">
				<?php echo get_the_title($id); ?>
			</div>
			<div class="portfolio-description">
				<?php echo get_the_excerpt($id); ?>
			</div>
		</a>
	</div>
</div>
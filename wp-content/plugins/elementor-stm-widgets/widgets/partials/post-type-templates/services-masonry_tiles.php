<?php
$size = 'stm-portfolio-masonry-grid-' . $i;
$size = ( $i == 4 || $i == 5) ? 'stm-portfolio-masonry-grid-3' : $size;
?>
<div class="portfolio-single">
	<?php if( get_post_thumbnail_id($id) ): ?>
		<div class="portfolio-img">
			<a href="<?php the_permalink($id); ?>">
				<?php
				$attachment_id = get_post_thumbnail_id($id);
				betop_get_the_post_thumbnail($attachment_id, $size);
				?>
			</a>
		</div>
	<?php endif; ?>
	<div class="portfolio-content">
		<a href="<?php the_permalink($id); ?>">
			<div class="head-font">
				<?php echo get_the_title($id); ?>
			</div>
			<div class="portfolio-description pc">
				<?php echo get_the_excerpt($id); ?>
			</div>
		</a>
		<span class="stm-plus"></span>
	</div>
</div>
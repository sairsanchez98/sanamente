<div class="portfolio-single post-single col-md-4 col-sm-6">
	<?php if( get_post_thumbnail_id($id) ): ?>
	<div class="portfolio-img">
		<a href="<?php the_permalink($id); ?>">
			<?php
			$attachment_id = get_post_thumbnail_id($id);
			betop_get_the_post_thumbnail($attachment_id, 'stm-portfolio-masonry-grid-8');
			?>
		</a>
	</div>
	<?php endif; ?>
	<div class="portfolio-content">
		<a href="<?php the_permalink($id); ?>" class="sc">
			<div class="pc head-font post-date">
				<?php echo esc_html( get_the_date( '', $id ) ); ?>
			</div>
			<div class="head-font dark-color post-title pc-hv">
				<?php echo esc_html(get_the_title($id)); ?>
			</div>
		</a>
	</div>
</div>
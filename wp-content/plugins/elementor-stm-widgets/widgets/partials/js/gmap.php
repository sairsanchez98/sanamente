	function stm_gmap_loaded() {

		var $ = jQuery;

		var center = new google.maps.LatLng(<?php echo esc_js($lat); ?>, <?php echo esc_js($lng); ?>);

		var mapOptions = {
			zoom: <?php echo esc_js($map_zoom); ?>,
			center: center,
			<?php echo sanitize_text_field($map_custom_style); ?>
		};

		var mapElement = document.getElementById('google-maps-styled');
		var map = new google.maps.Map(mapElement, mapOptions);
		var marker = new google.maps.Marker({
			position: center,
			icon: '<?php echo esc_url($marker); ?>',
			map: map,
			optimized: false,
			disableDefaultUI: true,
		});
		<?php if(!empty($info_window)): ?>
		var infowindow = new google.maps.InfoWindow({
			content: '<div class="info-window normal-font"><?php echo wp_kses_post($info_window); ?></div>',
			pixelOffset: new google.maps.Size(0, -10),
			boxStyle: {
				width: "306px"
			}
		});

		marker.addListener('click', function () {
			infowindow.open(map, marker);
			map.setCenter(center);
			$('#google-maps-styled').find('.info-window').parent().parent().parent().parent().addClass('infoblock-wrap');
		});

		<?php endif; ?>
	}

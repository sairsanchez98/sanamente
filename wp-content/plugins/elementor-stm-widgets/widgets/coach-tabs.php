<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class CoachTabs extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-coach-tabs';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Coach Tabs', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-tabs';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'coach-tabs' );
		$this->start_controls_section(
			'sm_coach_categories',
			[
				'label' => __( 'Coach categories', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'image_bg',
			[
				'label' => __( 'Background image', 'elementor-stm-widgets' ),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],

			]
		);
		$terms_args = array(
			'taxonomy' => 'coach',
		);
		$terms = get_terms( $terms_args );
		$terms_list = array();
		if ( !empty( $terms ) or !is_wp_error( $terms ) ) {
			foreach ( $terms as $term ) {
				$terms_list[ $term->term_id ] = $term->name;
			}
		}
		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'tab_category', [
				'label' => __( 'Category', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'label_block' => true,
				'options' => $terms_list
			]
		);


		$this->add_control(
			'tabs',
			[
				'label' => __( 'Tabs', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ tab_category }}}',
			]
		);


		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();
		$style = '';
		if ( !empty( $settings[ 'image_bg' ] ) ) {
			$style = 'style="background-image: url(' . $settings[ 'image_bg' ][ 'url' ] . ');"';
		}
		?>

		<div class="coach-tabs">
			<div class="coach-tabs__list">
				<div class="container">
					<div class="row">
						<?php
						$i = 0;
						foreach ( $settings[ 'tabs' ] as $item ) {
							$i++;
							$term_id = $item[ 'tab_category' ];
							$term = get_term( $term_id, 'coach' );
							if ( empty( $term ) or is_wp_error( $term ) ) continue;
							?>
							<div class="col-md-3 col-sm-6">
								<a href="#tab-<?php echo esc_attr( $i ); ?>"
								   class="pbcc-before catergory-tab-link <?php echo esc_attr($i) == 1 ? 'active' : ''; ?>">
									<div class="category-icon">
										<i class="<?php echo esc_attr( get_term_meta( $term_id, 'course_icon', true ) ); ?>"></i>
									</div>
									<div class="coach-cat-title head-font">
										<?php echo esc_html( $term->name ); ?>
									</div>
									<div class="coach-cat-description">
										<?php echo esc_html( $term->description ); ?>
									</div>
								</a>
							</div>
							<?php
						}
						?>
					</div>
				</div>
			</div>
			<div class="coach-tabs__posts" <?php echo wp_kses_post($style); ?>>
				<div class="container">
					<div class="row">
						<?php $i = 0;
						foreach ( $settings[ 'tabs' ] as $item ): ?>
							<?php $i++; ?>
							<div class="col-md-12 coach-tab <?php echo esc_attr($i) == 1 ? 'active' : ''; ?>"
							     id="tab-<?php echo esc_attr($i); ?>">
								<?php
								$args = array(
									'post_type' => 'product',
									'numberposts' => 1,
									'post_status' => 'publish',
									'tax_query' => array(
										'relation' => 'AND',
										array(
											'taxonomy' => 'product_type',
											'field' => 'slug',
											'terms' => 'grouped',
										),
										array(
											'taxonomy' => 'coach',
											'field' => 'id',
											'terms' => $item[ 'tab_category' ],
										)
									),
								);
								$coach = get_posts( $args );
								foreach ( $coach as $coach_item ) {
									setup_postdata( $coach_item );
									$coach_id = $coach_item->ID;
									require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/trainings/single-training-tab.php' );
								}
								wp_reset_postdata();
								?>
							</div>
						<?php
						endforeach; ?>
					</div>
				</div>
			</div>

		</div>

		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




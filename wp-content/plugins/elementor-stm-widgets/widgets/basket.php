<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Basket extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-basket';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'WooCommerce Basket for Header', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-woocommerce';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'basket' );
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'minicart',
			[
				'label' => __( 'Show minicart', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
			]
		);

		$this->add_control(
			'inline',
			[
				'label' => __( 'Display Inline', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
			]
		);


		$this->end_controls_section();


	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings_for_display();
		if ( class_exists( 'WooCommerce' ) ) {
			$wc = \WooCommerce::instance();
			$count = ( $wc->cart == null ) ? '0' : $wc->cart->get_cart_contents_count();
			?>

			<div class="stm-cart-button">
				<a href="<?php echo wc_get_cart_url(); ?>" class="btn btn-sm pbrc sc pbcc-hv">
					<span class="count wc-cart-count pbcc"><?php echo esc_html( $count ); ?></span>
					<i class="fa fa-shopping-bag"></i>
					<?php
					echo esc_html__( 'Cart', 'elementor-stm-widgets' );
					echo ( $settings[ 'minicart' ] == 'yes' ) ? '<i class="fa fa-angle-down"></i>' : '';
					?>
				</a>
				<?php if ( $settings[ 'minicart' ] == 'yes' && $wc->cart != null ): ?>
					<div class="mini-cart-dropdown widget_shopping_cart_content">
						<?php
						woocommerce_mini_cart();
						?>
					</div>
				<?php endif; ?>
			</div>


			<?php
		}
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




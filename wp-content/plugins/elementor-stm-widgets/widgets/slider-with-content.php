<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class SliderWithContent extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-slider-width-content';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Slider with content', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-slider-push';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'slider-with-content' );
		$this->start_controls_section(
			'content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'slider_title', [
				'label' => __( 'Title', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => 'Title',
			]
		);

		$this->add_control(
			'title_color', [
				'label' => __( 'Title color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .slider-title h3' => 'color: {{VALUE}};'
				]
			]
		);

		$this->add_control(
			'title_background_color', [
				'label' => __( 'Title background color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#fccf33',
				'selectors' => [
					'{{WRAPPER}} .slider-title' => 'background-color: {{VALUE}};'
				]
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'label' => __( 'Background', 'elementor-stm-widgets' ),
				'types' => [ 'classic', 'gradient', 'video' ],
				'selector' => '{{WRAPPER}} .slider-width-content-wrap',
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'slide_name', [
				'label' => __( 'Slide name', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'slide_title', [
				'label' => __( 'Title', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'slide_content', [
				'label' => __( 'Content', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
			]
		);


		$this->add_control(
			'slides',
			[
				'label' => __( 'Slides', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ slide_name }}}',
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'style',
			[
				'label' => __( 'Style', 'elementor-stm-widgets' ),
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => __( 'Title typography', 'elementor-stm-widgets' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_3,
				'selector' => '{{WRAPPER}} .slide-title',
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'content_typography',
				'label' => __( 'Content typography', 'elementor-stm-widgets' ),
				'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .slide-content',
			]
		);
		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings(); ?>
		<div class="slider-width-content-wrapper">

			<div class="container">
				<div class="row">
					<div class="col-md-4 slider-title">
						<h3><?php echo esc_html( $settings[ 'slider_title' ] ); ?></h3>
					</div>
				</div>
			</div>
			<div class="slider-width-content-wrap">
				<div>
					<div class="container">
						<div class="row">
							<div class="col-md-4 slider-width-content-navs">
								<ul>
									<?php foreach ( $settings[ 'slides' ] as $slide ): ?>
										<li class="head-font">
											<?php echo esc_html( $slide[ 'slide_name' ] ); ?>
										</li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="slider-with-content owl-carousel">
					<?php foreach ( $settings[ 'slides' ] as $slide ): ?>
						<div class="slide-with-content elementor-repeater-item-<?php echo esc_attr($slide[ '_id' ]); ?>">
							<div class="container">
								<div class="slide-title">
									<?php echo wp_kses_post( $slide[ 'slide_title' ] ); ?>
								</div>
								<div class="slide-content">
									<?php echo wp_kses_post( $slide[ 'slide_content' ] ); ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>

		</div>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




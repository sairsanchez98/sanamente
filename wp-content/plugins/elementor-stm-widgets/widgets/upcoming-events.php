<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class UpcomingEvents extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-upcoming-events';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Upcoming Events', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'fa fa-calendar';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'upcoming-events' );
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'post_number',
			[
				'name' => 'post_number',
				'label' => __( 'Post number', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label_block' => true,
				'default' => 3
			]
		);
		$this->add_control(
			'button_text',
			[
				'name' => 'button_text',
				'label' => __( 'Button text', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'all events', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'button_url',
			[
				'name' => 'button_url',
				'label' => __( 'Event page URL', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::URL,
				'label_block' => true,
			]
		);
		$this->add_control(
			'section_title',
			[
				'name' => 'section_title',
				'label' => __( 'Section Title', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$this->add_control(
			'section_description',
			[
				'name' => 'section_description',
				'label' => __( 'Section description', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'label_block' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Style', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'title_color',
			[
				'name' => 'title_color',
				'label' => __( 'Title color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'selectors' => [
					'{{WRAPPER}} .upcoming-text h2' => 'color: {{VALUE}};',
					'{{WRAPPER}} .upcoming-text .more-events:not(:hover)' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();
		$count = intval( $settings[ 'post_number' ] ) ? intval( $settings[ 'post_number' ] ) : 3;
		$args = array(
			'post_type' => 'events',
			'numberposts' => $count,
			'post_status' => 'future'
		);
		$events = get_posts( $args );
		$post_count = wp_count_posts( 'portfolio' );
		$post_count = $post_count->publish;
		?>
		<div class="upcoming-events">
			<div class="row">
				<div class="col-md-12 col-lg-4 col-xl-4 upcoming-text">
					<?php if ( !empty( $settings[ 'section_title' ] ) ): ?>
						<h2><?php echo esc_html( $settings[ 'section_title' ] ); ?></h2>
					<?php endif; ?>
					<?php if ( !empty( $settings[ 'section_description' ] ) ): ?>
						<div class="event-description"><?php echo wp_kses_post( $settings[ 'section_description' ] ); ?></div>
					<?php endif; ?>
					<?php if ( $settings[ 'button_url' ][ 'url' ] ): ?>
						<a href="<?php echo esc_url( $settings[ 'button_url' ][ 'url' ] ); ?>"
						   class="more-events head-font sc pbrc pc-hv">
							<?php echo esc_html( $settings[ 'button_text' ] ); ?></a>
					<?php endif; ?>
				</div>
				<div class="col-md-12 col-lg-8 col-xl-8">
					<div class="upcoming-event-list">
						<?php
						foreach ( $events as $event ) {
							setup_postdata( $event );
							$id = $event->ID;
							require( WP_PLUGIN_DIR . '/elementor-stm-widgets/widgets/partials/events/upcoming-event' . '.php' );
						}
						wp_reset_postdata();
						?>
					</div>
				</div>
			</div>
			<div class="text-center">

			</div>
		</div>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




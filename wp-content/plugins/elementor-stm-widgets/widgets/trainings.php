<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Trainings extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-trainings';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Trainings', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'fa fa-table';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'trainings' );
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'post_number',
			[
				'name' => 'post_number',
				'label' => __( 'Post number', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'label_block' => true,
				'default' => 9,
				'description' => __( 'Set -1 to show all trainings', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'button_text',
			[
				'name' => 'button_text',
				'label' => __( 'Button Text', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'Contact me', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'page_subtitle',
			[
				'name' => 'page_subtitle',
				'label' => __( 'Block title', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __( 'What I Do', 'elementor-stm-widgets' ),
			]
		);
		$this->add_control(
			'button_url',
			[
				'name' => 'button_url',
				'label' => __( 'Contact Button URL', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::URL,
				'label_block' => true,
			]
		);

		$this->add_control(
			'view_style',
			[
				'name' => 'view_style',
				'label' => __( 'View Style', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
						'style_1' => __( 'Style 1', 'elementor-stm-widgets' ),
						'style_2' => __( 'Style 2', 'elementor-stm-widgets' )
				],
				'default' => 'style_1',
				'label_block' => true,
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();
		$post_number = !empty( $settings[ 'post_number' ] ) ? $settings[ 'post_number' ] : 9;
		$args = array(
			'post_type' => 'product',
			'numberposts' => $post_number,
			'post_status' => 'publish',
			'tax_query' => array(
				array(
					'taxonomy' => 'product_type',
					'field' => 'slug',
					'terms' => 'grouped',
				),
			),
		);
		$trainings = get_posts( $args );
		require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/trainings/view_styles/' . $settings['view_style'] . '.php' );
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class ImageText extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-image-text';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __( 'Image with text', 'elementor-stm-widgets' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-image';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return [ 'theme-elements' ];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return [ 'manufacturer' ];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles( 'image-text' );
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'text',
			[
				'name' => 'text',
				'label' => __( 'Text', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'default' => __( 'betop', 'elementor-stm-widgets' ),
			]
		);

		$this->add_control(
			'link',
			[
				'name' => 'link',
				'label' => __( 'Link', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::URL,
				'label_block' => true,
			]
		);

		$this->add_control(
			'image',
			[
				'name' => 'image',
				'label' => __( 'Image', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'label_block' => true,
			]
		);
		$this->add_control(
			'text_color',
			[
				'name' => 'text_color',
				'label' => __( 'Text color', 'elementor-stm-widgets' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'selectors' => [
					'{{WRAPPER}} .image-text-text' => 'color: {{VALUE}};'
				],
			]
		);

		$this->add_control(
			'image_border_radius',
			[
				'label' => __( 'Border Radius', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .image-text img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);


		$this->end_controls_section();


	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings_for_display();
		?>
		<div class="image-text">
			<?php if ( $settings[ 'link' ] ): ?>
				<a class="image-text-link" href="<?php echo esc_url( $settings[ 'link' ][ 'url' ] ); ?>">
					<?php if ( $settings[ 'image' ][ 'url' ] ): ?>
						<div class="image-text-img"><img src="<?php echo esc_url( $settings[ 'image' ][ 'url' ] ); ?>"
						                                 alt="<?php echo esc_attr( $settings[ 'text' ] ); ?>"></div>
					<?php endif ?>
					<?php if ( $settings[ 'text' ] ): ?>
						<div class="image-text-text"><?php echo wp_kses_post($settings[ 'text' ]); ?></div>
					<?php endif ?>
				</a>
			<?php else: ?>
				<?php if ( $settings[ 'image' ][ 'url' ] ): ?>
					<div class="image-text-img"><img src="<?php echo esc_url( $settings[ 'image' ][ 'url' ] ); ?>"
					                                 alt="<?php echo esc_attr( $settings[ 'text' ] ); ?>"></div>
				<?php endif ?>
				<?php if ( $settings[ 'text' ] ): ?>
					<div class="image-text-text"><?php echo wp_kses_post($settings[ 'text' ]); ?></div>
				<?php endif ?>
			<?php endif ?>
		</div>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




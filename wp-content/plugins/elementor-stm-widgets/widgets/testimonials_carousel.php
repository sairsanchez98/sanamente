<?php

namespace SmWidgets\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Base_Control;

if (!defined('ABSPATH')) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class TestimonialsCarousel extends Widget_Base
{

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name()
	{
		return 'sm-testimonials_carousel';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title()
	{
		return __('Testimonials Carousel', 'elementor-stm-widgets');
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon()
	{
		return 'eicon-slider-push';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories()
	{
		return ['theme-elements'];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends()
	{
		return ['manufacturer'];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls()
	{
		betop_add_widget_styles('testimonials_carousel');
		$this->start_controls_section(
			'sm_offices_content',
			[
				'label' => __('Content', 'elementor-stm-widgets'),
			]
		);

		$this->add_control(
			'testimonial_style', [
				'label' => __('Style', 'elementor-stm-widgets'),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'style-1' => __('Style 1'),
					'style-2' => __('Style 2'),
					'style-3' => __('Style 3'),
					'style-4' => __('Style 4'),
				],
				'label_block' => true,
				'default' => 'style-1',
			]
		);

		$this->add_control(
			'slider_title', [
				'label' => __('Slider title', 'elementor-stm-widgets'),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block' => true,
				'condition' => [
					'testimonial_style' => 'style-3'
				]
			]
		);

		$this->add_control(
			'arrow_color', [
				'label' => __('Arrow color', 'elementor-stm-widgets'),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => 'rgba(0, 0, 0, 0.5)',
				'selectors' => [
					'{{WRAPPER}} .slick-arrow:before' => 'color: {{VALUE}};'
				]
			]
		);

		$this->add_control(
			'title_color', [
				'label' => __('Title color', 'elementor-stm-widgets'),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .testimonial-title' => 'color: {{VALUE}};'
				]
			]
		);

		$this->add_control(
			'sub_title_color', [
				'label' => __('Subtitle color', 'elementor-stm-widgets'),
				'type' => \Elementor\Controls_Manager::COLOR,
				'label_block' => true,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .testimonial-sub-title' => 'color: {{VALUE}};'
				]
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'testimonials_title', [
				'label' => __('Title', 'elementor-stm-widgets'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => 'Title',
			]
		);

		$repeater->add_control(
			'image',
			[
				'label' => __('Choose Image', 'elementor-stm-widgets'),
				'type' => Controls_Manager::MEDIA,
				'dynamic' => [
					'active' => true,
				],

			]
		);

		$repeater->add_control(
			'testimonials_sub_title', [
				'label' => __('Subtitle', 'elementor-stm-widgets'),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'default' => 'Subtitle',
			]
		);

		$repeater->add_control(
			'testimonials_content', [
				'label' => __('Content', 'elementor-stm-widgets'),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
			]
		);


		$this->add_control(
			'testimonials',
			[
				'label' => __('Testimonials', 'elementor-stm-widgets'),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ testimonials_title }}}',
			]
		);


		$this->end_controls_section();


	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render()
	{
		$settings = $this->get_settings();
		$view_style = $settings['testimonial_style'];
		$view_style = empty($view_style) ? 'style-1' : $view_style;
		require_once (ELEMENTOR_SM_WIDGETS__DIR__. '/widgets/partials/testimonials/' . $view_style . '.php');
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template()
	{

	}
}




<?php
require_once(ELEMENTOR_SM_WIDGETS__DIR__ . '/importer/helpers/content.php');
require_once(ELEMENTOR_SM_WIDGETS__DIR__ . '/importer/helpers/theme_options.php');
require_once(ELEMENTOR_SM_WIDGETS__DIR__ . '/importer/helpers/set_content.php');
require_once(ELEMENTOR_SM_WIDGETS__DIR__ . '/importer/helpers/widgets.php');

function betop_demo_import_content()
{
    check_ajax_referer('betop_demo_import_content', 'nonce');
    $layout = 'therapist';

    if(!empty($_GET['demo_template'])){
        $layout = sanitize_title($_GET['demo_template']);
    }

	update_option('betop_layout', $layout);

    /*Import content*/
    betop_theme_import_content($layout);

    /*Import theme options*/
    update_option('theme_options', betop_get_layout_options($layout));

    /*Import sliders*/
    //stm_theme_import_sliders($layout);

    /*Import Widgets*/
    betop_theme_import_widgets($layout);

    /*Set menu and pages*/
    betop_set_content_options($layout);

    do_action('betop_import_done');

	wp_send_json(array(
		'url' => get_home_url('/'),
		'title' => esc_html__('View site', 'betop'),
		'theme_options_title' => esc_html__('Theme options', 'betop'),
		'theme_options' => esc_url_raw(admin_url('admin.php?page=beTop'))
	));
    die();

}

add_action('wp_ajax_betop_demo_import_content', 'betop_demo_import_content');
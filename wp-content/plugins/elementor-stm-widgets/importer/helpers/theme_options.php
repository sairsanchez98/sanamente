<?php
$layouts_to_path = ELEMENTOR_SM_WIDGETS__DIR__ . '/importer/helpers/theme_options';

require_once $layouts_to_path . '/health_coach.php';
require_once $layouts_to_path . '/therapist.php';
require_once $layouts_to_path . '/yoga.php';
require_once $layouts_to_path . '/fitness.php';
require_once $layouts_to_path . '/business.php';
require_once $layouts_to_path . '/motivation.php';
require_once $layouts_to_path . '/swimming.php';
require_once $layouts_to_path . '/boxing.php';

function betop_get_layout_options($layout)
{
	$options = call_user_func('betop_theme_options_' . $layout);
	$options = json_decode($options, true);
	return $options;
}


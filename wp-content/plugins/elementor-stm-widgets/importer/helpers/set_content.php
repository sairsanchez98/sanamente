<?php
function betop_set_content_options( $layout ) {

	//Set pages
	update_option('show_on_front', 'page');
	$front_page = get_page_by_title( 'Front page' );
	if ( isset( $front_page->ID ) ) {
		update_option( 'page_on_front', $front_page->ID );
	}

	$whishlist_page = get_page_by_title( 'Whishlist' );
	if ( isset( $whishlist_page->ID ) ) {
		$whishlist_options = array(
			'wishlist' => $whishlist_page->ID
		);
		update_option( 'tinvwl-page', $whishlist_options );
	}
	$possible_blog_pages = array(
		'Blog',
	);

	foreach ( $possible_blog_pages as $blog_page ) {
		$blog_page = get_page_by_title( $blog_page );
		if ( isset( $blog_page->ID ) ) {
			update_option( 'page_for_posts', $blog_page->ID );
		}
	}
	// Disable Elementor DEFAULT COLOR AND TYPOGRAPHY
	update_option( 'elementor_disable_color_schemes', 'yes' );
	update_option( 'elementor_disable_typography_schemes', 'yes' );
	// set elementor container width
	update_option( 'elementor_container_width', 1200 );
	//	add to any settings
	$share_settings = get_option('addtoany_options');
	$share_settings['display_in_posts_on_front_page'] = -1;
	$share_settings['display_in_excerpts'] = -1;
	$share_settings['display_in_posts'] = -1;
	$share_settings['display_in_feed'] = -1;
	$share_settings['display_in_pages'] = -1;
	$share_settings['display_in_portfolio'] = -1;
	$share_settings['display_in_products'] = -1;
	$share_settings['display_in_events'] = -1;
	$share_settings['display_in_services'] = -1;
	update_option( 'addtoany_options', $share_settings );

	//	wishlist settings

	$wishlist_settings = get_option('tinvwl-add_to_wishlist');
	$wishlist_settings['position'] = 'shortcode';
	update_option('tinvwl-add_to_wishlist', $wishlist_settings);
	$wishlist_loop_settings = get_option('tinvwl-add_to_wishlist_catalog');
	$wishlist_loop_settings['position'] = 'shortcode';
	$wishlist_loop_settings['text'] = '';
	$wishlist_loop_settings['text_remove'] = '';
	update_option('tinvwl-add_to_wishlist_catalog', $wishlist_loop_settings);

	// update option content imported
	update_option('betop_content_imported', '1');


    $shop_page = get_page_by_title( 'Shop' );
    if ( isset( $shop_page->ID ) ) {
        update_option( 'woocommerce_shop_page_id', $shop_page->ID );
    }
    $cart_page = get_page_by_title( 'Cart' );
    if ( isset( $cart_page->ID ) ) {
        update_option( 'woocommerce_cart_page_id', $cart_page->ID );
    }
    $checkout_page = get_page_by_title( 'Checkout' );
    if ( isset( $checkout_page->ID ) ) {
        update_option( 'woocommerce_checkout_page_id', $checkout_page->ID );
    }
    $account_page = get_page_by_title( 'My Account' );
    if ( isset( $account_page->ID ) ) {
        update_option( 'woocommerce_myaccount_page_id', $account_page->ID );
    }


	if(defined('STM_LMS_PATH') && function_exists('stm_lms_import_sample_data')) {
	    $steps = array(
	        'questions',
            'quizzes',
            'lessons',
            'courses'
        );

	    foreach($steps as $step) {
            stm_lms_import_sample_data($step, false);
        }
    }

    $include = array(
        'rule' => array(
            'basic-global'
        )
    );
    $exclude = array();

    $q = new WP_Query(array(
        'post_type' => 'elementor-hf',
        'posts_per_page' => -1
    ));
    if($q->have_posts()) {
        while($q->have_posts()) {
            $q->the_post();
            update_post_meta(get_the_ID(), 'ehf_target_include_locations', serialize($include));
            update_post_meta(get_the_ID(), 'ehf_target_exclude_locations', serialize($exclude));

        }
    }
    wp_reset_postdata();

}



<?php

namespace SmWidgets;


use SmWidgets\Widgets\EventForm;
use SmWidgets\Widgets\Menu;
use SmWidgets\Widgets\FooterMenu;
use SmWidgets\Widgets\Logo;
use SmWidgets\Widgets\Basket;
use SmWidgets\Widgets\SliderWithContent;
use SmWidgets\Widgets\TestimonialsCarousel;
use SmWidgets\Widgets\Call_To_Action;
use SmWidgets\Widgets\Portfolio;
use SmWidgets\Widgets\GoogleMapStyled;
use SmWidgets\Widgets\LoginButton;
use SmWidgets\Widgets\Trainings;
use SmWidgets\Widgets\TrainingSlider;
use SmWidgets\Widgets\UpcomingEvents;
use SmWidgets\Widgets\TrainingBlock;
use SmWidgets\Widgets\CoachTabs;
use SmWidgets\Widgets\CoachSlider;
use SmWidgets\Widgets\Calendar;
use SmWidgets\Widgets\ImageText;
use SmWidgets\Widgets\Products;
use SmWidgets\Widgets\AnimateBlock;
use SmWidgets\Widgets\HeaderSlider;
use SmWidgets\Widgets\SinglePost;


if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Main Plugin Class
 *
 * Register new elementor widget.
 *
 * @since 1.0.0
 */
class Plugin
{

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct()
	{
		$this->add_actions();
	}

	/**
	 * Add Actions
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function add_actions()
	{
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'on_widgets_registered' ] );
	}


	/**
	 * On Widgets Registered
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function on_widgets_registered()
	{
		$this->includes();
		$this->register_widget();
	}

	/**
	 * Includes
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function includes()
	{
		require __DIR__ . '/widgets/menu.php';
		require __DIR__ . '/widgets/footer-menu.php';
		require __DIR__ . '/widgets/logo.php';
		require __DIR__ . '/widgets/basket.php';
		require __DIR__ . '/widgets/testimonials_carousel.php';
		require __DIR__ . '/widgets/call_to_action.php';
		require __DIR__ . '/widgets/portfolio.php';
		require __DIR__ . '/widgets/google_map.php';
		require __DIR__ . '/widgets/login_button.php';
		require __DIR__ . '/widgets/trainings.php';
		require __DIR__ . '/widgets/upcoming-events.php';
		require __DIR__ . '/widgets/training-block.php';
		require __DIR__ . '/widgets/coach-tabs.php';
		require __DIR__ . '/widgets/coach-slider.php';
		require __DIR__ . '/widgets/calendar.php';
		require __DIR__ . '/widgets/event-form.php';
		require __DIR__ . '/widgets/image-text.php';
		require __DIR__ . '/widgets/products.php';
		require __DIR__ . '/widgets/slider-with-content.php';
		require __DIR__ . '/widgets/animate-block.php';
		require __DIR__ . '/widgets/header-slider.php';
		require __DIR__ . '/widgets/single-post.php';
		require __DIR__ . '/widgets/training-slider.php';
	}

	/**
	 * Register Widget
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function register_widget()
	{
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Menu() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new FooterMenu() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Logo() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new EventForm() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Basket() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new TestimonialsCarousel() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Call_To_Action() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Portfolio() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new GoogleMapStyled() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new LoginButton() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Trainings() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new UpcomingEvents() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new TrainingBlock() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new CoachTabs() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new CoachSlider() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Calendar() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ImageText() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Products() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new SliderWithContent() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new AnimateBlock() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new HeaderSlider() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new SinglePost() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new TrainingSlider() );
	}
}

new Plugin();

<?php
// Image Gallery

add_action('elementor/element/image-gallery/section_gallery/before_section_end', function ($element, $args) {
	// add a control
	$element->add_control('slider', // update the control
		[
			'label' => __('Slider', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'render_type' => 'template',
		]
	);
}, 10, 2);

//Video
add_action('elementor/element/video/section_video/before_section_end', function ($element, $args) {

	// add a control
	$element->add_control(
		'inline',
		[
			'label' => __('Display Inline', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
		]
	);

}, 10, 2);
add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('video' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});

// Social Icons
add_action('elementor/element/social-icons/section_social_icon/before_section_end', function ($element, $args) {

	// add a control
	$element->add_control(
		'inline',
		[
			'label' => __('Display Inline', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
		]
	);

}, 10, 2);
add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('social-icons' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});

// Icon
add_action('elementor/element/icon/section_icon/before_section_end', function ($element, $args) {

	// add a control
	$element->add_control(
		'inline',
		[
			'label' => __('Display Inline', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
		]
	);

}, 10, 2);
add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('icon' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});
add_action('elementor/element/button/section_button/before_section_end', function($element){
	$element->add_control('icon_size',
		[
			'label' => __('Icon Size', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'range' => [
				'px' => [
					'min' => 6,
					'max' => 100,
				],
			],
			'selectors' => [
				'{{WRAPPER}} i' => 'font-size: {{SIZE}}{{UNIT}};',
			],
		]
	);
});

// Heading Widget

add_action('elementor/element/heading/section_title/before_section_end', function ($element, $args) {
	// add a control
	$element->add_control('color', // update the control
		[
			'label' => __('Title Bold Color', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::COLOR,
			'default' => '',
			'selectors' => [
				'{{WRAPPER}} b' => 'color: {{VALUE}};',
			],
		]
	);

	$element->update_responsive_control(
		'align',
		[
			'label' => __('Alignment', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::CHOOSE,
			'options' => [
				'left' => [
					'title' => __('Left', 'elementor-stm-widgets'),
					'icon' => 'fa fa-align-left',
				],
				'center' => [
					'title' => __('Center', 'elementor-stm-widgets'),
					'icon' => 'fa fa-align-center',
				],
				'right' => [
					'title' => __('Right', 'elementor-stm-widgets'),
					'icon' => 'fa fa-align-right',
				],
				'justify' => [
					'title' => __('Justified', 'elementor-stm-widgets'),
					'icon' => 'fa fa-align-justify',
				],
			],
			'render_type' => 'template',
			'default' => '',
			'selectors' => [
				'{{WRAPPER}}' => 'text-align: {{VALUE}};',
			],
		]
	);


	$element->add_control('icons', // update the control
		[
			'label' => __('Icon', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::ICONS,
			'label_block' => true,
		]
	);

	$element->add_control('icon_size',
		[
			'label' => __('Icon Size', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'range' => [
				'px' => [
					'min' => 6,
					'max' => 300,
				],
			],
			'selectors' => [
				'{{WRAPPER}} .elementor-heading-title i' => 'font-size: {{SIZE}}{{UNIT}};',
			],
		]
	);
	$element->add_control('icon_space',
		[
			'label' => __('Space after icon', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'range' => [
				'px' => [
					'min' => 6,
					'max' => 100,
				],
			],
			'selectors' => [
				'{{WRAPPER}} .elementor-heading-title i' => 'margin-right: {{SIZE}}{{UNIT}};',
			],
		]
	);

	$element->add_control('icon_color',
		[
			'label' => __('Icon Color', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::COLOR,
			'selectors' => [
				'{{WRAPPER}} .elementor-heading-title i' => 'color: {{VALUE}};',
			],
		]
	);

	// add a control
	$element->add_control(
		'inline',
		[
			'label' => __('Display Inline', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
		]
	);


}, 10, 2);


add_action('elementor/widget/render_content', function ($content, $widget) {


	if ('heading' === $widget->get_name()) {

		$settings = $widget->get_settings_for_display();

		if (empty($settings['title'])) {
			return;
		}

		$widget->add_render_attribute('title', 'class', 'elementor-heading-title');

		if (!empty($settings['size'])) {
			$widget->add_render_attribute('title', 'class', 'elementor-size-' . $settings['size']);
		}
		if (!empty($settings['icons'])) {
			$widget->add_render_attribute('_wrapper', 'class', 'title-with-icon');
		}

		if (!empty($settings['align'])) {
			$widget->add_render_attribute('title', 'class', 'elementor-align-after-' . $settings['align']);
		}
		if (!empty($settings['align_mobile'])) {
			$widget->add_render_attribute('title', 'class', 'elementor-align-after-mobile-' . $settings['align_mobile']);
		}
		if (!empty($settings['align_tablet'])) {
			$widget->add_render_attribute('title', 'class', 'elementor-align-after-tablet-' . $settings['align_tablet']);
		}
		if ($settings['icons']['value']) {
			$title = '<i class="' . $settings['icons']['value'] . '"></i><span>' . $settings['title'] . '</span>';
		} else {
			$title = $settings['title'];
		}


		if (!empty($settings['link']['url'])) {
//			$widget->add_render_attribute('url', 'href', $settings['link']['url']);

			if ($settings['link']['is_external']) {
				$widget->add_render_attribute('url', 'target', '_blank');
			}

			if (!empty($settings['link']['nofollow'])) {
				$widget->add_render_attribute('url', 'rel', 'nofollow');
			}

			$title = sprintf('<a %1$s>%2$s</a>', $widget->get_render_attribute_string('url'), $title);
		}

		if ($settings['inline'] == 'yes') {
			$widget->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
		}

		$title_html = sprintf('<%1$s %2$s>%3$s</%1$s>', $settings['header_size'], $widget->get_render_attribute_string('title'), $title);
		echo wp_kses_post($title_html);

	} else {
		return $content;
	}


}, 10, 2);


add_action('elementor/frontend/section/before_render', function ($element) {

	// Make sure we are in a section element
	if ('section' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if (!empty($settings['text_align'])) {
		$element->add_render_attribute('_wrapper', 'class', 'elementor-align-' . $settings['text_align']);
	}

});


//Image
add_action('elementor/element/image/section_image/before_section_end', function ($element) {
	// add a control
	$element->add_control('background_animation', // update the control
		[
			'label' => __('Background Animation', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'render_type' => 'template',
		]
	);

	$element->add_control(
		'background_position',
		[
			'label' => __('Background position', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SELECT,
			'dynamic' => [
				'active' => true,
			],
			'options' => [
				'top_left' => __('Top Left', 'elementor-stm-widgets'),
				'top_right' => __('Top Right', 'elementor-stm-widgets'),
				'bottom_left' => __('Bottom Left', 'elementor-stm-widgets'),
				'bottom_right' => __('Bottom Right', 'elementor-stm-widgets'),
			],
			'condition' => [
				'background_animation' => 'yes',
			],
			'show_label' => false,
		]
	);

	$element->add_control('background_color', // update the control
		[
			'label' => __('Background Color', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::COLOR,
			'render_type' => 'template',
			'condition' => [
				'background_animation' => 'yes',
			],
			'selectors' => [
				'{{WRAPPER}} .elementor-image::before' => 'background-color: {{VALUE}};',
			],
		]
	);

	$element->add_control(
		'background_size',
		[
			'label' => __('Background size', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SELECT,
			'dynamic' => [
				'active' => true,
			],
			'options' => [
				'bg_small' => __('Small', 'elementor-stm-widgets'),
				'bg_medium' => __('Medium', 'elementor-stm-widgets'),
				'bg_large' => __('Large', 'elementor-stm-widgets'),
			],
			'condition' => [
				'background_animation' => 'yes',
			],
			'show_label' => true,
		]
	);
	$element->add_control('icon_overlay_settings', // update the control
		[
			'label' => __('Overlay icon', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'render_type' => 'template',
		]
	);
	$element->add_control(
		'ovelay_icon',
		[
			'label' => __('Choose icon', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::ICON,
			'dynamic' => [
				'active' => true,
			],
			'condition' => [
				'icon_overlay_settings' => 'yes',
			],
			'show_label' => true,
		]
	);
	$element->add_responsive_control(
		'icon_position_top',
		[
			'label' => __('Icon top position', 'elementor'),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => ['px', '%'],
			'range' => [
				'px' => [
					'min' => 0,
					'max' => 1000,
				],
			],
			'selectors' => [
				'{{WRAPPER}} .icon-wrap' => 'top: {{SIZE}}{{UNIT}};',
			],
			'condition' => [
				'icon_overlay_settings' => 'yes',
			],
		]
	);
	$element->add_responsive_control(
		'icon_position_right',
		[
			'label' => __('Icon right position', 'elementor'),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => ['px', '%'],
			'range' => [
				'px' => [
					'min' => 0,
					'max' => 1000,
				],
			],
			'selectors' => [
				'{{WRAPPER}} .icon-wrap' => 'right: {{SIZE}}{{UNIT}};',
			],
			'condition' => [
				'icon_overlay_settings' => 'yes',
			],
		]
	);
	$element->add_responsive_control(
		'icon_position_bottom',
		[
			'label' => __('Icon bottom position', 'elementor'),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => ['px', '%'],
			'range' => [
				'px' => [
					'min' => 0,
					'max' => 1000,
				],
			],
			'selectors' => [
				'{{WRAPPER}} .icon-wrap' => 'bottom: {{SIZE}}{{UNIT}};',
			],
			'condition' => [
				'icon_overlay_settings' => 'yes',
			],
		]
	);
	$element->add_responsive_control(
		'icon_position_left',
		[
			'label' => __('Icon left position', 'elementor'),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => ['px', '%'],
			'selectors' => [
				'{{WRAPPER}} .icon-wrap' => 'left: {{SIZE}}{{UNIT}};',
			],
			'range' => [
				'px' => [
					'min' => 0,
					'max' => 1000,
				],
			],
			'condition' => [
				'icon_overlay_settings' => 'yes',
			],
		]
	);
	$element->add_responsive_control(
		'icon_size',
		[
			'label' => __('Icon size', 'elementor'),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => ['px', '%'],
			'selectors' => [
				'{{WRAPPER}} .icon-wrap i' => 'font-size: {{SIZE}}{{UNIT}};',
			],
			'range' => [
				'px' => [
					'min' => 0,
					'max' => 100,
				],
			],
			'condition' => [
				'icon_overlay_settings' => 'yes',
			],
		]
	);
	$element->add_responsive_control(
		'icon_padding',
		[
			'label' => __('Icon padding', 'elementor'),
			'type' => \Elementor\Controls_Manager::DIMENSIONS,
			'size_units' => ['px', '%'],
			'selectors' => [
				'{{WRAPPER}} .icon-wrap' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
			],
			'condition' => [
				'icon_overlay_settings' => 'yes',
			],
		]
	);
	$element->add_responsive_control(
		'icon_background',
		[
			'label' => __('Icon background', 'elementor'),
			'type' => \Elementor\Controls_Manager::COLOR,
			'selectors' => [
				'{{WRAPPER}} .icon-wrap' => 'background-color: {{VALUE}};',
			],
			'condition' => [
				'icon_overlay_settings' => 'yes',
			],
		]
	);
	$element->add_responsive_control(
		'icon_color',
		[
			'label' => __('Icon color', 'elementor'),
			'type' => \Elementor\Controls_Manager::COLOR,
			'selectors' => [
				'{{WRAPPER}} .icon-wrap i' => 'color: {{VALUE}};',
			],
			'condition' => [
				'icon_overlay_settings' => 'yes',
			],
		]
	);
}, 11);

add_action('elementor/widget/before_render_content', 'betop_render_image', 101);
/**
 * Adding a new attribute to our button
 *
 * @param \Elementor\Widget_Base $button
 */

add_action('elementor/widget/render_content', function ($content, $widget) {

	if ('image' === $widget->get_name()) {

		$settings = $widget->get_settings_for_display();
		if ($settings['icon_overlay_settings'] == 'yes' && !empty($settings['ovelay_icon'])) {
			return $content . '<div class="icon-wrap"><i class="' . esc_attr($settings['ovelay_icon']) . '"></i> </div>';
		} else {
			return $content;
		}
		?>
		<?php

	} else {
		return $content;
	}


}, 10, 2);


function betop_render_image($image)
{
	if ('image' === $image->get_name()) {
		// Get the settings
		$settings = $image->get_settings();

		// Adding classes to image wrapper
		if ($settings['background_position']) {
			$image->add_render_attribute('_wrapper', array(
				'class' => $settings['background_position']
			));
		}
		if ($settings['background_size']) {
			$image->add_render_attribute('_wrapper', array(
				'class' => $settings['background_size']
			));
		}
		if ($settings['background_animation'] == 'yes') {
			$image->add_render_attribute('_wrapper', array(
				'class' => 'animation_bg'
			));
		}
		if ($settings['icon_overlay_settings'] == 'yes') {
			$image->add_render_attribute('_wrapper', array(
				'class' => 'icon-overlay'
			));
		}
	}
}

// Menu

add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('sm-menu' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});

// Icon List
add_action('elementor/element/icon-list/section_icon/before_section_end', function ($element, $args) {

	// add a control
	$element->add_control(
		'inline',
		[
			'label' => __('Display Inline', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
		]
	);

}, 10, 2);
//Icon box
add_action('elementor/element/icon-box/section_style_icon/before_section_end', function ($element, $args) {

	// add a control
	$element->add_control(
		'sm-position',
		[
			'label' => __('Icon position', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SELECT,
			'options' => array(
				'default' => __('Default', 'elementor-stm-widgets'),
				'left' => __('Left', 'elementor-stm-widgets')
			)
		]
	);

}, 10, 2);

add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('icon-box' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('sm-position') == 'left') {
		$element->add_render_attribute('_wrapper', 'class', 'icon_left');
	}

});


add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('icon-list' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});

// Login Button

add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('sm-login-button' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});

// Text Editor
add_action('elementor/element/text-editor/section_editor/before_section_end', function ($element, $args) {

	// add a control
	$element->add_control(
		'inline',
		[
			'label' => __('Display Inline', 'elementor-stm-widgets'),
			'type' => \Elementor\Controls_Manager::SWITCHER,
		]
	);

}, 10, 2);
add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('text-editor' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});

// Search

add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('sm-search' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});

// Basket

add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('sm-basket' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});

// Default Button

add_action('elementor/frontend/widget/before_render', function ($element) {

	// Make sure we are in a section element
	if ('sm-default-button' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});


// Logo

add_action('elementor/frontend/widget/before_render', function ($element) {

	if ('sm-logo' !== $element->get_name()) {
		return;
	}
	// get the settings
	$settings = $element->get_settings();

	if ($element->get_settings('inline') == 'yes') {
		$element->add_render_attribute('_wrapper', 'class', 'sm_display_inline');
	}

});


//custom icons
function betop_add_custom_icons($controls_registry)
{
	// Get existing icons
	$icons = $controls_registry->get_control('icon')->get_settings('options');
	// Append new icons
	$new_icons = array_merge(
		array(
			'mc-working-hours' => 'Working hours',
			'mc-office-phone' => 'Office phone',
			'mc-globe' => 'Globe',
			'mc-mail' => 'Mail',
			'mc-tag' => 'Tag',
			'mc-minus' => 'Minus',
			'mc-plus' => 'Plus',
			'mc-chevron-left-1' => 'Chevron left',
			'mc-chevron-right' => 'Chevron right',
			'mc-mailbox' => 'Mailbox',
			'mc-couple' => 'Couple',
			'mc-head' => 'Head',
			'mc-teen' => 'Teen',
			'mc-article' => 'Article',
			'mc-tribune' => 'Tribune',
			'mc-video' => 'Video',
			'mc-book' => 'Book',
			'mc-fitness' => 'Fitness',
			'mc-running' => 'Running',
			'mc-stationary-bicycle' => 'Stationary Bicycle',
			'mc-waist' => 'Waist',
			'mc-shopping-bag' => 'Bag',
			'mc-zoom' => 'Zoom',
			'mc-left' => 'Left arrow',
			'mc-right' => 'Right arrow',
			'mc-groceries' => 'Groceries',
			'mc-certificate' => 'Certificate',
			'mc-play' => 'Play',
			'mc-hamburger' => 'Hamburger',
			'mc-meditation' => 'Meditation',
			'mc-padmasana' => 'Padmasana',
			'mc-warrior' => 'Warrior',
			'mc-yog-lotus' => 'Lotus',
			'mc-yog-hamsa' => 'Hamsa',
			'mc-yog-book' => 'Book',
			'mc-yog-hand' => 'Hand',
			'mc-yog-om' => 'Om',
			'mc-yog-stress' => 'Stress',
			'mc-yog-ballon' => 'Ballon',
			'mc-yog-incense' => 'Incense',
			'mc-yog-hand-1' => 'Hand',
			'mc-yog-hand-2' => 'Hand',
			'mc-yog-meditation' => 'Meditation',
			'mc-yog-chakra' => 'Chakra',
			'mc-yog-placeholder' => 'Placeholder',
			'mc-yog-bosu-ball' => 'Bosu ball',
			'mc-yog-flower' => 'Flower',
			'mc-yog-hand-3' => 'Hand',
			'mc-yog-esteem' => 'Esteem',
			'mc-yog-happiness' => 'Happiness',
			'mc-yog-necklace' => 'Necklace',
			'mc-yog-peace-sign' => 'Peace sign',
			'mc-yog-stone' => 'Stone',
			'mc-yog-yin-yang' => 'Yin-yang',
			'mc-yog-yoga' => 'Yoga',
			'mc-yog-clothes' => 'Clothes',
			'mc-yog-incense-1' => 'Incense',
			'mc-yog-candles' => 'Candles',
			'mc-yog-heart' => 'Heart',
			'mc-yog-body' => 'Body',
			'mc-yog-drop' => 'Drop',
			'mc-yog-listen' => 'Listen',
			'mc-yog-head' => 'Head',
			'mc-yog-carpet' => 'Carpet',
			'mc-yog-droplet' => 'Droplet',
			'mc-yog-ball' => 'Ball',
			'mc-yog-teapot' => 'Teapot',
			'mc-yog-chakra-1' => 'Chakra',
			'mc-yog-friends' => 'Friends',
			'mc-yog-hand-4' => 'Hand',
			'mc-yog-bamboo' => 'Bamboo',
			'mc-yog-chakra-2' => 'Chakra',
			'mc-yog-hand-5' => 'Hand',
			'mc-yog-buddha' => 'Buddha',
			'mc-yog-heart-1' => 'Heart',
			'mc-yog-pants' => 'Pants',
			'mc-yog-bowl' => 'Bowl',
			'mc-yog-resistance' => 'Resistance',
			'mc-yog-plant' => 'Plant',
			'mc-yog-chatting' => 'Chatting',
			'mc-yog-head-1' => 'Head',
			'mc-yog-calendar' => 'Calendar',
			'mc-calendar' => 'Calendar',
			'mc-burner' => 'Burner',
			'mc-chakra-1' => 'Chakra',
			'mc-chakra-2' => 'Chakra',
			'mc-chakra-3' => 'Chakra',
			'mc-chakra' => 'Chakra',
			'mc-flower' => 'Flower',
			'mc-incense' => 'Incense',
			'mc-lotus-1' => 'Lotus',
			'mc-lotus' => 'Lotus',
			'mc-om-1' => 'Om',
			'mc-om' => 'Om',
			'mc-stones' => 'Stones',
			'mc-teapot' => 'Teapot',
			'mc-yoga-1' => 'Yoga',
			'mc-yoga' => 'Yoga',
			'mc-coin' => 'Coin',
			'mc-clock' => 'Clock',
			'mc-smile' => 'Smile',
			'mc-medal' => 'Medal',
			'mc-quote-left' => 'Quote Left',
			'mc-quote-right' => 'Quote Right',
		),
		$icons
	);
	// Then we set a new list of icons as the options of the icon control
	$controls_registry->get_control('icon')->set_settings('options', $new_icons);
}

add_action('elementor/controls/controls_registered', 'betop_add_custom_icons', 10, 1);


//Custom icons new
function betop_elementor_add_custom_icons_new( $tabs = array() ) {

    $new_icons[ 'betop' ] = [
        'name'          => 'betop',
        'label'         => 'Betop Icons',
        'url'           => '',
        'enqueue'       => '',
        'prefix'        => '',
        'displayPrefix' => '',
        'labelIcon'     => 'fas fa-paint-brush',
        'ver'           => '1.0.1',
        'fetchJson'     => ELEMENTOR_SM_WIDGETS__PLUGINURL__ . 'assets/fonts/icons.json',
    ];

    return array_merge( $tabs, $new_icons );
}
add_action( 'elementor/icons_manager/additional_tabs', 'betop_elementor_add_custom_icons_new', 9999999, 1 );


function function_stretch_column( $element ) {

	$element->add_control(
		'stretch_column',
		[
			'label' => __( 'Display Flex:', 'homepress-elementor' ),
            'type' => \Elementor\Controls_Manager::SELECT,
            'default' => 'none',
            'options' => [
				'none' => __( 'None', 'homepress-elementor' ),
                'right' => __( 'To Right', 'homepress-elementor' ),
                'left' => __( 'To Left', 'homepress-elementor' ),
            ],
            'prefix_class' => 'stretch-to-',
            'description' => 'Block will be fully flexed to the selected side',
        ]
    );

}
add_action( 'elementor/element/column/section_advanced/before_section_end', 'function_stretch_column', 10, 2 );
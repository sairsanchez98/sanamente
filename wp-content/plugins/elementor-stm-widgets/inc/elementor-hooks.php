<?php
add_filter( 'elementor/editor/localize_settings', function ( $config ) {
    $config['default_schemes']['color']['items'] = [
        '1' => '#000',
        '2' => '#515254',
        '3' => '#515254',
        '4' => '#ffc03f'
    ];

    $config['default_schemes']['typography']['items'] = [
        '1' => [
            'font_family' => 'Poppins',
            'font_weight' => '700',
        ],
        '2' => [
            'font_family' => 'Poppins',
            'font_weight' => '600',
        ],
        '3' => [
            'font_family' => 'Open Sans',
            'font_weight' => '400',
        ],
        '4' => [
	        'font_family' => 'Open Sans',
	        'font_weight' => '400',
        ]
    ];

    return $config;
} );





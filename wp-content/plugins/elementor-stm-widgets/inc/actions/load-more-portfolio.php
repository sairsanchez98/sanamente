<?php
add_action( 'wp_ajax_betop_load_more_portfolio', 'betop_load_more_portfolio' );
add_action( 'wp_ajax_nopriv_betop_load_more_portfolio', 'betop_load_more_portfolio' );
if ( !function_exists( 'betop_load_more_portfolio' ) ) {
	function betop_load_more_portfolio()
	{
	    check_ajax_referer('betop_load_more_portfolio', 'security');
		$count = !empty($_POST[ 'count' ]) ? intval($_POST[ 'count' ]) : 9;
		$per_page = !empty($_POST[ 'per_page' ]) ? intval($_POST[ 'count' ]) : 9;
		$offset = !empty($_POST[ 'offset' ]) ? intval($_POST[ 'offset' ]) : 9;
		$view_type = !empty($_POST[ 'view_type' ]) ? sanitize_text_field($_POST[ 'view_type' ]) : 'grid';
		$post_type = !empty($_POST[ 'post_type' ]) ? sanitize_text_field($_POST[ 'post_type' ]) : 'post';
		$post_status = $post_type == 'events' ? 'future, publish' : 'publish';
		$args = array(
			'post_type' => $post_type,
			'numberposts' => $per_page,
			'post_status' => $post_status,
			'offset' => $offset
		);
		$portfolios = get_posts( $args );
		$i = 0;
		foreach ( $portfolios as $portfolio ) {
			setup_postdata( $portfolio );
			$id = $portfolio->ID;
			$i++;
			$i = $i > 8 ? 1 : $i;
			require( ELEMENTOR_SM_WIDGETS__DIR__ . '/widgets/partials/post-type-templates/' . $post_type . '-' . $view_type . '.php' );
		}
		wp_reset_postdata();
		$response = array();
		$response[ 'count' ] = $count;
		$response[ 'per_page' ] = $per_page;
		$response[ 'offset' ] = $offset;
		$response[ 'view_type' ] = $view_type;
		$response[ 'posts' ] = ob_get_contents();
		ob_clean();
		wp_send_json( $response );
	}
}


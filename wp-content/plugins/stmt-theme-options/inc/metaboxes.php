<?php
add_filter('stm_wpcfto_boxes', function ($boxes) {

    $boxes['stmt_portfolio_setting'] = array(
        'post_type' => array('portfolio'),
        'label' => esc_html__('Post settings', 'stmt_theme_options'),
    );
    $boxes['stmt_product_setting'] = array(
        'post_type' => array('product'),
        'label' => esc_html__('Group membership limit', 'stmt_theme_options'),
    );
    $boxes['stmt_page_settings'] = array(
        'post_type' => array('page'),
        'label' => esc_html__('Page options', 'stmt_theme_options'),
    );
    $boxes['stmt_event_member'] = array(
        'post_type' => array('event_member'),
        'label' => esc_html__('Member info', 'stmt_theme_options'),
    );
    $boxes['stmt_services'] = array(
        'post_type' => array('services'),
        'label' => esc_html__('Services', 'stmt_theme_options'),
    );

    return $boxes;
});

add_filter('stm_wpcfto_fields', function ($fields) {

    $fields['stmt_portfolio_setting'] = array(

        'stmt_media_section' => array(
            'fields' => array(
                'stmt_media_repeater' => array(
                    'type' => 'repeater',
                    'label' => esc_html__( 'Post gallery', 'stmt_theme_options' ),
                    'fields' => array(
                        'title' => array(
                            'type' => 'text',
                            'label' => esc_html__( 'Title', 'stmt_theme_options' ),
                        ),
                        'image' => array(
                            'type' => 'image',
                            'label' => esc_html__( 'Image', 'stmt_theme_options' ),
                        ),
                        'description' => array(
                            'type' => 'textarea',
                            'label' => esc_html__( 'Description', 'stmt_theme_options' ),
                        ),
                    )
                ),
            )
        ),

    );

    $fields['stmt_product_setting'] = array(

        'stmt_group_section' => array(
            'name' => esc_html__('Group membership limit', 'stmt_theme_options'),
            'fields' => array(
                'stmt_is_repeatable' => array(
                    'type' => 'checkbox',
                    'label' => esc_html__('Is repeatable training', 'stmt_theme_options'),
                ),
                'stmt_membership_limit' => array(
                    'type' => 'number',
                    'label' => esc_html__('Group membership limit', 'stmt_theme_options'),
                    'dependency' => array(
                        'key' => 'stmt_is_repeatable',
                        'value' => 'not_empty'
                    )
                ),
                'stmt_training_duration' => array(
                    'type' => 'select',
                    'label' => esc_html__('Training duration (in days)', 'stmt_theme_options'),
                    'options' => array(
                        'weekly' => esc_html__('Weekly', 'stmt_theme_options'),
                        'monthly' => esc_html__('Monthly', 'stmt_theme_options')
                    ),
                    'dependency' => array(
                        'key' => 'stmt_is_repeatable',
                        'value' => 'not_empty'
                    )
                ),
                'stmt_deadline' => array(
                    'type' => 'date',
                    'label' => esc_html__('Booking deadline', 'stmt_theme_options'),
                ),
            ),
        ),

    );

    $fields['stmt_page_settings'] = array(
        'stmt_header_section' => array(
            'name' => esc_html__('Header settings', 'stmt_theme_options'),
            'fields' => array(
                'stmt_title_box' => array(
                    'type' => 'checkbox',
                    'label' => esc_html__('Hide title box', 'stmt_theme_options'),
                ),
                'stmt_title_box_color' => array(
                    'type' => 'color',
                    'label' => esc_html__('Title box color', 'stmt_theme_options'),
                ),
                'stmt_title_background_color' => array(
                    'type' => 'color',
                    'label' => esc_html__('Title box background color', 'stmt_theme_options'),
                ),
                'stmt_title_background_image' => array(
                    'type' => 'image',
                    'label' => esc_html__('Title box background image', 'stmt_theme_options'),
                ),
            )
        ),
    );

    $fields['stmt_services'] = array(
        'stmt_services_icon' => array(
            'name' => esc_html__('Services icon', 'stmt_theme_options'),
            'fields' => array(
                'stmt_service_icon' => array(
                    'type' => 'image',
                    'label' => esc_html__('Service icon', 'stmt_theme_options'),
                ),
            )
        ),
    );

    $fields['stmt_event_member'] = array(
        'stmt_header_section' => array(
            'name' => esc_html__('Member info', 'stmt_theme_options'),
            'fields' => array(
                'stmt_member_name' => array(
                    'type' => 'text',
                    'label' => esc_html__('Name', 'stmt_theme_options'),
                ),
                'stmt_member_email' => array(
                    'type' => 'text',
                    'label' => esc_html__('Email', 'stmt_theme_options'),
                ),
                'stmt_member_phone' => array(
                    'type' => 'text',
                    'label' => esc_html__('Phone', 'stmt_theme_options'),
                ),
            )
        ),
    );

    return $fields;
});
<?php
if( !function_exists( 'betop_get_option' ) ) {
    function betop_get_option( $option_id, $default )
    {
        if( class_exists( 'ReduxFramework' ) ) {
            global $theme_options;

            if( ( isset( $theme_options[ $option_id ] ) && !empty( $theme_options[ $option_id ] ) ) || ( isset( $theme_options[ $option_id ] ) && $theme_options[ $option_id ] === '0' ) ) {
                return $theme_options[ $option_id ];
            }
            else {
                return $default;
            }
        }
        else {
            return $default;
        }
    }
}
function betop_responsive_styles()
{
    $tablet_styles = '';
    $mobile_styles = '';
    for( $i = 1; $i <= 6; $i++ ) {
        $tablet_fonts = betop_get_option( 'typosettings-h' . $i . '-typo_tablet', '' );
        if( !empty( $tablet_fonts ) ) {
            $tablet_styles .= 'h' . $i . ', .h' . $i . '{';
            foreach( $tablet_fonts as $key => $value ) {
                if( $key == 'google' || $key == 'subset' ) continue;
                if( !empty( $value ) ) $tablet_styles .= $key . ':' . $value . '; ';
            }
            $tablet_styles .= '}';
        }
        $mobile_fonts = betop_get_option( 'typosettings-h' . $i . '-typo_mobile', '' );
        if( !empty( $mobile_fonts ) ) {
            $mobile_styles .= 'h' . $i . ', .h' . $i . '{';
            foreach( $mobile_fonts as $key => $value ) {
                if( $key == 'google' || $key == 'subset' ) continue;
                if( !empty( $value ) ) $mobile_styles .= $key . ':' . $value . '; ';
            }
            $mobile_styles .= '}';
        }
    }
    $tablet_body_fonts = betop_get_option( 'typosettings-body-typo_tablet', '' );
    if( !empty( $tablet_body_fonts ) ) {
        $tablet_styles .= ' body, .elementor-widget-text-editor, input, textarea, .woocommerce-ordering select, .man_cart_block a, .normal-font, .elementor-header .sub-menu a, .archive-product-meta a h2{';
        foreach( $tablet_body_fonts as $key => $value ) {
            if( $key == 'google' || $key == 'subset' ) continue;
            if( !empty( $value ) ) $tablet_styles .= $key . ':' . $value . '; ';
        }
        $tablet_styles .= '}';
    }
    $mobile_body_fonts = betop_get_option( 'typosettings-body-typo_mobile', '' );
    if( !empty( $mobile_body_fonts ) ) {
        $mobile_styles .= ' body, .elementor-widget-text-editor, input, textarea, .woocommerce-ordering select, .man_cart_block a, .normal-font, .elementor-header .sub-menu a, .archive-product-meta a h2{';
        foreach( $mobile_body_fonts as $key => $value ) {
            if( $key == 'google' || $key == 'subset' ) continue;
            if( !empty( $value ) ) $mobile_styles .= $key . ':' . $value . '; ';
        }
        $mobile_styles .= '}';
    }
    $responsive_styles = '';
    $responsive_styles .= '<style type="text/css">';
    if( !empty( $tablet_styles ) ) {
        $responsive_styles .= '@media (max-width: 1024px){ ' . $tablet_styles . ' } ';
    }
    if( !empty( $mobile_styles ) ) {
        $responsive_styles .= '@media (max-width: 480px){ ' . $mobile_styles . ' } ';
    }
    $responsive_styles .= '</style>';
    return $responsive_styles;
}

add_filter( 'body_class', function( $classes ) {
    $theme_option_class = 'stm_theme_options';
    $classes[] = $theme_option_class;
    return $classes;
} );

add_filter( 'stm_wpcfto_ms_icons', 'betop_add_theme_icons' );

function betop_add_theme_icons( $icons )
{
    $theme_icons = array(
        'mc-working-hours',
        'mc-office-phone',
        'mc-globe',
        'mc-mail',
        'mc-tag',
        'mc-minus',
        'mc-plus',
        'mc-chevron-left-1',
        'mc-chevron-right',
        'mc-mailbox',
        'mc-couple',
        'mc-head',
        'mc-teen',
        'mc-article',
        'mc-tribune',
        'mc-video',
        'mc-book',
        'mc-fitness',
        'mc-running',
        'mc-stationary-bicycle',
        'mc-waist',
        'mc-shopping-bag',
        'mc-zoom',
        'mc-left',
        'mc-right',
        'mc-groceries',
        'mc-certificate',
        'mc-play',
        'mc-hamburger',
        'mc-meditation',
        'mc-padmasana',
        'mc-warrior',
        'mc-yog-lotus',
        'mc-yog-hamsa',
        'mc-yog-book',
        'mc-yog-hand',
        'mc-yog-om',
        'mc-yog-stress',
        'mc-yog-ballon',
        'mc-yog-incense',
        'mc-yog-hand-1',
        'mc-yog-hand-2',
        'mc-yog-meditation',
        'mc-yog-chakra',
        'mc-yog-placeholder',
        'mc-yog-bosu-ball',
        'mc-yog-flower',
        'mc-yog-hand-3',
        'mc-yog-esteem',
        'mc-yog-happiness',
        'mc-yog-necklace',
        'mc-yog-peace-sign',
        'mc-yog-stone',
        'mc-yog-yin-yang',
        'mc-yog-yoga',
        'mc-yog-clothes',
        'mc-yog-incense-1',
        'mc-yog-candles',
        'mc-yog-heart',
        'mc-yog-body',
        'mc-yog-drop',
        'mc-yog-listen',
        'mc-yog-head',
        'mc-yog-carpet',
        'mc-yog-droplet',
        'mc-yog-ball',
        'mc-yog-teapot',
        'mc-yog-chakra-1',
        'mc-yog-friends',
        'mc-yog-hand-4',
        'mc-yog-bamboo',
        'mc-yog-chakra-2',
        'mc-yog-hand-5',
        'mc-yog-buddha',
        'mc-yog-heart-1',
        'mc-yog-pants',
        'mc-yog-bowl',
        'mc-yog-resistance',
        'mc-yog-plant',
        'mc-yog-chatting',
        'mc-yog-head-1',
        'mc-yog-calendar',
        'mc-calendar',
        'mc-burner',
        'mc-chakra-1',
        'mc-chakra-2',
        'mc-chakra-3',
        'mc-chakra',
        'mc-flower',
        'mc-incense',
        'mc-lotus-1',
        'mc-lotus',
        'mc-om-1',
        'mc-om',
        'mc-stones',
        'mc-teapot',
        'mc-yoga-1',
        'mc-yoga',
        'mc-coin',
        'mc-clock',
        'mc-smile',
        'mc-medal',
        'mc-quote-left',
        'mc-quote-right',
    );
    $icons = array_merge($theme_icons, $icons);
    return $icons;
}
<?php

add_filter('stm_wpcfto_term_meta_fields', function ($fields) {

    $fields['coach'] = array(
        'course_image' => array(
            'label' => esc_html__('Category Image', 'stmt_theme_options'),
            'type' => 'image',
        ),
        'course_icon' => array(
            'label' => esc_html__('Category Icon', 'stmt_theme_options'),
            'type' => 'icon',
        ),
    );

    return $fields;

}, 1000, 1);
<?php

if (!defined('ABSPATH')) exit; //Exit if accessed directly


//require_once STMT_TO_DIR . '/post_type/metaboxes/metabox.php';

class STMT_TO_Post_Type
{
	function __construct()
	{
		add_action('init', array($this, 'post_types_init'), -1);
	}

	function post_types_init()
	{
		$post_types = array(
			'Portfolio' => 'portfolio',
			'Events' => 'events',
			'Services' => 'services'
		);
		foreach ($post_types as $post_type => $key) {
			$name = betop_get_option($key . '_name', $post_type);
			$plural_name = betop_get_option($key . '_plural_name', $post_type);
			$archive = betop_get_option($key . '_enable_archive', false);
			$archive = $archive == 1 ? true : false;
			register_post_type($post_type, array(
				'labels' => array(
					'name' => sprintf(__('%s', 'betop'), $name),
					'singular_name' => sprintf(__('%s', 'betop'), $name),
					'add_new' => __('Add New', 'betop'),
					'add_new_item' => sprintf(__('Add New %s', 'betop'), $name),
					'edit_item' => sprintf(__('Edit %s', 'betop'), $name),
					'new_item' => sprintf(__('New %s', 'betop'), $name),
					'view_item' => sprintf(__('View %s', 'betop'), $name),
					'search_items' => sprintf(__('Search ', 'betop'), $name),
					'not_found' => sprintf(__('%s Not found', 'betop'), $name),
					'not_found_in_trash' => sprintf(__('%s Not found in trash', 'betop'), $name),
					'parent_item_colon' => '',
					'menu_name' => sprintf(__('%s', 'betop'), $plural_name),

				),
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'query_var' => true,
				'rewrite' => true,
				'capability_type' => 'post',
				'has_archive' => $archive,
				'hierarchical' => false,
				'menu_position' => null,
				'supports' => array('title', 'thumbnail', 'excerpt', 'editor')
			));
		}
		register_post_type('event_member', array(
			'labels' => array(
				'name' => __('Members', 'betop'),
				'singular_name' => __('Member', 'betop'),
				'add_new' => __('Add New', 'betop'),
				'add_new_item' => __('Add New members', 'betop'),
				'edit_item' => __('Edit members', 'betop'),
				'new_item' => __('New members', 'betop'),
				'view_item' => __('View member', 'betop'),
				'search_items' => __('Search members', 'betop'),
				'not_found' => __('Members not found', 'betop'),
				'not_found_in_trash' => __('Members Not found in trash', 'betop'),
				'parent_item_colon' => '',
				'menu_name' => __('Members', 'betop'),
			),
			'public' => true,
			'exclude_from_search'   => true,
			'publicly_queryable'    => (bool)false,
			'show_ui' => true,
			'show_in_menu'          => 'edit.php?post_type=events',
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports'              => array( 'title' )
		));
	}

}

new STMT_TO_Post_Type();

require_once STMT_TO_DIR . '/post_type/taxonomies.php';
<?php

if ( ! defined( 'ABSPATH' ) ) exit; //Exit if accessed directly


class STMT_TO_Taxonomies
{
	function __construct()
	{
		add_action('init', array($this, 'taxonomies_init'), -1);
	}

	function taxonomies_init()
	{
		$taxonomies = $this->taxonomies();
		foreach ($taxonomies as $taxonomy => $taxonomy_args) {
			register_taxonomy($taxonomy, $taxonomy_args['post_type'], $taxonomy_args['args']);
		}
	}

	function taxonomies()
	{
		return apply_filters('stmt_to_taxonomies', array(
			'coach' => array(
				'post_type' => 'product',
				'args' => array(
					'hierarchical'          => true,
					'update_count_callback' => '_wc_term_recount',
					'label'                 => __( 'Categories', 'betop' ),
					'labels'                => array(
						'name'              => __( 'Coach categories', 'betop' ),
						'singular_name'     => __( 'Coach category', 'betop' ),
						'menu_name'         => _x( 'Coach categories', 'Admin menu name', 'betop' ),
						'search_items'      => __( 'Search coach categories', 'betop' ),
						'all_items'         => __( 'All coach categories', 'betop' ),
						'parent_item'       => __( 'Parent coach category', 'betop' ),
						'parent_item_colon' => __( 'Parent coach category:', 'betop' ),
						'edit_item'         => __( 'Edit coach category', 'betop' ),
						'update_item'       => __( 'Update coach category', 'betop' ),
						'add_new_item'      => __( 'Add new coach category', 'betop' ),
						'new_item_name'     => __( 'New coach category name', 'betop' ),
						'not_found'         => __( 'No coach categories found', 'betop' ),
					),
					'show_ui'               => true,
					'query_var'             => true,
					'capabilities'          => array(
						'manage_terms' => 'manage_product_terms',
						'edit_terms'   => 'edit_product_terms',
						'delete_terms' => 'delete_product_terms',
						'assign_terms' => 'assign_product_terms',
					),
					'rewrite'               => true
				)
			)
		));
	}

}

new STMT_TO_Taxonomies();
<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package betop
 */

get_header();
?>

	<div class="container post-container">
		<div class="row">
			<div class="col-md-12 man_main_sidebar">

				<div id="primary-content" class="content-area">
					<main id="main" class="site-main">

						<?php
						while ( have_posts() ) :
							the_post();

							get_template_part( 'template-parts/content', get_post_type() );

							// If comments are open or we have at least one comment, load up the comment template.

						endwhile; // End of the loop.
						?>
						<?php get_template_part( 'partials/post', 'tags' ); ?>
					</main>
				</div>

			</div>

		</div>

	</div>

<?php

get_footer();

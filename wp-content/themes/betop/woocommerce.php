<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package betop
 */
get_header();
?>


	<div class="container">
		<div class="row">
			<?php if ( is_singular( 'product' ) ): ?>
				<div class="col-md-12 man_main_sidebar">

					<div class="content-area">

						<?php woocommerce_content(); ?>

					</div>

				</div>
			<?php else: ?>
				<?php
				$show_sidebar = betop_get_option( 'shopsettings-fullwidth', '1' );
				$column_width = 'col-lg-9';
				if ( $show_sidebar == '0' || !is_active_sidebar( 'sidebar-woo' ) ) {
					$column_width = 'col-lg-12';
				}
				$sidebar_position = betop_get_option( 'shop-sidebar-position', 'left' );
				?>
				<?php if ( $show_sidebar != '0' && $sidebar_position == 'left' && is_active_sidebar( 'sidebar-woo' ) ): ?>
					<div class="col-lg-3 col-md-12 woo-sidebar">
						<?php dynamic_sidebar( 'sidebar-woo' ); ?>
					</div>
				<?php endif; ?>
				<div class="<?php echo esc_attr( $column_width ); ?> col-md-12 man_main_sidebar">

					<div class="content-area">

						<?php wc_get_template( 'archive-product.php' ); ?>

					</div>

				</div>
				<?php if ( $show_sidebar != '0' && $sidebar_position == 'right' && is_active_sidebar( 'sidebar-woo' ) ): ?>
					<div class="col-lg-3 col-md-12 woo-sidebar">
						<?php dynamic_sidebar( 'sidebar-woo' ); ?>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>

	</div>

<?php

get_footer();
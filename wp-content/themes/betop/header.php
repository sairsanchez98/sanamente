<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package betop
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="profile" href="//gmpg.org/xfn/11">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?> >

<?php
$preloader = betop_get_option('preloader-sw', false)
?>

<div id="page" class="man_page <?php if( !class_exists( 'ReduxFramework' ) ) {
    echo esc_attr( 'man_page_default' );
}
if( $preloader ) echo esc_attr( 'unload' );
?>">
    <?php
    $show_title_box = true;
    $title_box_global = betop_get_option( 'hide_title_box', 0 );
    if( $title_box_global == 0 ) {
        $show_title_box = true;
    } else $show_title_box = false;
    if( get_post_meta( get_the_id(), 'stmt_title_box', true ) == 'on' || is_singular( 'product' ) ) {
        $show_title_box = false;
    }
    if( function_exists( 'hfe_render_header' ) && get_option( 'betop_content_imported' ) == '1' ) {
        hfe_render_header();
    } else {
        get_template_part( 'template-parts/header/default' );
    }
    if( !is_404() && $show_title_box ) {
        get_template_part( 'template-parts/header/intro' );
    }
    ?>
    <div id="content" class="site-content <?php if( !$show_title_box ) echo esc_attr( 'no-title-box' ); ?>">




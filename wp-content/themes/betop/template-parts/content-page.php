<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package betop
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content ">
		<?php
		the_content();
		$args = array(
			'before'           => '<div class="post-pagination">',
			'after'            => '</div>',
			'link_before'      => '<span class="page-num">',
			'link_after'       => '</span>',
			'next_or_number'   => 'number',
			'nextpagelink'     => esc_html__('Next', 'betop'),
			'previouspagelink' => esc_html__('Prev', 'betop'),
			'pagelink'         => '%',
			'echo'             => 1,
		);
		wp_link_pages($args);
		?>
	</div>

</article>

<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package betop
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<?php
	if ( is_singular( 'product' ) ) { ?>
		<div class="main_single_page">
			<?php
			the_content();
			$args = array(
				'before' => '<div class="post-pagination">',
				'after' => '</div>',
				'link_before' => '<span class="page-num">',
				'link_after' => '</span>',
				'next_or_number' => 'number',
				'nextpagelink' => esc_html__( 'Next', 'betop' ),
				'previouspagelink' => esc_html__( 'Prev', 'betop' ),
				'pagelink' => '%',
				'echo' => 1,
			);
			wp_link_pages( $args );
			?>
		</div>
		<?php
	} elseif ( is_singular() ) {
		?>

		<div class="main_single_page">
			<div class="post-date pc head-font">
				<?php the_date(); ?>
			</div>
			<?php
			$hide_title_box = betop_get_option( 'hide_title_box', 0 );
			if ( $hide_title_box != 0 ):
				?>
				<h1><?php the_title(); ?></h1>
			<?php endif; ?>
			<?php if ( has_post_thumbnail() ): ?>
				<div class="single-thumbnail">
					<?php the_post_thumbnail( 'betop_post-thumbnail' ); ?>
				</div>
			<?php endif; ?>
			<div class="coach-post-content">
				<?php
				the_content();
				$args = array(
					'before' => '<div class="post-pagination">',
					'after' => '</div>',
					'link_before' => '<span class="page-num">',
					'link_after' => '</span>',
					'next_or_number' => 'number',
					'nextpagelink' => esc_html__( 'Next', 'betop' ),
					'previouspagelink' => esc_html__( 'Prev', 'betop' ),
					'pagelink' => '%',
					'echo' => 1,
				);
				wp_link_pages( $args );
				?>
			</div>
			<?php
			if ( is_singular('post') ) {
				get_template_part( 'partials/post/post', 'after' );
			}
			elseif(is_singular('services')){
				get_template_part( 'partials/post', 'tags' );
			}

			?>
		</div>

		<?php
	} else {
		?>


		<div class="man_news_item_list">
			<?php if ( has_post_thumbnail() ) {
				?>
				<div class="man_news_item_photo">
					<a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_post_thumbnail( 'woocommerce_thumbnail' ); ?></a>
				</div>
				<?php
			}
			?>

			<div class="man_news_item_cont_list<?php if ( !has_post_thumbnail() ) {
				echo ' man_news_item_cont_list_full';
			} ?>">
				<div class="man_news_item_title">
					<?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
				</div>
				<div class="man_news_item_txt">
					<?php
					the_excerpt();

					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'betop' ),
						'after' => '</div>',
					) );
					?>
				</div>
				<div class="man_news_item_tech">
					<a href="<?php echo esc_url( get_permalink() ); ?>" class="btn">Read More<i
								class="ti ti-arrow-right"></i></a>
				</div>


			</div>

		</div>

		<?php
	}
	?>


</article>

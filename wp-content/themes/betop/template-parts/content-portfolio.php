<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package betop
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="main_single_page">
		<?php
		$gallery = get_post_meta( get_the_ID(), 'stmt_media_repeater', true );
		$gallery = json_decode( $gallery );
		if ( !empty( $gallery ) ): ?>
			<div class="portfolio-gallery owl-carousel">
				<?php foreach ( $gallery as $media ): ?>
					<div class="portfolio-gallery__item">
						<?php betop_get_the_post_thumbnail( $media->id, 'stm-single-portfolio' ); ?>
						<div class="portfolio-gallery__item__description">
							<?php if ( !empty( $media->title ) ): ?>
								<h5><?php echo esc_html( $media->title ); ?></h5>
							<?php endif; ?>
							<?php if ( !empty( $media->description ) ): ?>
								<div class="portfolio-description"><?php echo esc_html( $media->description ); ?></div>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php elseif ( has_post_thumbnail() ): ?>
			<div class="single-thumbnail">
				<?php
				$thumbnail_id = get_post_thumbnail_id( get_the_id() );
				betop_get_the_post_thumbnail( $thumbnail_id, 'stm-single-portfolio' );
				?>
			</div>
		<?php endif; ?>
		<div class="portfolio-content">
			<?php
			the_content();
			$args = array(
				'before' => '<div class="post-pagination">',
				'after' => '</div>',
				'link_before' => '<span class="page-num">',
				'link_after' => '</span>',
				'next_or_number' => 'number',
				'nextpagelink' => esc_html__( 'Next', 'betop' ),
				'previouspagelink' => esc_html__( 'Prev', 'betop' ),
				'pagelink' => '%',
				'echo' => 1,
			);
			wp_link_pages( $args );
			?>
		</div>
	</div>


</article>

<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package betop
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="main_single_page">
		<?php
		$hide_title_box = betop_get_option( 'hide_title_box', 0 );
		if ( $hide_title_box != 0 ):
			?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<?php if ( has_post_thumbnail() ): ?>
			<div class="single-thumbnail">
				<?php the_post_thumbnail( 'betop_post-thumbnail' ); ?>
			</div>
		<?php endif; ?>
		<div class="coach-post-content">
			<?php
			the_content();
			$args = array(
				'before' => '<div class="post-pagination">',
				'after' => '</div>',
				'link_before' => '<span class="page-num">',
				'link_after' => '</span>',
				'next_or_number' => 'number',
				'nextpagelink' => esc_html__( 'Next', 'betop' ),
				'previouspagelink' => esc_html__( 'Prev', 'betop' ),
				'pagelink' => '%',
				'echo' => 1,
			);
			wp_link_pages( $args );
			?>
		</div>
		<?php
		if ( is_singular( 'post' ) ) {
			get_template_part( 'partials/post/post', 'after' );
		}
		?>
		<?php get_template_part( 'partials/post', 'tags' ); ?>
	</div>
</article>

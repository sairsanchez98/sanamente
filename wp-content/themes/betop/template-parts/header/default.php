<header id="masthead" class="site-header default-header">
	<div class="container">
		<div class="row">
			<div class="col-sm-3 col-md-3 col-lg-2">
				<div class="site-logo">
					<a href="<?php echo esc_url(get_home_url()); ?>" class="head-font">
						<?php echo esc_html(get_bloginfo()); ?>
					</a>
				</div>
			</div>
			<div class="col-sm-9 col-md-9 col-lg-10">
				<div class="site-branding">

					<div id="elementor-header-primary" class="elementor-header">
						<button class="sm_menu_toggle pbcc">
							<span></span>
							<span></span>
							<span></span>
						</button>
						<div id="sm_menu" class="sm_menu">
							<span class="close-menu"></span>
							<nav itemtype="//schema.org/SiteNavigationElement" itemscope="itemscope" id="elementor-navigation" class="elementor-navigation">
								<?php
								wp_nav_menu( array(
									'container'      => false,
									'menu_id'        => 'primary',
									'menu_class'     => 'sm_nav_menu',
									'echo'           => true,
								) );
								?>
							</nav><!-- #site-navigation -->
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>



</header><!-- #masthead -->
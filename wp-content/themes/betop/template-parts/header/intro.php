<?php
$post_id = get_the_ID();
$page_styles = false;
$styles = '';
$title_styles = '';
$color = get_post_meta($post_id, 'stmt_title_box_color', true);
$background_image = get_post_meta($post_id, 'stmt_title_background_image', true);
$background_color = get_post_meta($post_id, 'stmt_title_background_color', true);
if(!empty($color)){
	$title_styles .= 'color: ' . $color . '; ';
}
if(!empty($background_image)){
	$styles .= 'background-image: url(' . wp_get_attachment_url($background_image) . '); ';
	$page_styles = true;
}
if(!empty($background_color)){
	$styles .= 'background-color: ' . $background_color . '; ';
	$page_styles = true;
}
?>
<div class="man_intro man_intro_default man_image_bck" style="<?php echo esc_attr($styles); ?>">

	<div class="main_intro_cont">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 style="<?php echo esc_attr($title_styles); ?>">
						<?php
						if (is_search()) {
							printf(esc_html__('Search: %s', 'betop'), '<span>' . get_search_query() . '</span>');
						} elseif (class_exists('WooCommerce') && is_shop()) {
							woocommerce_page_title();
						} elseif (is_category() || is_archive()) {
							echo esc_html(strip_tags(get_the_archive_title()));
						}
						else {
							if (!is_front_page() && is_home()) {
								echo get_the_title(get_option('page_for_posts'));
							} elseif (is_front_page()) {
								echo esc_html(get_bloginfo());
							} else the_title();
						}
						?>
					</h1>
				</div>
			</div>
		</div>

	</div>

</div>
<?php
$show_sidebar = betop_get_option('blogsettings-fullwidth', '1');
$column_width = 'col-lg-9';
if($show_sidebar == '0'){
	$column_width = 'col-lg-12';
}
$sidebar_position = betop_get_option('blog-sidebar-position', 'right');
?>
<div class="archive-wrap">
	<div class="container">
		<div class="row">
			<?php if($show_sidebar != '0' && $sidebar_position == 'left'): ?>
				<div class="col-sm-12 col-md-12 col-lg-3 man_sidebar">
					<?php dynamic_sidebar('sidebar-main'); ?>
				</div>
			<?php endif; ?>
			<div class="col-sm-12 col-md-12 <?php echo esc_attr($column_width); ?> sidebar-<?php echo esc_attr($sidebar_position); ?>">
				<?php
				while ( have_posts() ) {
					the_post();
					get_template_part('partials/post/list/post-content');
				}
				the_posts_pagination(array(
					'prev_text'    => esc_html__('Prev', 'betop'),
					'next_text'    => esc_html__('Next', 'betop'),
				));
				?>
			</div>
			<?php if($show_sidebar != '0' && $sidebar_position == 'right'): ?>
			<div class="col-sm-12 col-md-12 col-lg-3 man_sidebar">
				<?php dynamic_sidebar('sidebar-main'); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package betop
 */
get_header();
$container_class_shop = 'col-lg-9';
if ( !is_active_sidebar( 'sidebar-woo' ) ) {
	$container_class_shop = 'col-lg-12';
}
?>
<?php if ( class_exists( 'WooCommerce' ) && ( is_product_category() || is_shop() ) ): ?>
	<div class="container">
		<div class="row">
			<?php if ( is_active_sidebar( 'sidebar-woo' ) ): ?>
				<div class="col-lg-3 col-md-12 man_sidebar">
					<?php dynamic_sidebar( 'sidebar-woo' ); ?>
				</div>
			<?php endif; ?>
			<div class="<?php echo esc_attr( $container_class_shop ); ?> col-md-12 man_main_sidebar">

				<div id="primary-content" class="content-area">
					<main id="main" class="site-main">

						<?php
						while ( have_posts() ) :
							the_post();

							get_template_part( 'template-parts/content', 'page' );

						endwhile;
						?>

					</main>
				</div>

			</div>


		</div>


	</div>

<?php else: ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12 man_main_sidebar">

				<div id="primary-content" class="content-area">
					<main id="main" class="site-main">

						<?php
						while ( have_posts() ) :
							the_post();

							get_template_part( 'template-parts/content', 'page' );

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile;
						?>

					</main>
				</div>

			</div>

		</div>

	</div>
<?php endif; ?>
<?php

get_footer();


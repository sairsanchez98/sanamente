<?php
/**
 * Template Name: Full width page
 */
get_header();

?>
	<div id="primary-content" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) :
				the_post();

				the_content();
				$args = array(
					'before'           => '<div class="post-pagination">',
					'after'            => '</div>',
					'link_before'      => '<span class="page-num">',
					'link_after'       => '</span>',
					'next_or_number'   => 'number',
					'nextpagelink'     => esc_html__('Next', 'betop'),
					'previouspagelink' => esc_html__('Prev', 'betop'),
					'pagelink'         => '%',
					'echo'             => 1,
				);
				wp_link_pages($args);

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main>
	</div>
<?php

get_footer();


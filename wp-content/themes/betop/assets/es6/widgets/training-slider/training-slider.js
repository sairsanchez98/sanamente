(function ($) {
	$(window).load(function(){
		betopTrainingSlider();
	});

	function betopTrainingSlider() {
		let owlRtl = false;

		if ($('body').hasClass('rtl')) {
			owlRtl = true;
		}

		$('.training-slider').each(function(){
			let prev = $(this).find('.slider-content-area .prev');
			let next = $(this).find('.slider-content-area .next');
			let $this = $(this).find('.training-slider-wrap');
			let owl = $this.owlCarousel({
				rtl: owlRtl,
				navContainer: $(this).find('.slider-content-area .custom-navs'),
				nav: false,
				dots: false,
				items: 3,
				loop: true,
				slideBy: 1,
				responsive: {
					0: {
						items: 1,
					},
					480: {
						items: 2,
					},
					1024: {
						items: 3,
					}
				}
			});
		});
	}
})(jQuery);
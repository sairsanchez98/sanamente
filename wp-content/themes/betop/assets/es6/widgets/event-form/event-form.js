(function ($) {
	$(document).ready(function () {
		$('.event-form form').on('submit', function (e) {
			e.preventDefault();
			let eventId = $(this).find('.join').attr('data-event-id');
			let memberName = $(this).find('input[name=member-name]').val();
			let memberEmail = $(this).find('input[name=member-email]').val();
			let memberPhone = $(this).find('input[name=member-phone]').val();
			let form = $(this);
			$.ajax({
				url: ajaxurl,
				dataType: 'json',
				method: 'post',
				data: {
					action: 'betop_add_new_member',
					event_id: eventId,
					member_name: memberName,
					member_email: memberEmail,
					member_phone: memberPhone,
					security: window.wp_data.betop_add_new_member
				},
				beforeSend: function(){
					form.addClass('loading');
				},
				success: function success(data) {
					form.removeClass('loading');
					form.find('.message').text(data.message);
					form.find('.message').addClass(data.status + ' open');
				}
			});
		});
	});
})(jQuery);
'use strict';

(function ($) {
	$(window).load(function () {
		betop_carousel();
	});

	function betop_carousel() {
		var owlRtl = false;

		if ($('body').hasClass('rtl')) {
			owlRtl = true;
		}

		$('.slider-with-content').each(function () {

			var $this = $(this);

			var owl = $this.owlCarousel({
				rtl: owlRtl,
				nav: false,
				dots: true,
				dotsContainer: '.slider-width-content-navs ul',
				items: 1,
				autoplay: true,
				autoplayHoverPause: true,
				loop: false,
				slideBy: 1,
				animateOut: 'fadeOut',
				animateIn: 'fadeIn'
			});

			$('.slider-width-content-navs ul li').on('click', function () {
				owl.trigger('to.owl.carousel', [$(this).index(), 300]);
			});
		});
	}
})(jQuery);
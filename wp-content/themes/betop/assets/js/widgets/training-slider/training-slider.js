'use strict';

(function ($) {
	$(window).load(function () {
		betopTrainingSlider();
	});

	function betopTrainingSlider() {
		var owlRtl = false;

		if ($('body').hasClass('rtl')) {
			owlRtl = true;
		}

		$('.training-slider').each(function () {
			var prev = $(this).find('.slider-content-area .prev');
			var next = $(this).find('.slider-content-area .next');
			var $this = $(this).find('.training-slider-wrap');
			var owl = $this.owlCarousel({
				rtl: owlRtl,
				navContainer: $(this).find('.slider-content-area .custom-navs'),
				nav: false,
				dots: false,
				items: 3,
				loop: true,
				slideBy: 1,
				responsive: {
					0: {
						items: 1
					},
					480: {
						items: 2
					},
					1024: {
						items: 3
					}
				}
			});
		});
	}
})(jQuery);
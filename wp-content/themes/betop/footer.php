<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package betop
 */

?>

</div>


<?php
if ( function_exists( 'hfe_render_footer' ) && get_option('betop_content_imported') == '1' ) {
	hfe_render_footer();
} else {
	?>
	<div class="man_footer_default <?php echo has_nav_menu( 'secondary' ) ? esc_attr( 'with-menu' ) : esc_attr( 'without-menu' ); ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<nav itemtype="//schema.org/SiteNavigationElement" itemscope="itemscope"
					     id="elementor-navigation-footer" class="elementor-navigation">
						<?php
						if ( has_nav_menu( 'secondary' ) ) {
							wp_nav_menu( array(
								'container' => false,
								'theme_location' => 'secondary',
								'menu_class' => 'footer-menu',
								'depth' => 1,
								'echo' => true,
							) );
						}
						?>
					</nav>
				</div>
				<div class="col-md-12">
					<div class="stm-copyright">
						<?php
						$url = '//betop.stylemixthemes.com';
						$url2 = '//stylemixthemes.com/';
						echo esc_html__( 'Copyright &copy; ', 'betop' ) . date( 'Y' ) . '<a href="' . esc_url( $url ) . '"> ' . esc_html__( 'beTop', 'betop' ) . '</a> ' . esc_html__( 'by', 'betop' ) . ' <a href="' . esc_url( $url2 ) . '" >' . esc_html__( 'StylemixThemes.', 'betop' ) . '</a>';
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php } ?>


</div>

<?php wp_footer(); ?>


</body>
</html>

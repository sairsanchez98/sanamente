<?php
/**
 * Template Name: Page with sidebar
 */
get_header();
$content_class = 'col-lg-9';
if ( !is_active_sidebar( 'sidebar-main' ) ) {
	$content_class = 'col-lg-12';
}
?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 man_main_sidebar <?php echo esc_attr( $content_class ); ?>">

				<div id="primary-content" class="content-area">
					<main id="main" class="site-main">

						<?php
						while ( have_posts() ) :
							the_post();

							get_template_part( 'template-parts/content', 'page' );

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile; // End of the loop.
						?>

					</main>
				</div>

			</div>
			<?php if ( is_active_sidebar( 'sidebar-main' ) ): ?>
				<div class="col-sm-12 col-md-12 col-lg-3 man_sidebar">
					<?php
					dynamic_sidebar( 'sidebar-main' );
					?>
				</div>
			<?php endif; ?>

		</div>


	</div>
<?php


get_footer();


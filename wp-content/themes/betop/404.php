<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package betop
 */
get_header();
$image_404 = '';
$body_404_bg = betop_get_option('body-404-background', array());
if( !empty( $body_404_bg['url'] )){
	$image_404 = $body_404_bg['url'];
}
else {
	$image_404 = get_template_directory_uri() . '/assets/images/404.png';
}
?>
	<div class="page-404-wrap">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<img src="<?php echo esc_url($image_404); ?>" alt="<?php esc_attr_e('404', 'betop'); ?>"/>
					<h3><?php esc_html_e("Something's Missing", 'betop'); ?></h3>
					<p><?php esc_html_e('This page is missing or you assembled the link incorrectly', 'betop'); ?></p>
					<a href="<?php echo esc_url(home_url('/')); ?>" class="btn pbcc pbrc"><?php esc_html_e('Back to homepage', 'betop'); ?></a>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();
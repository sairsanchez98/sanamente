<?php
$show_sidebar = betop_get_option( 'blogsettings-fullwidth', '1' );
if ( $show_sidebar == '1' ) {
	$image_size = 'betop_post-thumbnail';
} else {
	$image_size = 'betop_post-full-container';
}
?>
<div class="main_single_page one-post-content <?php echo is_sticky() ? 'sticky-post' : ''; ?> post-format-<?php echo get_post_format( get_the_ID() ) ? get_post_format( get_the_ID() ) : 'standart'; ?>">
	<?php if ( has_post_thumbnail() ): ?>
		<div class="single-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( $image_size ); ?>
				<?php echo get_post_format( get_the_ID() ) == 'video' ? '<i class="mc-play"></i>' : ''; ?>
			</a>
			<?php if ( is_sticky() ): ?>
				<span class="head-font sticky-label pbcc"><?php esc_html_e( 'Sticky post', 'betop' ); ?></span>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<div class="post-date pc head-font">
		<?php echo esc_html(get_the_date()); ?>
	</div>
	<a href="<?php echo esc_url(get_permalink()); ?>">
		<h3 class="pc-hv"><?php the_title(); ?></h3>
	</a>
	<?php if ( is_sticky() && !has_post_thumbnail() ): ?>
		<span class="head-font sticky-label pbcc"><?php esc_html_e( 'Sticky post', 'betop' ); ?></span>
	<?php endif; ?>
	<?php if ( get_the_excerpt() ): ?>
		<div class="post-excerpt">
			<?php the_excerpt(); ?>
		</div>
	<?php endif; ?>
	<a href="<?php the_permalink(); ?>" class="btn dark read-more">
		<?php esc_html_e( 'Read More', 'betop' ); ?>
	</a>
</div>

<div class="stm_post-after">
	<div class="tags head-font">
		<?php if ( get_the_tags( get_the_id() ) ): ?>
			<i class="mc-tag pc"></i>
			<?php the_tags( '', ', ', '' ); ?>
		<?php endif; ?>
	</div>
	<div class="share-wrap">
		<?php
		if ( shortcode_exists( 'addtoany' ) ) { ?>
			<span class="head-font"><?php echo esc_html__( 'SHARE:', 'betop' ); ?></span>
			<?php echo do_shortcode( '[addtoany]' );
		}
		?>
	</div>

</div>
<?php
$repeatable_product = get_post_meta( get_the_ID(), 'stmt_is_repeatable', true );
$product_type = '';
if ( $repeatable_product && $repeatable_product == 'on' ) {
	$product_type = 'repeatable-product';
} else {
	$product_type = 'no-repeatable-product';
}
?>
<div class="single-training-wrapper">
	<?php if ( has_post_thumbnail() ): ?>
		<div class="single-training-wrapper__image">
			<?php
			the_post_thumbnail( 'betop_post-full-container' );
			?>
		</div>
	<?php endif; ?>
	<div class="single-training-wrapper__content">
		<?php if ( get_the_title() ): ?>
			<h2><?php the_title(); ?></h2>
		<?php endif; ?>
		<div class="content">
			<?php the_excerpt(); ?>
		</div>
		<div class="custom-content">
			<?php the_content(); ?>
		</div>
	</div>
	<div class="training-actions <?php echo esc_attr($product_type); ?>">
		<?php do_action( 'woocommerce_single_product_summary' ); ?>
	</div>

</div>

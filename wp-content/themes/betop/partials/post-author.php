<?php
$author_name = get_the_author_meta( 'display_name' );
$author_bio = get_the_author_meta( 'description' );
if ( !empty( $author_bio ) ):
	?>
	<div class="stm_author_box">
		<div class="stm_author_box__avatar">
			<?php echo get_avatar( get_the_author_meta( 'email' ), 133 ); ?>
		</div>
		<div class="stm_author_box__info">
			<div class="stm_author_box__name head-font">
				<?php echo esc_html( $author_name ); ?>
			</div>
			<div class="stm_author_box__content"><?php echo wp_kses_post( $author_bio ); ?></div>
		</div>
	</div>
<?php endif; ?>
<?php
$stmt_inc_path = get_template_directory() . '/inc';
if ( !isset( $redux_demo ) && file_exists( $stmt_inc_path . '/theme_options.php' ) ) {
	require_once( $stmt_inc_path . '/theme_options.php' );
}
require_once( $stmt_inc_path . '/enqueue.php' );
require_once( $stmt_inc_path . '/resize-image.php' );
require_once( $stmt_inc_path . '/setup.php' );
require_once( $stmt_inc_path . '/actions/add-new-member.php' );
require_once( $stmt_inc_path . '/custom.php' );

/*WooCommerce Setup*/
if ( class_exists( 'WooCommerce' ) ) {
	require_once( $stmt_inc_path . '/woo_setup.php' );
	require_once( $stmt_inc_path . '/coach-reservation/reservation.php' );
}

if ( is_admin() ) {
	require_once $stmt_inc_path . "/admin/tgm/registration.php";
	require_once $stmt_inc_path . "/admin/product_registration/admin.php";
}
/*WPML Options*/
if ( function_exists('icl_object_id') ) {
	require_once( $stmt_inc_path . '/wpml.php');
}

function betop_glob_wpdb(){
    global $wpdb;
    return $wpdb;
}

function betop_glob_page_now(){
    global $pagenow;
    return $pagenow;
}
<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if ( !class_exists( 'Redux' ) ) {
	return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "theme_options";

// This line is only for altering the demo. Can be easily removed.
$opt_name = apply_filters( 'theme_options/opt_name', $opt_name );

/*
 *
 * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
 *
 */

$sampleHTML = '';
if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
	Redux_Functions::initWpFilesystem();

	global $wp_filesystem;

	$sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
}

// Background Patterns Reader
$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
$sample_patterns_url = ReduxFramework::$_url . '../sample/patterns/';
$sample_patterns = array();

if ( is_dir( $sample_patterns_path ) ) {

	if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
		$sample_patterns = array();

		while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

			if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
				$name = explode( '.', $sample_patterns_file );
				$name = str_replace( '.' . end( $name ), '', $sample_patterns_file );
				$sample_patterns[] = array(
					'alt' => $name,
					'img' => $sample_patterns_url . $sample_patterns_file
				);
			}
		}
	}
}

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
	// TYPICAL -> Change these values as you need/desire
	'opt_name' => $opt_name,
	// This is where your data is stored in the database and also becomes your global variable name.
	'display_name' => $theme->get( 'Name' ),
	// Name that appears at the top of your panel
	'display_version' => $theme->get( 'Version' ),
	// Version that appears at the top of your panel
	'menu_type' => 'menu',
	//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
	'allow_sub_menu' => true,
	// Show the sections below the admin menu item or not
	'menu_title' => esc_html__( 'Theme Options', 'betop' ),
	'page_title' => esc_html__( 'Theme Options', 'betop' ),
	// You will need to generate a Google API key to use this feature.
	// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
	'google_api_key' => '',
	// Set it you want google fonts to update weekly. A google_api_key value is required.
	'google_update_weekly' => false,
	// Must be defined to add google fonts to the typography module
	'async_typography' => false,
	// Use a asynchronous font on the front end or font string
	//'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
	'admin_bar' => true,
	// Show the panel pages on the admin bar
	'admin_bar_icon' => 'dashicons-admin-tools',
	// Choose an icon for the admin bar menu
	'admin_bar_priority' => 50,
	// Choose an priority for the admin bar menu
	'global_variable' => '',
	// Set a different name for your global variable other than the opt_name
	'dev_mode' => false,
	// Show the time the page took to load, etc
	'update_notice' => true,
	// If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
	'customizer' => true,
	// Enable basic customizer support
	//'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
	//'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

	// OPTIONAL -> Give you extra features
	'page_priority' => 2,

	// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
	'page_parent' => 'themes.php',
	// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	'page_permissions' => 'manage_options',
	// Permissions needed to access the options panel.
	'menu_icon' => 'dashicons-admin-tools',
	// Specify a custom URL to an icon
	'last_tab' => '',
	// Force your panel to always open to a specific tab (by id)
	'page_icon' => 'icon-themes',
	// Icon displayed in the admin panel next to your menu_title
	'page_slug' => '',
	// Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
	'save_defaults' => true,
	// On load save the defaults to DB before user clicks save or not
	'default_show' => false,
	// If true, shows the default value next to each field that is not the default value.
	'default_mark' => '',
	// What to print by the field's title if the value shown is default. Suggested: *
	'show_import_export' => true,
	// Shows the Import/Export panel when not used as a field.

	'show_options_object' => false,

	// CAREFUL -> These options are for advanced use only
	'transient_time' => 60 * MINUTE_IN_SECONDS,
	'output' => true,
	// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
	'output_tag' => true,
	// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
	// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

	// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
	'database' => '',
	// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
	'use_cdn' => true,
	// If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

	// HINTS
	'hints' => array(
		'icon' => 'el el-question-sign',
		'icon_position' => 'right',
		'icon_color' => 'lightgray',
		'icon_size' => 'normal',
		'tip_style' => array(
			'color' => 'red',
			'shadow' => true,
			'rounded' => false,
			'style' => '',
		),
		'tip_position' => array(
			'my' => 'top left',
			'at' => 'bottom right',
		),
		'tip_effect' => array(
			'show' => array(
				'effect' => 'slide',
				'duration' => '500',
				'event' => 'mouseover',
			),
			'hide' => array(
				'effect' => 'slide',
				'duration' => '500',
				'event' => 'click mouseleave',
			),
		),
	)
);


Redux::setArgs( $opt_name, $args );


Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Main', 'betop' ),
	'id' => 'mainsettigns-body',
	'icon' => 'el el-photo',
	'customizer_width' => '450px',
	'fields' => array(
		array(
			'id' => 'body-background',
			'type' => 'background',
			'title' => esc_html__( 'Body Background', 'betop' ),
			'subtitle' => esc_html__( 'Body background with image, color, etc.', 'betop' ),
			'output' => array( 'body' ),
		),
		array(
			'id' => 'hide_title_box',
			'type' => 'checkbox',
			'title' => esc_html__( 'Hide title box', 'betop' ),
		),
		array(
			'id' => 'title-box-background',
			'type' => 'background',
			'title' => esc_html__( 'Title box background', 'betop' ),
			'subtitle' => esc_html__( 'Title box background with image, color, etc.', 'betop' ),
			'output' => array( '.man_intro' ),
		),
		array(
			'id' => 'title-box-color',
			'type' => 'color',
			'title' => esc_html__( 'Title box color', 'betop' ),
			'output' => array( '.man_intro h1' ),
		),

	),
) );


Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Preloader', 'betop' ),
	'id' => 'mainsettigns-preloader',
	'subsection' => true,
	'icon' => 'el el-time',
	'customizer_width' => '450px',
	'fields' => array(
		array(
			'id' => 'preloader-sw',
			'type' => 'switch',
			'title' => esc_html__( 'Hide preloader', 'betop' ),
			'default' => false,
		),
	)
) );

Redux::setSection( $opt_name, array(
	'title' => esc_html__( '404 ', 'betop' ),
	'id' => 'mainsettigns-404',
	'subsection' => true,
	'icon' => 'el el-photo',
	'customizer_width' => '450px',
	'fields' => array(
		array(
			'id' => 'body-404-background',
			'type' => 'media',
			'title' => esc_html__( '404 page image', 'betop' ),
		),

	),
) );

Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Colors', 'betop' ),
	'id' => 'color-settings',
	'icon' => 'el el-brush',
	'customizer_width' => '450px',
	'fields' => array(
		array(
			'id' => 'body_text_color',
			'type' => 'color',
			'title' => esc_html__( 'Body text color', 'betop' ),
			'transparent' => false,
			'output' => array(
				'color' => 'body, h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6, nav a, .stm_post-after .tags a',
			),
		),
		array(
			'id' => 'main_color',
			'type' => 'color',
			'title' => esc_html__( 'Primary color', 'betop' ),
			'transparent' => false,
			'output' => array(
				'color' => '.stm_post-after .tags a:not(:hover), .stm_post-after .tags, .star-rating, .pc, .pc-hv:hover, time, .reply a:hover, .site-content ul li:before, a, .select2-container--default .select2-results__option--highlighted[aria-selected], .select2-container--default .select2-results__option--highlighted[data-selected], .woo-sidebar .product-categories li a:hover, .woo-sidebar .widget_layered_nav ul li a:hover, .star-rating, .coach-tabs__list .catergory-tab-link.active .category-icon i, .coach-tabs__list .catergory-tab-link:hover .category-icon i, nav a:hover, .man_sidebar .widget_archive ul li a:hover, .man_sidebar .widget_meta ul li a:hover, .man_sidebar .widget_categories li:before, .woocommerce ul.products li.product .archive-product-meta > a:hover h2, .man_sidebar .widget_stm_recent_posts .widget_media a .stm-post-content h6:hover, .man_sidebar .widget_categories li a:hover, .woocommerce ul.products li.product .archive-product-meta .price, #content .widget_top_rated_products ul li a:hover',
				'border-color' => '.pbrc, .pbrc-hv:hover, ul.tabs li.active a, .wpcf7-form input[type=submit], .elementor-widget-sm-basket .stm-cart-button .btn, body.health_coach footer .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"], .btn:not(.dark):not(.sbrc), body.business .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"], body.therapist .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"], .btn.dark:hover, body.yoga .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]:hover, blockquote, body.swimming .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]:hover',
				'background-color' => '.pbcc, .pbcc-hv:hover, .man_sidebar .widget_mc4wp_form_widget, .comment-form .form-submit .submit, table thead th, .nav-links .page-numbers.current, .nav-links .page-numbers:hover, .woo-sidebar .widget_text h5:before, .woo-sidebar .widget_price_filter .ui-slider-horizontal .ui-slider-range, .woo-sidebar .widget_price_filter .ui-slider .ui-slider-handle, ul.products .archive-image-wrap .hidden-labels a, body.single-product .content-area .woocommerce-product-gallery .flex-control-nav li.active, body.single-product .content-area .woocommerce-product-gallery .flex-control-nav li:hover, .woocommerce div.product div.images .woocommerce-product-gallery__trigger:before, .woocommerce div.product div.images .woocommerce-product-gallery__trigger:after, .quantity .quantity-wrap .quantity-control:hover, .woocommerce div.product form.cart .single_add_to_cart_button:hover, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce-cart .wc-proceed-to-checkout a.checkout-button:hover, .woocommerce button.button.alt:hover, .wpcf7-form input[type=submit], .training-category a::before, body .training-block-slider .owl-dots button.active, .pbcc-before:before, .pbcc-after:after, .ui-datepicker .ui-datepicker-current-day, .ui-datepicker tbody td:not(.ui-state-disabled):hover, .stm-cart-button .count, body.health_coach footer .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]:not(:hover), .woocommerce .woocommerce-pagination ul.page-numbers li .page-numbers.current, .woocommerce .woocommerce-pagination ul.page-numbers li .page-numbers:hover, .woocommerce .woocommerce-pagination ul.page-numbers li .page-numbers.next:hover, .training-block-slider-style_2 .training-slider .owl-nav button, body.single-product .content-area .woocommerce-product-gallery::before, body.business .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]:not(:hover), body.therapist .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]:not(:hover), .woocommerce-grouped-product-list-item__price .amount, .btn.dark:hover, body.yoga .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]:hover, body.single-product .content-area .woocommerce-product-gallery .flex-control-nav li img.flex-active + span, body.single-product .content-area .woocommerce-product-gallery .flex-control-nav li:hover span, body.swimming .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]:hover, body.demo-imported .woocommerce-grouped-product-list-item__price .amount, .woocommerce #review_form #respond .form-submit input[type=submit]',
			),
		),
		array(
			'id' => 'secondary_color',
			'type' => 'color',
			'title' => esc_html__( 'Secondary color', 'betop' ),
			'transparent' => false,
			'output' => array(
				'color' => 'body div .sc, body div .sc-hv:hover, body.business .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]',
				'border-color' => '.sbrc, .sbrc-hv:hover',
				'background-color' => '.sbcc, .sbcc-hv:hover, .training-block-slider-wrap .training-block-slider .owl-dots button',
			),
		),
		array(
			'id' => 'third_color',
			'type' => 'color',
			'title' => esc_html__( 'Third color', 'betop' ),
			'transparent' => false,
			'output' => array(
				'color' => 'body div div .tc, body div .tc-hv:hover, .tc-before:before, .tc-before:after, body.fitness .stm-cart-button .btn-sm i.fa-angle-down, .portfolio-carousel .post_type_carousel.owl-carousel .owl-nav button:hover, body.motivation .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]:hover',
				'border-color' => '.tbrc, .tbrc-hv:hover, .tbrc-before:before, .tbrc-before:after, body.health_coach .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"], body.fitness .stm-cart-button a.btn-sm, body.motivation .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"], body.swimming .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]',
				'background-color' => '.tbcc, .tbcc-hv:hover, .tbcc-before:before, .tbcc-before:after, body.health_coach .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]:not(:hover), body.fitness .stm-cart-button > a, body.fitness .stm-cart-button .btn-sm .count, body.motivation .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]:not(:hover), body.swimming .elementor-widget-wp-widget-mc4wp_form_widget .mc4wp-form .mc4wp-form-fields input[type="submit"]',
			),
		),

	)
) );

Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Typography', 'betop' ),
	'id' => 'typosettings',
	'customizer_width' => '400px',
	'icon' => 'el el-font',
	'fields' => array(
		array(
			'id' => 'typosettings-h-typo',
			'type' => 'typography',
			'title' => esc_html__( 'Titles', 'betop' ),
			'google' => true,
			'output' => array( 'h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6, .head-font, nav a, .comment-author b.fn, time, body .date, .reply a, .nav-links .prev, .nav-links .next, table thead th, footer nav a, .select2, .next.page-numbers, .prev.page-numbers, .quantity label, .woocommerce div.product .woocommerce-product-rating .woocommerce-review-link, .tabs li a, .contact-data, .ui-datepicker-header .ui-datepicker-title, .ui-datepicker table td, .ui-datepicker table th, .select2-results__option, .login-btn-wrap .dropdown-account li a, blockquote, .woocommerce ul.products li.product .archive-product-meta .price, .shop_table' ),
			'text-align' => false,
			'letter-spacing' => false,
			'font-size' => false,
			'font-style' => false,
			'line-height' => false,
			'color' => false,
			'font-weight' => false,
			'default' => array(
				'font-family' => 'Poppins',
			),
		),
		array(
			'id' => 'typosettings-h1-typo',
			'type' => 'typography',
			'title' => esc_html__( 'H1 Font', 'betop' ),
			'google' => true,
			'output' => array( 'h1, .h1' ),
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-family' => 'Poppins',
				'font-weight' => '700',
				'font-size' => '54px',
				'line-height' => '60px',
			),
		),
		array(
			'id' => 'typosettings-h1-typo_tablet',
			'type' => 'typography',
			'title' => esc_html__( 'H1 Font tablet', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '48px',
				'line-height' => '54px',
			),
		),
		array(
			'id' => 'typosettings-h1-typo_mobile',
			'type' => 'typography',
			'title' => esc_html__( 'H1 Font mobile', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '42px',
				'line-height' => '48px',
			),
		),
		array(
			'id' => 'typosettings-h2-typo',
			'type' => 'typography',
			'title' => esc_html__( 'H2 Font', 'betop' ),
			'output' => array( 'h2, .h2' ),
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'google' => true,
			'default' => array(
				'font-family' => 'Poppins',
				'font-weight' => '600',
				'font-size' => '42px',
				'line-height' => '48px',
			),
		),
		array(
			'id' => 'typosettings-h2-typo_tablet',
			'type' => 'typography',
			'title' => esc_html__( 'H2 Font tablet', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '36px',
				'line-height' => '42px',
			),
		),
		array(
			'id' => 'typosettings-h2-typo_mobile',
			'type' => 'typography',
			'title' => esc_html__( 'H2 Font mobile', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '30px',
				'line-height' => '36px',
			),
		),
		array(
			'id' => 'typosettings-h3-typo',
			'type' => 'typography',
			'title' => esc_html__( 'H3 Font', 'betop' ),
			'output' => array( 'h3, .h3' ),
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'google' => true,
			'default' => array(
				'font-family' => 'Poppins',
				'font-weight' => '600',
				'font-size' => '36px',
				'line-height' => '42px',
			),
		),
		array(
			'id' => 'typosettings-h3-typo_tablet',
			'type' => 'typography',
			'title' => esc_html__( 'H3 Font tablet', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '30px',
				'line-height' => '36px',
			),
		),
		array(
			'id' => 'typosettings-h3-typo_mobile',
			'type' => 'typography',
			'title' => esc_html__( 'H3 Font mobile', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '24px',
				'line-height' => '30px',
			),
		),
		array(
			'id' => 'typosettings-h4-typo',
			'type' => 'typography',
			'title' => esc_html__( 'H4 Font', 'betop' ),
			'output' => array( 'h4, .h4' ),
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'google' => true,
			'default' => array(
				'font-family' => 'Poppins',
				'font-weight' => '500',
				'font-size' => '30px',
				'line-height' => '36px',
			),
		),
		array(
			'id' => 'typosettings-h4-typo_tablet',
			'type' => 'typography',
			'title' => esc_html__( 'H4 Font tablet', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '24px',
				'line-height' => '30px',
			),
		),
		array(
			'id' => 'typosettings-h4-typo_mobile',
			'type' => 'typography',
			'title' => esc_html__( 'H4 Font mobile', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '18px',
				'line-height' => '24px',
			),
		),
		array(
			'id' => 'typosettings-h5-typo',
			'type' => 'typography',
			'title' => esc_html__( 'H5 Font', 'betop' ),
			'output' => array( 'h5, .h5' ),
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'google' => true,
			'default' => array(
				'font-family' => 'Poppins',
				'font-weight' => '500',
				'font-size' => '24px',
				'line-height' => '30px',
			),
		),
		array(
			'id' => 'typosettings-h5-typo_tablet',
			'type' => 'typography',
			'title' => esc_html__( 'H5 Font tablet', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '18px',
				'line-height' => '24px',
			),
		),
		array(
			'id' => 'typosettings-h5-typo_mobile',
			'type' => 'typography',
			'title' => esc_html__( 'H5 Font mobile', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '16px',
				'line-height' => '22px',
			),
		),
		array(
			'id' => 'typosettings-h6-typo',
			'type' => 'typography',
			'title' => esc_html__( 'H6 Font', 'betop' ),
			'output' => array( 'h6, .h6' ),
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'google' => true,
			'default' => array(
				'font-family' => 'Poppins',
				'font-weight' => '500',
				'font-size' => '18px',
				'line-height' => '24px',
			),
		),
		array(
			'id' => 'typosettings-h6-typo_tablet',
			'type' => 'typography',
			'title' => esc_html__( 'H6 Font tablet', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '16px',
				'line-height' => '22px',
			),
		),
		array(
			'id' => 'typosettings-h6-typo_mobile',
			'type' => 'typography',
			'title' => esc_html__( 'H6 Font mobile', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '14px',
				'line-height' => '20px',
			),
		),
		array(
			'id' => 'typosettings-body-typo',
			'type' => 'typography',
			'title' => esc_html__( 'Body Font', 'betop' ),
			'subtitle' => esc_html__( 'Specify the body font properties.', 'betop' ),
			'google' => true,
			'output' => array( 'body, .elementor-widget-text-editor, input, textarea, .woocommerce-ordering select, .man_cart_block a, .normal-font' ),
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'color' => '#000',
				'font-size' => '18px',
				'font-family' => 'Open Sans',
				'line-height' => '30px'
			),
		),
		array(
			'id' => 'typosettings-body-typo_tablet',
			'type' => 'typography',
			'title' => esc_html__( 'Body font tablet', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '18px',
				'line-height' => '30px'
			),
		),
		array(
			'id' => 'typosettings-body-typo_mobile',
			'type' => 'typography',
			'title' => esc_html__( 'Body font mobile', 'betop' ),
			'google' => true,
			'text-align' => false,
			'color' => false,
			'font-style' => false,
			'default' => array(
				'font-size' => '18px',
				'line-height' => '30px'
			),
		),

	)
) );

Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Buttons', 'betop' ),
	'id' => 'typosettings-button',
	'icon' => 'el el-website',
	'customizer_width' => '450px',
	'fields' => array(
		array(
			'id' => 'button-typo',
			'type' => 'typography',
			'title' => esc_html__( 'Button Font', 'betop' ),
			'google' => false,
			'default' => array(
				'font-size' => '17px',
			),
			'font-family' => false,
			'output' => array( '.btn, input[type=submit], button, .button, .elementor-size-md' ),
			'text-align' => false,
		),
		array(
			'id' => 'button_font_family',
			'type' => 'typography',
			'title' => esc_html__( 'Button Font Family', 'betop' ),
			'google' => true,
			'font-size' => false,
			'font-weight' => false,
			'default' => array(
				'font-family' => 'Poppins',
			),
			'output' => array( '.btn, input[type=submit], button, .button, .elementor-size-md, .elementor-widget-button a.elementor-button, .elementor-widget-button .elementor-button' ),
			'text-align' => false,
		),
	)
) );

Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Blog', 'betop' ),
	'id' => 'blogsettings',
	'customizer_width' => '400px',
	'icon' => 'el el-folder-open',
	'fields' => array(
		array(
			'id' => 'blogsettings-fullwidth',
			'type' => 'switch',
			'title' => esc_html__( 'Sidebar', 'betop' ),
			'default' => true,
		),
		array(
			'id' => 'blog-sidebar-position',
			'type' => 'select',
			'title' => esc_html__( 'Blog sidebar position', 'betop' ),
			'options' => array(
				'left' => esc_html__( 'Left', 'betop' ),
				'right' => esc_html__( 'Right', 'betop' )
			),
			'default' => esc_html__( 'right', 'betop' ),
		),
	)
) );

Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Shop', 'betop' ),
	'id' => 'shopsettings',
	'customizer_width' => '400px',
	'icon' => 'el el-shopping-cart',
	'fields' => array(
		array(
			'id' => 'shopsettings-fullwidth',
			'type' => 'switch',
			'title' => esc_html__( 'Sidebar', 'betop' ),
			'default' => true,
		),
		array(
			'id' => 'shop-sidebar-position',
			'type' => 'select',
			'title' => esc_html__( 'Shop sidebar position', 'betop' ),
			'options' => array(
				'left' => esc_html__( 'Left', 'betop' ),
				'right' => esc_html__( 'Right', 'betop' )
			),
			'default' => esc_html__( 'left', 'betop' ),
		),
	)
) );


/*
 * Post type settings
 * */
$post_types = array(
	'Portfolio' => 'portfolio',
	'Events' => 'events',
	'Services' => 'services'
);

Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Post types', 'betop' ),
	'id' => 'post_types',
	'customizer_width' => '400px',
	'icon' => 'el el-folder-open'
) );
foreach ( $post_types as $post_type => $key ) {
	Redux::setSection( $opt_name, array(
		'title' => $post_type . esc_html__( ' Post type settings', 'betop' ),
		'id' => $key . '_post_type_settings',
		'subsection' => true,
		'icon' => 'el el-file',
		'customizer_width' => '450px',
		'fields' => array(
			array(
				'id' => $key . '_name',
				'type' => 'text',
				'title' => esc_html__( 'Name', 'betop' ),
				'default' => $post_type,
			),
			array(
				'id' => $key . '_plural_name',
				'type' => 'text',
				'title' => esc_html__( 'Plural Name', 'betop' ),
				'default' => $post_type,
			),
			array(
				'id' => $key . '_enable_archive',
				'type' => 'switch',
				'title' => esc_html__( 'Enable Archive Page', 'betop' ),
				'default' => false,
			),
		)
	) );
}

Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Google', 'betop' ),
	'id' => 'google',
	'icon' => 'el el-map-marker',
	'customizer_width' => '450px',
	'fields' => array(
		array(
			'id' => 'gmap-api',
			'type' => 'text',
			'title' => esc_html__( 'Google Maps API key', 'betop' ),
		),

	),
) );


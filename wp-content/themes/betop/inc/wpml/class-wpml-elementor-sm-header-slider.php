<?php

/**
 * Class WPML_Elementor_SM_Header_Slider
 */
class WPML_Elementor_SM_Header_Slider extends WPML_Elementor_Module_With_Items {

	/**
	 * @return string
	 */
	public function get_items_field() {
		return 'slides';
	}

	/**
	 * @return array
	 */
	public function get_fields() {
		return array( 'slide_name', 'button_text');
	}

	/**
	 * @param string $field
	 *
	 * @return string
	 */
	protected function get_title( $field ) {
		switch( $field ) {
			case 'slide_name':
				return esc_html__( 'Header slide name', 'betop' );

			case 'button_text':
				return esc_html__( 'Header slide button text', 'betop' );

			default:
				return '';
		}
	}

	/**
	 * @param string $field
	 *
	 * @return string
	 */
	protected function get_editor_type( $field ) {
		switch( $field ) {
			case 'slide_name':
				return 'LINE';
			case 'button_text':
				return 'LINE';
			default:
				return '';
		}
	}

}

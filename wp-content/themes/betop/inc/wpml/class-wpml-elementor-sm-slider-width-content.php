<?php

/**
 * Class WPML_Elementor_SM_Slider_Width_Content
 */
class WPML_Elementor_SM_Slider_Width_Content extends WPML_Elementor_Module_With_Items
{

	/**
	 * @return string
	 */
	public function get_items_field()
	{
		return 'slides';
	}

	/**
	 * @return array
	 */
	public function get_fields()
	{
		return array( 'slide_name', 'slide_title', 'slide_content' );
	}

	/**
	 * @param string $field
	 *
	 * @return string
	 */
	protected function get_title( $field )
	{
		switch ( $field ) {
			case 'slide_name':
				return esc_html__( 'Slide name', 'betop' );
			case 'slide_title':
				return esc_html__( 'Slide title', 'betop' );
			case 'slide_content':
				return esc_html__( 'Slide content', 'betop' );
			default:
				return '';
		}
	}

	/**
	 * @param string $field
	 *
	 * @return string
	 */
	protected function get_editor_type( $field )
	{
		switch ( $field ) {
			case 'slide_name':
				return 'LINE';
			case 'slide_title':
				return 'VISUAL';
			case 'slide_content':
				return 'VISUAL';
			default:
				return '';
		}
	}

}

<?php

/**
 * Class WPML_Elementor_SM_Testimonials_Carousel
 */
class WPML_Elementor_SM_Testimonials_Carousel extends WPML_Elementor_Module_With_Items
{

	/**
	 * @return string
	 */
	public function get_items_field()
	{
		return 'testimonials';
	}

	/**
	 * @return array
	 */
	public function get_fields()
	{
		return array( 'testimonials_title', 'testimonials_sub_title', 'testimonials_content' );
	}

	/**
	 * @param string $field
	 *
	 * @return string
	 */
	protected function get_title( $field )
	{
		switch ( $field ) {
			case 'testimonials_title':
				return esc_html__( 'Testimonial title', 'betop' );
			case 'testimonials_sub_title':
				return esc_html__( 'Testimonial sub title', 'betop' );
			case 'testimonials_content':
				return esc_html__( 'Testimonial content', 'betop' );
			default:
				return '';
		}
	}

	/**
	 * @param string $field
	 *
	 * @return string
	 */
	protected function get_editor_type( $field )
	{
		switch ( $field ) {
			case 'testimonials_title':
				return 'LINE';
			case 'testimonials_sub_title':
				return 'LINE';
			case 'testimonials_content':
				return 'VISUAL';
			default:
				return '';
		}
	}

}

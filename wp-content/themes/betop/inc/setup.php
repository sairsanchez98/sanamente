<?php
if ( !function_exists( 'betop_setup' ) ) {

	add_action( 'after_setup_theme', 'betop_setup' );

	function betop_setup()
	{
		load_theme_textdomain( 'betop', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		if ( function_exists( 'add_image_size' ) ) {
			add_theme_support( 'post-thumbnails' );
			add_image_size( 'betop_post-thumbnail', 841, 476, true );
			add_image_size( 'betop_post-full-container', 1170, 478, true );
		}

		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'betop' ),
			'secondary' => esc_html__( 'Secondary', 'betop' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
			'gallery',
			'audio',
		) );

		if ( !isset( $content_width ) ) $content_width = 1200;


	}
}

add_action( 'after_switch_theme', function () {
	if ( empty( get_option( 'theme_options', '' ) ) ) {
		$themeoptions_json = '{"last_tab":"","body-background":{"background-color":"#ffffff","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"hide_title_box":"1","title-box-background":{"background-color":"","background-repeat":"","background-size":"","background-attachment":"","background-position":"","background-image":"","media":{"id":"","height":"","width":"","thumbnail":""}},"title-box-color":"","preloader-sw":"","preloader-background":{"color":"","alpha":"1","rgba":""},"preloader-color":{"color":"","alpha":"1","rgba":""},"body-404-background":{"url":"","id":"","height":"","width":"","thumbnail":"","title":"","caption":"","alt":"","description":""},"body_text_color":"#000000","main_color":"#ecb537","secondary_color":"#111829","third_color":"#1289e4","typosettings-h-typo":{"font-family":"Poppins","font-options":"","google":"1","subsets":"latin-ext"},"typosettings-h1-typo":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"700","font-style":"","subsets":"","font-size":"54px","line-height":"60px"},"typosettings-h1-typo_tablet":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"48px","line-height":"54px"},"typosettings-h1-typo_mobile":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"42px","line-height":"48px"},"typosettings-h2-typo":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"600","font-style":"","subsets":"","font-size":"42px","line-height":"48px"},"typosettings-h2-typo_tablet":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"36px","line-height":"42px"},"typosettings-h2-typo_mobile":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"30px","line-height":"36px"},"typosettings-h3-typo":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"600","font-style":"","subsets":"","font-size":"36px","line-height":"42px"},"typosettings-h3-typo_tablet":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"30px","line-height":"36px"},"typosettings-h3-typo_mobile":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"24px","line-height":"30px"},"typosettings-h4-typo":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"500","font-style":"","subsets":"","font-size":"30px","line-height":"36px"},"typosettings-h4-typo_tablet":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"24px","line-height":"30px"},"typosettings-h4-typo_mobile":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"18px","line-height":"24px"},"typosettings-h5-typo":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"24px","line-height":"30px"},"typosettings-h5-typo_tablet":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"18px","line-height":"24px"},"typosettings-h5-typo_mobile":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"16px","line-height":"22px"},"typosettings-h6-typo":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"500","font-style":"","subsets":"","font-size":"18px","line-height":"24px"},"typosettings-h6-typo_tablet":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"16px","line-height":"22px"},"typosettings-h6-typo_mobile":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"14px","line-height":"20px"},"typosettings-body-typo":{"font-family":"Open Sans","font-options":"","google":"1","font-weight":"400","font-style":"","subsets":"","font-size":"18px","line-height":"30px"},"typosettings-body-typo_tablet":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"17px","line-height":"27px"},"typosettings-body-typo_mobile":{"font-family":"","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","font-size":"16px","line-height":"24px"},"button-typo":{"font-weight":"","font-style":"","font-size":"17px","line-height":"17px","color":""},"button_font_family":{"font-family":"Poppins","font-options":"","google":"1","font-weight":"","font-style":"","subsets":"","line-height":"","color":""},"blogsettings-fullwidth":"1","blog-sidebar-position":"right","shopsettings-fullwidth":"1","shop-sidebar-position":"left","portfolio_name":"Portfolio","portfolio_plural_name":"Portfolio","portfolio_enable_archive":"","events_name":"Events","events_plural_name":"Events","events_enable_archive":"","services_name":"Services","services_plural_name":"Services","services_enable_archive":"","gmap-api":"AIzaSyC0UrOyCHoCuyal4rjL7qSlrF_cI0Xfz7M","redux_import_export":"","redux-backup":1}';
		$options = json_decode( $themeoptions_json, true );
		update_option( 'theme_options', $options );
	}
} );


add_action( 'widgets_init', 'betop_widgets_init' );
function betop_widgets_init()
{
	register_sidebar( array(
		'name' => esc_html__( 'Sidebar', 'betop' ),
		'id' => 'sidebar-main',
		'description' => esc_html__( 'Add widgets here.', 'betop' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
		'class' => 'man_sidebar',
	) );
	register_sidebar( array(
		'name' => esc_html__( 'Woo Sidebar', 'betop' ),
		'id' => 'sidebar-woo',
		'description' => esc_html__( 'Add widgets here.', 'betop' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
		'class' => 'woo-sidebar',
	) );
}

add_action( 'wp_head', 'betop_ajaxurl' );

function betop_ajaxurl()
{
    $variables = array(
        'betop_add_new_member' => wp_create_nonce('betop_add_new_member'),
        'betop_is_available' => wp_create_nonce('betop_is_available'),
        'betop_ajax_add_review' => wp_create_nonce('betop_ajax_add_review'),
        'betop_load_more_portfolio' => wp_create_nonce('betop_load_more_portfolio'),
    );
    ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>';
		<?php echo 'window.wp_data = ' . json_encode( $variables ) . ';' ?>
	</script>
<?php }

add_filter( 'the_posts', 'betop_show_future_posts' );

function betop_show_future_posts( $posts )
{
	global $wp_query, $wpdb;

	if ( is_single() && empty( $posts ) ) {
		$posts = $wpdb->get_results( $wp_query->request );

		if ( isset( $posts[ 0 ]->post_status ) && $posts[ 0 ]->post_status != 'future' ) {
			$posts = array();
		}
	}

	return $posts;
}

add_filter( 'body_class', function ( $classes ) {
	$layout_class = betop_get_layout();
	$has_imported = '';
	if ( get_option( 'betop_content_imported' ) && get_option( 'betop_content_imported' ) == '1' ) {
		$has_imported = 'demo-imported';
	} else {
		$has_imported = 'demo-not-imported';
	}
	$classes[] = $has_imported;
	$classes[] = $layout_class;
	return $classes;
} );
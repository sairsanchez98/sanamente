<?php
function betop_getImageSizeData()
{

	$sizes = array(
		'stm-single-portfolio' => array(
			'width' => '1170',
			'height' => '630',
			'crop' => true
		),
		'stm-portfolio-grid' => array(
			'width' => '570',
			'height' => '408',
			'crop' => true
		),
		'stm-portfolio-masonry-big' => array(
			'width' => '600',
			'height' => '900',
			'crop' => false
		),
		'stm-portfolio-masonry-grid-1' => array(
			'width' => '770',
			'height' => '558',
			'crop' => true
		),
		'stm-portfolio-masonry-grid-2' => array(
			'width' => '370',
			'height' => '558',
			'crop' => true
		),
		'stm-portfolio-masonry-grid-3' => array(
			'width' => '370',
			'height' => '293',
			'crop' => true
		),
		'stm-portfolio-masonry-grid-6' => array(
			'width' => '370',
			'height' => '408',
			'crop' => true
		),

		'stm-portfolio-masonry-grid-7' => array(
			'width' => '770',
			'height' => '631',
			'crop' => true
		),
		'stm-portfolio-masonry-grid-8' => array(
			'width' => '370',
			'height' => '193',
			'crop' => true
		),
		'training-grid-img' => array(
			'width' => '740',
			'height' => '510',
			'crop' => true
		),
		'upcoming-event-list' => array(
			'width' => '359',
			'height' => '507',
			'crop' => true
		),
		'training-tab-img' => array(
			'width' => '744',
			'height' => '688',
			'crop' => true
		),
		'coach-full-slide' => array(
			'width' => '1680',
			'height' => '775',
			'crop' => true
		),
		'training-slide' => array(
			'width' => '370',
			'height' => '190',
			'crop' => true
		),
		'training-style-2' => array(
			'width' => '555',
			'height' => '270',
			'crop' => true
		),
		'product-list-img' => array(
			'width' => '405',
			'height' => '600',
			'crop' => true
		),
		'post-grid-3' => array(
			'width' => '220',
			'height' => '220',
			'crop' => true
		),
		'post-type-carousel' => array(
			'width' => '540',
			'height' => '670',
			'crop' => true
		),
		'single-post-image' => array(
			'width' => '570',
			'height' => '307',
			'crop' => true
		),
		'single-training-slide' => array(
			'width' => '540',
			'height' => '600',
			'crop' => true
		),
	);

	return $sizes;
}

function betop_get_thumbnail( $attachment_id, $size = 'thumbnail', $icon = false )
{

	$intermediate = image_get_intermediate_size( $attachment_id, $size );
	$upload_dir = wp_upload_dir();

	if ( !$intermediate OR !file_exists( $upload_dir[ 'basedir' ] . '/' . $intermediate[ 'path' ] ) ) {

		if ( !( $file = get_attached_file( $attachment_id ) ) )
			return false;

		$imagesize = getimagesize( $file );

		$_wp_additional_image_sizes = wp_get_additional_image_sizes();

		foreach ( get_intermediate_image_sizes() as $s ) {
			$sizes[ $s ] = array( 'width' => '', 'height' => '', 'crop' => false );
			if ( isset( $_wp_additional_image_sizes[ $s ][ 'width' ] ) ) {
				// For theme-added sizes
				$sizes[ $s ][ 'width' ] = intval( $_wp_additional_image_sizes[ $s ][ 'width' ] );
			} else {
				// For default sizes set in options
				$sizes[ $s ][ 'width' ] = get_option( "{$s}_size_w" );
			}

			if ( isset( $_wp_additional_image_sizes[ $s ][ 'height' ] ) ) {
				// For theme-added sizes
				$sizes[ $s ][ 'height' ] = intval( $_wp_additional_image_sizes[ $s ][ 'height' ] );
			} else {
				// For default sizes set in options
				$sizes[ $s ][ 'height' ] = get_option( "{$s}_size_h" );
			}

			if ( isset( $_wp_additional_image_sizes[ $s ][ 'crop' ] ) ) {
				// For theme-added sizes
				$sizes[ $s ][ 'crop' ] = $_wp_additional_image_sizes[ $s ][ 'crop' ];
			} else {
				// For default sizes set in options
				$sizes[ $s ][ 'crop' ] = get_option( "{$s}_crop" );
			}
		}

		$sizes = array_merge( $sizes, betop_getImageSizeData() );

		if ( !isset( $sizes[ $size ] ) ) {
			$sizes[ 'width' ] = $imagesize[ 0 ] - 1;
			$sizes[ 'height' ] = $imagesize[ 1 ] - 1;
		} else {

			$sizes = $sizes[ $size ];
			if ( $sizes[ 'width' ] >= $imagesize[ 0 ] )
				$sizes[ 'width' ] = $imagesize[ 0 ] - 1;

			if ( $sizes[ 'height' ] >= $imagesize[ 1 ] )
				$sizes[ 'height' ] = $imagesize[ 1 ] - 1;
		}

		$editor = wp_get_image_editor( $file );
		if ( !is_wp_error( $editor ) ) {

			$resize = $editor->multi_resize( [ $sizes ] );
			$wp_get_attachment_metadata = wp_get_attachment_metadata( $attachment_id );

			if ( !$wp_get_attachment_metadata ) {
				$wp_get_attachment_metadata = [];
				$wp_get_attachment_metadata[ 'width' ] = $imagesize[ 0 ];
				$wp_get_attachment_metadata[ 'height' ] = $imagesize[ 1 ];
				$wp_get_attachment_metadata[ 'file' ] = _wp_relative_upload_path( $file );
				$wp_get_attachment_metadata[ 'sizes' ][ $size ] = $resize[ 0 ];
			} else {
				if ( isset( $resize[ 0 ] ) ) {
					$wp_get_attachment_metadata[ 'sizes' ][ $size ] = $resize[ 0 ];
				}
			}
			wp_update_attachment_metadata( $attachment_id, $wp_get_attachment_metadata );
		}
	}

	$image = image_downsize( $attachment_id, $size );
	return apply_filters( 'get_thumbnail', $image, $attachment_id, $size, $icon );
}

function betop_get_the_post_thumbnail( $postId, $size )
{
	$imgSrc = betop_get_thumbnail( $postId, $size );
	if ( !empty( $imgSrc ) ) {
		echo '<img src="' . esc_url( $imgSrc[ 0 ] ) . '" alt="' . esc_attr( get_the_title( get_post_thumbnail_id( $postId ) ) ) . '" />';
	}
}
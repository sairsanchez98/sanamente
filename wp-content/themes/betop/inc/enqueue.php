<?php

define( 'BETOP_THEME_VERSION', ( WP_DEBUG ) ? time() : wp_get_theme()->get( 'Version' ) );

function betop_fonts_url()
{
	$font_url = '';
	if ( 'off' !== _x( 'on', 'Google font: on or off', 'betop' ) ) {
		$font_url = add_query_arg( 'family', 'Open+Sans:300,400,400i,600,700|Poppins:400,500,600,700', "//fonts.googleapis.com/css" );
	}
	return $font_url;
}

if ( !class_exists( 'ReduxFramework' ) ) {

	function betop_class_names( $classes )
	{
		$classes[] = 'no-options';
		return $classes;
	}

	add_filter( 'body_class', 'betop_class_names' );

	function betop_styles()
	{
		wp_enqueue_style( 'betop-fonts', betop_fonts_url(), array(), BETOP_THEME_VERSION );
	}

	add_action( 'wp_enqueue_scripts', 'betop_styles' );
}


/*
 * Editor Style
 */
add_editor_style( array( 'css/editor-style.css' ) );

/**
 * Load CSS
 */
function betop_theme_styles()
{

	if ( class_exists( 'WooCommerce' ) ) {
		wp_enqueue_style( 'betop-woocommerce', get_template_directory_uri() . '/assets/css/woocommerce.css', array() );
		wp_enqueue_script( 'woocommerce-js', get_template_directory_uri() . '/assets/js/woocommerce.js', array( 'jquery' ), BETOP_THEME_VERSION, true );
		wp_enqueue_script( 'jquery-ui-datepicker', array( 'jquery' ) );
	}
	wp_enqueue_style( 'select2', get_template_directory_uri() . '/assets/css/select2.min.css', null, BETOP_THEME_VERSION, 'all' );
	wp_enqueue_style( 'betop-main-styles', get_template_directory_uri() . '/assets/css/main.css', array( 'select2' ), BETOP_THEME_VERSION );
	wp_enqueue_style( 'betop-responsive-styles', get_template_directory_uri() . '/assets/css/responsive.css', array( 'betop-main-styles' ), BETOP_THEME_VERSION );
	wp_enqueue_style( 'betop-icons', get_template_directory_uri() . '/assets/fonts/fonts.css', array() );
	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/css/fontawesome.css', array() );
	wp_enqueue_style( 'fontawesome-free', get_template_directory_uri() . '/assets/fonts/fontawesome/css/all.min.css', array() );
	wp_enqueue_style( 'owl-css', get_template_directory_uri() . '/assets/css/owl.carousel.css', array(), '1.8.1' );
	wp_enqueue_style( 'owl-theme-css', get_template_directory_uri() . '/assets/css/owl.theme.default.css', array(), '1.8.1' );

	$layout = betop_get_layout();
	if ( file_exists( get_theme_file_path( '/assets/css/layouts/' . $layout . '.css' ) ) ) {
		wp_enqueue_style( 'betop-layout-styles', get_theme_file_uri( '/assets/css/layouts/' . $layout . '.css' ), array( 'betop-main-styles' ), BETOP_THEME_VERSION );
	}

	wp_enqueue_style( 'betop-style', get_stylesheet_uri() );
	wp_enqueue_script( 'imagesloaded' );
	$google_api_key = betop_get_option( 'gmap-api', '' );
	if ( $google_api_key && !empty( $google_api_key ) ) {
		$google_api_map = '//maps.googleapis.com/maps/api/js?key=' . $google_api_key . '&callback=stm_gmap_loaded';
	} else {
		$google_api_map = '//maps.googleapis.com/maps/api/js?';
	}
	wp_register_script( 'betop-stm-gmap', $google_api_map, '', BETOP_THEME_VERSION, true );
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '1.8.1', true );
	wp_enqueue_script( 'isotope', get_template_directory_uri() . '/assets/js/isotope.pkgd.min.js', array( 'jquery' ), '3.0.6', true );
	wp_enqueue_script( 'packery-mode', get_template_directory_uri() . '/assets/js/packery-mode.pkgd.min.js', array( 'jquery' ), '3.0.6', true );
	wp_enqueue_script( 'sticky-sidebar', get_template_directory_uri() . '/assets/js/sticky-sidebar.min.js', array( 'jquery' ), BETOP_THEME_VERSION, true );
	wp_enqueue_script( 'select2', get_template_directory_uri() . '/assets/js/select2.full.min.js', array( 'jquery' ), BETOP_THEME_VERSION, true );
	wp_enqueue_script( 'betop-main-scripts', get_template_directory_uri() . '/assets/js/script.js', array( 'jquery', 'sticky-sidebar' ), BETOP_THEME_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'betop_theme_styles' );

add_action( 'elementor/frontend/after_register_styles', 'betop_style_scripts_admin' );
add_action( 'elementor/editor/before_enqueue_scripts', 'betop_style_scripts_admin' );


function betop_style_scripts_admin()
{
	wp_enqueue_style( 'betop-admin_styles', get_template_directory_uri() . '/assets/css/admin.css', null, BETOP_THEME_VERSION, 'all' );
	wp_enqueue_style( 'betop-icons', get_template_directory_uri() . '/assets/fonts/fonts.css', array(), BETOP_THEME_VERSION );
	wp_enqueue_style( 'betop-google-fonts', betop_fonts_url(), array(), BETOP_THEME_VERSION );
	wp_enqueue_style( 'fontawesome-free', get_template_directory_uri() . '/assets/fonts/fontawesome/css/all.min.css', array() );
}

add_action( 'admin_enqueue_scripts', 'betop_style_scripts_admin', 11 );


function betop_elementor_editor_styles(){
    $widgets = array(
        'accordion',
        'animate-block',
        'basket',
        'calendar',
        'call_to_action',
        'coach-slider',
        'coach-tabs',
        'event-form',
        'footer-menu',
        'google-maps',
        'header-slider',
        'heading',
        'icon-box',
        'image',
        'image-text',
        'login-button',
        'logo',
        'mailchimp',
        'menu',
        'portfolio',
        'products',
        'single-post',
        'slider-with-content',
        'testimonial',
        'testimonials_carousel',
        'training-block',
        'training-slider',
        'trainings',
        'upcoming-events',
        'video'
    );
    foreach($widgets as $widget){
        wp_enqueue_style( 'betop-' . $widget, get_template_directory_uri() . '/assets/css/widgets/'. $widget . '/' . $widget . '.css', null, BETOP_THEME_VERSION, 'all' );
    }
}
add_action( 'elementor/frontend/after_register_styles', 'betop_elementor_editor_styles' );
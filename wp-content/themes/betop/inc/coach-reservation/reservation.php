<?php
function betop_woocommerce_thankyou( $order_id )
{
	$order = wc_get_order( $order_id );
	$order_info = array();
	$parent_id = '';

	foreach ( $order->get_items() as $item_id => $item_product ) {
		//Get the product ID
		$quantity = $item_product->get_quantity();
		$product_id = $item_product->get_product_id();
		if ( isset( $_COOKIE[ 'reserve_product_' . $product_id ] ) ) {
			if ( empty( $_COOKIE[ 'reserve_product_' . $product_id ] ) ) continue;

			$item_data = $_COOKIE[ 'reserve_product_' . $product_id ];
			$item_data = str_replace( '\\', '', $item_data );
			$item_data = json_decode( $item_data, true );
			$reserve_date = $item_data[ 'date' ];
			$order_info[ $product_id ] = $reserve_date;
			$parent_id = $item_data[ 'parent' ];
			$reserve_info = array(
				$reserve_date => $quantity
			);

			$reserve_name = "reserve_{$product_id}";
			$reserve = array();
			if(!empty(get_post_meta( $parent_id, $reserve_name, true ))){
				$reserve = get_post_meta( $parent_id, $reserve_name, true );
			}


			$duration = get_post_meta( $parent_id, 'stmt_training_duration', true );
			if ( $duration == 'weekly' ) {
				$end = date( 'U', strtotime( "+{$quantity} week", $reserve_date ) );
			} else {
				$end = date( 'U', strtotime( "+{$quantity} month", $reserve_date ) );
			}
			$reserved_days = betop_separate_date( $reserve_date, $end );

			foreach ( $reserved_days as $day ) {
				if ( !empty( $reserve[ $day ] ) ) {
					$reserve[ $day ] += 1;

				} else {
					$reserve[ $day ] = 1;
				}
			}


			if ( !$reserve[ $reserve_date ] ) {
				update_post_meta( $parent_id, $reserve_name, $reserve_info );
			} else {
				if ( !empty( $reserve[ $reserve_date ] ) ) {
					$reserve[ $reserve_date ] = $reserve[ $reserve_date ] + $quantity;
					update_post_meta( $parent_id, $reserve_name, $reserve );
				} else {
					$new_reserved_days = array_unique( $reserve + $reserve_info );
					update_post_meta( $parent_id, $reserve_name, $new_reserved_days );
				}
			}
			setcookie( 'reserve_product_' . $product_id, null, -1, '/' );
		}
	}
}

;
add_action( 'woocommerce_thankyou', 'betop_woocommerce_thankyou', 10, 1 );

function betop_separate_date( $start, $end, $interval = 'P1D' )
{
	$period = new DatePeriod(
		new DateTime( date( 'Y-m-d', $start ) ),
		new DateInterval( $interval ),
		new DateTime( date( 'Y-m-d', $end ) )
	);
	$dates = array();
	foreach ( $period as $key => $value ) {
		$dates[] = $value->format( 'U' );
	}
	return $dates;
}


function betop_unvailable_days( $selected_days, $parent, $child )
{
	$reserved_days = get_post_meta( $parent, "reserve_{$child}", true );
	$limit = get_post_meta( $parent, 'stmt_membership_limit', true );
	$unavailable_days = array();
	foreach ( $selected_days as $day ) {
		if ( !empty( $reserved_days[ $day ] ) && $reserved_days[ $day ] >= $limit ) {
			$unavailable_days[] = $day;
		}
	}
	return $unavailable_days;
}


function betop_is_available()
{
    check_ajax_referer('betop_is_available', 'security');
	$parent_id = sanitize_text_field($_POST[ 'parent_id' ]);
	$child_id = sanitize_text_field($_POST[ 'child_id' ]);
	$date_start = sanitize_text_field($_POST[ 'date_start' ]);
	$quantity = sanitize_text_field($_POST[ 'quantity' ]);
	if(empty($parent_id) || empty($child_id) || empty($date_start)) return;
	$duration = get_post_meta( $parent_id, 'stmt_training_duration', true );
	if ( $duration == 'weekly' ) {
		$end = date( 'U', strtotime( "+{$quantity} week", $date_start ) );
	} else {
		$end = date( 'U', strtotime( "+{$quantity} month", $date_start ) );
	}
	$reserved_days = betop_separate_date( $date_start, $end );
	$unavailable_days = betop_unvailable_days( $reserved_days, $parent_id, $child_id );

	$response = array();
	if ( !empty( $unavailable_days ) ) {
		$ranges = betop_diff( $unavailable_days );
		if ( !empty( $ranges ) ) {
			$print_range = '';
			foreach ( $ranges as $range ) {
				if ( count( $range ) > 1 ) {
					$print_range .= date( 'j, M', $range[ 0 ] ) . ' - ' . date( 'j, M', end( $range ) ) . ', ';
				} else {
					$print_range .= date( 'j, M', $range[ 0 ] ) . ', ';
				}
			}
		}
		$print_range = substr( $print_range, 0, -2 );
		$message = sprintf( esc_html__( 'These dates is not available:', 'betop' ) . ' %s. ' . esc_html__( 'Please choose another date.', 'betop' ), $print_range );
		$response[ 'available' ] = false;
		$response[ 'text' ] = $message;
	} else {
		$response[ 'available' ] = true;
	}
	$response[ 'child' ] = $child_id;
	wp_send_json( $response );
}

add_action( 'wp_ajax_nopriv_betop_is_available', 'betop_is_available' );
add_action( 'wp_ajax_betop_is_available', 'betop_is_available' );

function betop_diff( $numbs, $period = 86400 )
{
	$arr = array();
	$num = 0;
	$ident = $numbs[ 0 ];

	for ( $i = 0; $i < count( $numbs ); $i++ ) {
		if ( $ident == $numbs[ $i ] ) {
			$arr[ $num ][] = $numbs[ $i ];
			$ident = $ident + $period;
		} else {
			$ident = $numbs[ $i ];
			$num += 1;
			$arr[ $num ][] = $numbs[ $i ];
			$ident = $ident + $period;
		}
	}
	return $arr;
}
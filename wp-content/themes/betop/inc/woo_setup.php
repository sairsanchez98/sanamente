<?php

// WooCommerce
function betop_add_woocommerce_support()
{
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

add_action( 'after_setup_theme', 'betop_add_woocommerce_support' );


add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function ( $size ) {
	return array(
		'width' => 570,
		'height' => 600,
		'crop' => true,
	);
} );

add_filter( 'woocommerce_get_image_size_thumbnail', function ( $size ) {
	return array(
		'width' => 350,
		'height' => 350,
		'crop' => true,
	);
} );

add_filter( 'woocommerce_get_image_size_single', function ( $size ) {
	return array(
		'width' => 570,
		'height' => 600,
		'crop' => true,
	);
} );

add_filter( 'woocommerce_pagination_args', function ( $args ) {
	$args[ 'prev_text' ] = esc_html__( 'Prev', 'betop' );
	$args[ 'next_text' ] = esc_html__( 'Next', 'betop' );
	return $args;
}, 100 );

/**
 * Remove the breadcrumbs
 */
add_action( 'init', 'woo_remove_wc_breadcrumbs' );
function woo_remove_wc_breadcrumbs()
{
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}


// Remove Additional Information Tab
add_filter( 'woocommerce_product_tabs', 'betop_remove_product_tabs', 98 );

function betop_remove_product_tabs( $tabs )
{
	return $tabs;
}


// Labels
add_filter( 'woocommerce_product_add_to_cart_text', 'custom_woocommerce_product_add_to_cart_text' );
function custom_woocommerce_product_add_to_cart_text()
{
	global $product;

	$product_type = $product->get_type();
	if ( $product->get_price() !== '' ) {
		switch ( $product_type ) {
			case 'external':
				return esc_html__( 'Buy product', 'betop' );
				break;
			case 'grouped':
				return esc_html__( 'View products', 'betop' );
				break;
			case 'simple':
				return esc_html__( 'Add to cart', 'betop' );
				break;
			case 'variable':
				return esc_html__( 'Select options', 'betop' );
				break;
			default:
				return esc_html__( 'Read more', 'betop' );
		}  // end switch
	} else {
		return esc_html__( 'Read more', 'betop' );
	}
}

add_filter( 'woocommerce_add_to_cart_fragments', 'betop_custom_cart_count', 10, 1 );

function betop_custom_cart_count( $fragments )
{

	$fragments[ 'span.wc-cart-count' ] = '<span class="count wc-cart-count pbcc">' . WC()->cart->get_cart_contents_count() . '</span>';

	return $fragments;

}

<?php
add_action( 'wp_ajax_betop_add_new_member', 'betop_add_new_member' );
add_action( 'wp_ajax_nopriv_betop_add_new_member', 'betop_add_new_member' );
if ( !function_exists( 'betop_add_new_member' ) ) {
	function betop_add_new_member()
	{
	    check_ajax_referer('betop_add_new_member', 'security');
		$event_id = !empty( $_POST[ 'event_id' ] ) ? sanitize_text_field($_POST[ 'event_id' ]) : '';
		$member_name = !empty( $_POST[ 'member_name' ] ) ? sanitize_text_field($_POST[ 'member_name' ]) : '';
		$member_email = !empty( $_POST[ 'member_email' ] ) ? sanitize_text_field($_POST[ 'member_email' ]) : '';
		$member_phone = !empty( $_POST[ 'member_phone' ] ) ? sanitize_text_field($_POST[ 'member_phone' ]) : '';
		$response = array();
		if ( !empty( $event_id ) && !empty( $member_name ) && !empty( $member_email ) && !empty( $member_phone ) ) {
			$post_name = $member_name . '-' . $member_phone . '-' . $event_id;
			$args = array(
				'name' => $post_name,
				'post_type' => 'event_member',
				'post_status' => 'publish',
				'numberposts' => 1
			);
			$event_members = get_posts( $args );
			if ( !$event_members || empty( $event_members ) ) {
				$post_title = $member_name . '-' . get_the_title( $event_id );
				$post_id = wp_insert_post( wp_slash( array(
					'post_status' => 'publish',
					'post_type' => 'event_member',
					'post_title' => wp_strip_all_tags( $post_title ),
					'post_name' => $post_name
				) ) );
				if ( $post_id ) {
					update_post_meta( $post_id, 'stmt_member_name', $member_name );
					update_post_meta( $post_id, 'stmt_member_email', $member_email );
					update_post_meta( $post_id, 'stmt_member_phone', $member_phone );
					update_post_meta( $post_id, 'event_id', $event_id );
					$response[ 'message' ] = esc_html__( 'You have joined the event', 'betop' );
					$response[ 'status' ] = 'success';
				}
			} else {
				$response[ 'message' ] = esc_html__( 'You have already joined the event', 'betop' );
				$response[ 'status' ] = 'warning';
			}


		} else {
			$response[ 'message' ] = esc_html__( 'Please fill all fields', 'betop' );
			$response[ 'status' ] = 'error';
		}
		wp_send_json( $response );
	}
}
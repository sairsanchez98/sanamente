<?php


/*Repeater Widgets Options*/
require_once( get_template_directory() . '/inc/wpml/class-wpml-elementor-sm-header-slider.php' );
require_once( get_template_directory() . '/inc/wpml/class-wpml-elementor-sm-slider-width-content.php' );
require_once( get_template_directory() . '/inc/wpml/class-wpml-elementor-sm-testimonials_carousel.php' );
function betop_add_translate_widget( $widgets )
{
	$widgets[ 'sm-animate-block' ] = [
		'conditions' => [ 'widgetType' => 'sm-animate-block' ],
		'fields' => [
			[
				'field' => 'text',
				'type' => esc_html__( 'Background Text', 'betop' ),
				'editor_type' => 'VISUAL'
			]
		],
	];
	$widgets[ 'sm-portfolio' ] = [
		'conditions' => [ 'widgetType' => 'sm-portfolio' ],
		'fields' => [
			[
				'field' => 'button_text',
				'type' => esc_html__( 'Contact Button Text', 'betop' ),
				'editor_type' => 'LINE'
			]
		],
	];
	$widgets[ 'google_maps' ] = [
		'conditions' => [ 'widgetType' => 'google_maps' ],
		'fields' => [
			[
				'field' => 'info_window',
				'type' => esc_html__( 'Info Window Text', 'betop' ),
				'editor_type' => 'VISUAL'
			]
		],
	];
	$widgets[ 'sm-image-text' ] = [
		'conditions' => [ 'widgetType' => 'sm-image-text' ],
		'fields' => [
			[
				'field' => 'text',
				'type' => esc_html__( 'Image text', 'betop' ),
				'editor_type' => 'VISUAL'
			]
		],
	];
	$widgets[ 'sm-logo' ] = [
		'conditions' => [ 'widgetType' => 'sm-logo' ],
		'fields' => [
			[
				'field' => 'text',
				'type' => esc_html__( 'Logo text', 'betop' ),
				'editor_type' => 'LINE'
			]
		],
	];
	$widgets[ 'sm-training-block' ] = [
		'conditions' => [ 'widgetType' => 'sm-training-block' ],
		'fields' => [
			[
				'field' => 'title',
				'type' => esc_html__( 'Training block title', 'betop' ),
				'editor_type' => 'LINE'
			],
			[
				'field' => 'sub_title',
				'type' => esc_html__( 'Training block sub title', 'betop' ),
				'editor_type' => 'VISUAL'
			]
		],
	];
	$widgets[ 'sm-training-slider' ] = [
		'conditions' => [ 'widgetType' => 'sm-training-slider' ],
		'fields' => [
			[
				'field' => 'block_title',
				'type' => esc_html__( 'Training slider title', 'betop' ),
				'editor_type' => 'LINE'
			],
			[
				'field' => 'block_description',
				'type' => esc_html__( 'Training slider description', 'betop' ),
				'editor_type' => 'VISUAL'
			]
		],
	];
	$widgets[ 'sm-trainings' ] = [
		'conditions' => [ 'widgetType' => 'sm-trainings' ],
		'fields' => [
			[
				'field' => 'page_subtitle',
				'type' => esc_html__( 'Trainings block subtitle', 'betop' ),
				'editor_type' => 'LINE'
			],
			[
				'field' => 'button_text',
				'type' => esc_html__( 'Trainings block button text', 'betop' ),
				'editor_type' => 'LINE'
			]
		],
	];
	$widgets[ 'sm-upcoming-events' ] = [
		'conditions' => [ 'widgetType' => 'sm-upcoming-events' ],
		'fields' => [
			[
				'field' => 'section_title',
				'type' => esc_html__( 'Upcoming events section title', 'betop' ),
				'editor_type' => 'LINE'
			],
			[
				'field' => 'button_text',
				'type' => esc_html__( 'Upcoming events button text', 'betop' ),
				'editor_type' => 'LINE'
			]
		],
	];
	$widgets[ 'sm-icon-title' ] = [
		'conditions' => [ 'widgetType' => 'sm-icon-title' ],
		'fields' => [
			[
				'field' => 'text',
				'type' => esc_html__( 'Call to action text', 'betop' ),
				'editor_type' => 'VISUAL'
			],
			[
				'field' => 'button_text',
				'type' => esc_html__( 'Button text', 'betop' ),
				'editor_type' => 'LINE'
			],
			[
				'field' => 'button_link',
				'type' => esc_html__( 'Button link', 'betop' ),
				'editor_type' => 'LINE'
			],
			[
				'field' => 'contact_text',
				'type' => esc_html__( 'Contact text', 'betop' ),
				'editor_type' => 'LINE'
			]
		],
	];
	$widgets[ 'sm-header-slider' ] = [
		'conditions' => [ 'widgetType' => 'sm-header-slider' ],
		'integration-class' => 'WPML_Elementor_SM_Header_Slider',
	];
	$widgets[ 'sm-slider-width-content' ] = [
		'conditions' => [ 'widgetType' => 'sm-slider-width-content' ],
		'integration-class' => 'WPML_Elementor_SM_Slider_Width_Content',
	];
	$widgets[ 'sm-testimonials_carousel' ] = [
		'conditions' => [ 'widgetType' => 'sm-testimonials_carousel' ],
		'integration-class' => 'WPML_Elementor_SM_Testimonials_Carousel',
	];
	return $widgets;
}

add_filter( 'wpml_elementor_widgets_to_translate', 'betop_add_translate_widget' );

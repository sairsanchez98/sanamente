<?php
function betop_header_footer_elementor_support()
{
	add_theme_support( 'header-footer-elementor' );
}
add_action( 'after_setup_theme', 'betop_header_footer_elementor_support' );

function betop_sub_str( $string, $length, $affix )
{
	if ( strlen( $string ) <= $length ) {
		return $string;
	} else {
		return substr( $string, 0, $length ) . $affix;
	}
}


if ( !function_exists( 'betop_get_option' ) ) {
	function betop_get_option( $option_id, $default )
	{
		if ( class_exists( 'ReduxFramework' ) ) {
			global $theme_options;

			if ( ( isset( $theme_options[ $option_id ] ) && !empty( $theme_options[ $option_id ] ) ) || ( isset( $theme_options[ $option_id ] ) && $theme_options[ $option_id ] === '0' ) ) {
				return $theme_options[ $option_id ];
			} else {
				return $default;
			}
		} else {
			return $default;
		}
	}
}

if ( !function_exists( 'betop_duration_products' ) ) {
	function betop_duration_products( $product_id, $print = false )
	{
		$duration = get_post_meta( $product_id, 'stmt_training_duration', true );
		$period = '';
		if ( $duration == 'weekly' ) {
			$period = 'week';
			if($print) {
			    echo esc_html__('week', 'betop');
            }
		} else {
			$period = 'month';
            if($print) {
                echo esc_html__('month', 'betop');
            }
		}
        if(!$print) {
		    return $period;
        }
	}
}

if ( !function_exists( 'betop_get_layout' ) ) {
	function betop_get_layout()
	{
		return get_option( 'betop_layout', 'therapist' );
	}
}

function betop_theme_layout_plugins($layout = 'yoga', $get_layouts = false) {

	$required = array(
		'elementor',
		'add-to-any',
		'woocommerce',
		'ti-woocommerce-wishlist',
		'stmt-theme-options',
		'elementor-stm-widgets',
		'contact-form-7',
		'mailchimp-for-wp',
		'header-footer-elementor',
	);

	$plugins = array(
		'yoga' => array(

		),
		'health_coach' => array(

		),
		'fitness' => array(

		),
		'business' => array(

		),
		'therapist' => array(

		),
		'motivation' => array(

		),
		'swimming' => array(

		),
		'boxing' => array(

		),
	);

	if ($get_layouts) return $plugins;

	return array_merge($required, $plugins[$layout]);
}

function betop_theme_recommended_plugins() {
	return array(
		'stm-gdpr-compliance'
	);
}
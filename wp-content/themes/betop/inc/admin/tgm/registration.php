<?php
/*Require TGM CLASS*/
require_once $stmt_inc_path . '/admin/tgm/class-tgm-plugin-activation.php';

/*Register plugins to activate*/
add_action('tgmpa_register', 'betop_require_plugins');

function betop_require_plugins($return = false)
{
    $plugins_path = get_template_directory() . '/inc/admin/tgm/plugins';

    $plugins = array(
        'elementor' => array(
            'name' => 'Elementor',
            'slug' => 'elementor',
            'required' => true,
        ),
        'add-to-any' => array(
            'name' => 'AddToAny Share Buttons',
            'slug' => 'add-to-any',
            'required' => true,
        ),
        'woocommerce' => array(
            'name' => 'WooCommerce',
            'slug' => 'woocommerce',
            'required' => true,
        ),
        'ti-woocommerce-wishlist' => array(
            'name' => 'WooCommerce Wishlist Plugin',
            'slug' => 'ti-woocommerce-wishlist',
            'required' => true,
        ),
        'stmt-theme-options' => array(
            'name' => 'STMT Theme Options',
            'slug' => 'stmt-theme-options',
            'source' => get_package('stmt-theme-options', 'zip'),
            'required' => true,
            'version' => '1.0',
            'external_url' => 'https://stylemixthemes.com/'
        ),
        'elementor-stm-widgets' => array(
            'name' => 'STM Elementor Widgets',
            'slug' => 'elementor-stm-widgets',
            'source' => get_package('elementor-stm-widgets', 'zip'),
            'required' => true,
            'version' => '1.0.8',
            'external_url' => 'https://stylemixthemes.com/'
        ),
        'contact-form-7' => array(
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
            'required' => true,
        ),
        'mailchimp-for-wp' => array(
            'name' => 'MailChimp for WordPress',
            'slug' => 'mailchimp-for-wp',
            'required' => true,
        ),
        'header-footer-elementor' => array(
            'name'     => 'Header Footer Elementor',
            'slug'     => 'header-footer-elementor',
            'required' => true
        ),
        'stm-gdpr-compliance' => array(
            'name' => 'GDPR Compliance & Cookie Consent',
            'slug' => 'stm-gdpr-compliance',
            'source' => get_package('stm-gdpr-compliance', 'zip'),
            'required' => false,
            'version' => '1.1',
        ),

    );

    if ($return) {
        return $plugins;
    } else {
        $config = array(
            'id' => 'betop',
            'is_automatic' => false
        );

        $layout_plugins = betop_theme_layout_plugins(betop_get_layout());
        $recommended_plugins = betop_theme_recommended_plugins();
        $layout_plugins = array_merge($layout_plugins, $recommended_plugins);

        $tgm_layout_plugins = array();
        foreach($layout_plugins as $layout_plugin) {
            $tgm_layout_plugins[$layout_plugin] = $plugins[$layout_plugin];
        }

        tgmpa($tgm_layout_plugins, $config);
    }
}
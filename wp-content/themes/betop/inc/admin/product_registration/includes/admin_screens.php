<?php
//Register scripts and styles for admin pages
function betop_startup_styles()
{
    wp_enqueue_style('stm-startup_css', get_template_directory_uri() . '/inc/admin/product_registration/assets/css/style.css', null, 1.6, 'all');
}

add_action('admin_enqueue_scripts', 'betop_startup_styles');

//Other themes
function other_themes()
{
    $api_url = '//stylemixthemes.scdn2.secure.raxcdn.com/api/themes.json';
    $response = wp_remote_get($api_url);

    if (!is_wp_error($response) && $response['response']['code'] === 200) {
        $other_themes = json_decode($response['body']);

        return $other_themes;
    }

    return false;
}

//Register Startup page in admin menu
function betop_register_startup_screen()
{
    $theme = betop_get_theme_info();
    $theme_name = $theme['name'];
    $theme_name_sanitized = 'betop';

    // Work around for theme check.
    $stm_admin_menu_page_creation_method = 'add' . '_menu_page';
    $stm_admin_submenu_page_creation_method = 'add' . '_submenu_page';

    /*Item Registration*/
    $stm_admin_menu_page_creation_method(
        $theme_name,
        esc_html__('beTop', 'betop'),
        'manage_options',
        $theme_name_sanitized,
        'betop_theme_admin_page_functions',
        get_template_directory_uri() . '/inc/admin/product_registration/assets/img/icon.png',
        '2.1111111111'
    );

    /*Demo Import*/
    $stm_admin_submenu_page_creation_method(
        $theme_name_sanitized,
        esc_html__('Demo import', 'betop'),
        esc_html__('Demo import', 'betop'),
        'manage_options',
        $theme_name_sanitized . '-demos',
        'betop_theme_admin_install_demo_page'
    );

    /*System status*/
    $stm_admin_submenu_page_creation_method(
        $theme_name_sanitized,
        esc_html__('System status', 'betop'),
        esc_html__('System status', 'betop'),
        'manage_options',
        $theme_name_sanitized . '-system-status',
        'betop_theme_admin_system_status_page'
    );

    /*Theme options export*/
    if (!empty($_GET['stm_get_to'])) {
        $stm_admin_submenu_page_creation_method(
            $theme_name_sanitized,
            esc_html__('Theme options export', 'betop'),
            esc_html__('Theme options export', 'betop'),
            'manage_options',
            'gigant-theme-options-export',
            'betop_theme_options_export'
        );
    }
}

add_action('admin_menu', 'betop_register_startup_screen', 20);

function betop_startup_templates($path)
{
    $path = 'inc/admin/product_registration/screens/' . $path . '.php';

    $located = locate_template($path);

    if ($located) {
        load_template($located);
    }
}

//Startup screen menu page welcome
function betop_theme_admin_page_functions()
{
    betop_startup_templates('startup');
}

/*Support Screen*/
function betop_theme_admin_support_page()
{
    betop_startup_templates('support');
}

/*Install Plugins*/
function betop_theme_admin_plugins_page()
{
    betop_startup_templates('plugins');
}

/*Install Demo*/
function betop_theme_admin_install_demo_page()
{
    betop_startup_templates('install_demo');
}

/*System status*/
function betop_theme_admin_system_status_page()
{
    betop_startup_templates('system_status');
}

/*Other Themes*/
function betop_theme_admin_other_themes_page()
{
    betop_startup_templates('other_themes');
}

//Admin tabs
function betop_get_admin_tabs($screen = 'welcome')
{
    $theme = betop_get_theme_info();
    $creds = stm_get_creds();
    $theme_name = $theme['name'];
    $theme_name_sanitized = 'stm-admin';
    if (empty($screen)) {
        $screen = $theme_name_sanitized;
    }
    ?>
    <div class="clearfix">
        <div class="stm_theme_info">
            <div class="stm_theme_version"><?php echo substr($theme['v'], 0, 3); ?></div>
        </div>
        <div class="stm-about-text-wrap">
            <h1><?php printf(esc_html__('Welcome to %s', 'betop'), $theme_name); ?></h1>
        </div>
    </div>
    <?php $notice = get_site_transient('stm_auth_notice');
    if (!empty($creds['t']) && !empty($notice)): ?>
        <div class="stm-admin-message"><strong>Theme Registration Error:</strong> <?php echo esc_attr($notice); ?></div>
    <?php endif; ?>
    <h2 class="nav-tab-wrapper">
        <a href="<?php echo esc_url_raw(admin_url('admin.php?page=betop')); ?>"
           class="<?php echo ('welcome' === $screen) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_attr_e('Product Registration', 'betop'); ?></a>

        <a href="<?php echo esc_url_raw(admin_url('admin.php?page=betop-demos')); ?>"
           class="<?php echo ('demos' === $screen) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_attr_e('Install Demos', 'betop'); ?></a>

        <a href="<?php echo esc_url_raw(admin_url('admin.php?page=tgmpa-install-plugins')); ?>"
           class="<?php echo ('plugins' === $screen) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_attr_e('Plugins', 'betop'); ?></a>

        <a href="<?php echo esc_url_raw(admin_url('admin.php?page=betop-system-status')); ?>"
           class="<?php echo ('system-status' === $screen) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_attr_e('System Status', 'betop'); ?></a>

    </h2>
    <?php
}
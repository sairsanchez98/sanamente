<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( '', $product ); ?>>
	<div class="archive-image-wrap">
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
//	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */ ?>
		<a href="<?php the_permalink(); ?>" class="product-img-link">


		<?php
	do_action( 'woocommerce_before_shop_loop_item_title' );

?></a>
		<?php
	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */ ?>
		<div class="hidden-labels">
	<?php
		if( shortcode_exists("ti_wishlists_addtowishlist") ){
			echo do_shortcode("[ti_wishlists_addtowishlist loop=yes]");
		}
		do_action( 'woocommerce_after_shop_loop_item' );


		?>
		<a class="stm_single_product__more stm_lightgallery__selector"
		   title="<?php the_title_attribute(); ?>"
		   href="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>">
			<i class="mc-zoom"></i>
		</a>
		</div>
	</div>

	<div class="archive-product-meta">
		<a href="<?php the_permalink(); ?>">
	<?php
		/**
		 * Hook: woocommerce_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_product_title - 10
		 */
		do_action( 'woocommerce_shop_loop_item_title' ); ?>
		</a>
		<?php
		/**
		 * Hook: woocommerce_after_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_rating - 5
		 * @hooked woocommerce_template_loop_price - 10
		 */
		do_action( 'woocommerce_after_shop_loop_item_title' );
		?>
	</div>
</li>

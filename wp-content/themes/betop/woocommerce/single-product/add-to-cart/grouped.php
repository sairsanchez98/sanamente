<?php
/**
 * Grouped product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/grouped.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.4.0
 */
defined( 'ABSPATH' ) || exit;

global $product, $post;
if(get_option('betop_content_imported') && get_option('betop_content_imported') == '1'){
	$repeatable_product = get_post_meta( get_the_ID(), 'stmt_is_repeatable', true );
	do_action( 'woocommerce_before_add_to_cart_form' );
	$quantity_label = '';
	$duration = get_post_meta( get_the_ID(), 'stmt_training_duration', true );
	if ( $duration == 'weekly' ) {
		$quantity_label = 'Weeks';
	} else {
		$quantity_label = 'Months';
	}
	$product_type = '';
	if ( $repeatable_product && $repeatable_product == 'on' ) {
		$product_type = 'repeatable-product';
	} else {
		$product_type = 'no-repeatable-product';
	}
	?>

	<form class="cart grouped_form <?php echo esc_attr( $product_type ); ?>"
	      action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>"
	      method="post" enctype='multipart/form-data'>
		<table cellspacing="0" class="woocommerce-grouped-product-list group_table">
			<tbody>
			<?php if ( !empty( $grouped_products ) ): ?>
				<tr class="grouped-top-labels">
					<td>
						<span class="pbcc-before head-font"><?php esc_html_e('Class', 'betop'); ?></span>
					</td>
					<td>
						<span class="pbcc-before head-font"><?php esc_html_e('Period', 'betop'); ?></span>
					</td>
					<td>
						<span class="pbcc-before head-font"><?php esc_html_e('Price', 'betop'); ?></span>
					</td>
				</tr>
			<?php endif; ?>
			<?php
			$quantites_required = false;
			$previous_post = $post;
			$grouped_product_columns = apply_filters( 'woocommerce_grouped_product_columns', array(
				'label',
				'quantity',
				'price',
			), $product );

			foreach ( $grouped_products as $grouped_product_child ) {
				$post_object = get_post( $grouped_product_child->get_id() );
				$quantites_required = $quantites_required || ( $grouped_product_child->is_purchasable() && !$grouped_product_child->has_options() );
				$post = $post_object; // WPCS: override ok.
				setup_postdata( $post );
				$child_id = $grouped_product_child->get_id();
				$deadline = get_post_meta( $child_id, 'stmt_deadline', true );

				$deadline = !empty( $deadline ) ? strtotime( $deadline ) : '';
				$is_active_class_product = ( !empty( $deadline ) && $deadline < time() ) ? 'disabled' : '';
				echo '<tr id="product-' . esc_attr( $grouped_product_child->get_id() ) . '" class="woocommerce-grouped-product-list-item ' . esc_attr( implode( ' ', wc_get_product_class( '', $grouped_product_child->get_id() ) ) ) . ' ' . esc_attr( $is_active_class_product ) . ' unavailable ">';
				// Output columns for each product.
				foreach ( $grouped_product_columns as $column_id ) {
					do_action( 'woocommerce_grouped_product_list_before_' . $column_id, $grouped_product_child );
					switch ( $column_id ) {
						case 'quantity':
							ob_start();

							if ( !$grouped_product_child->is_purchasable() || $grouped_product_child->has_options() || !$grouped_product_child->is_in_stock() ) {
								woocommerce_template_loop_add_to_cart();
							} elseif ( $grouped_product_child->is_sold_individually() ) {
								echo '<input type="checkbox" name="' . esc_attr( 'quantity[' . $grouped_product_child->get_id() . ']' ) . '" value="1" class="wc-grouped-product-add-to-cart-checkbox" />';
							} else {
								do_action( 'woocommerce_before_add_to_cart_quantity' );

								woocommerce_quantity_input( array(
									'input_name' => 'quantity[' . $grouped_product_child->get_id() . ']',
									'input_value' => isset( $_POST[ 'quantity' ][ $grouped_product_child->get_id() ] ) ? wc_stock_amount( wc_clean( wp_unslash( $_POST[ 'quantity' ][ $grouped_product_child->get_id() ] ) ) ) : 0, // WPCS: CSRF ok, input var okay, sanitization ok.
									'min_value' => apply_filters( 'woocommerce_quantity_input_min', 0, $grouped_product_child ),
									'max_value' => apply_filters( 'woocommerce_quantity_input_max', $grouped_product_child->get_max_purchase_quantity(), $grouped_product_child ),
									'class' => 'grouped-quantity',
									'grouped_quantity_label' => $quantity_label
								) );

								do_action( 'woocommerce_after_add_to_cart_quantity' );
							} ?>
							<?php if ( $repeatable_product && $repeatable_product == 'on' ): ?>
							<div class="date-reservation">
								<h4 class="normal-font"><?php echo esc_html_e( 'Choose start day', 'betop' ); ?></h4>
								<span><input type="text" class="pbrc" name="reservation_date" readonly="true"
								             autocomplete="off" data-id="<?php echo esc_attr( $product->get_id() ); ?>"/><i
											class="mc-calendar pc"></i></span>
							</div>
							<?php echo '<div class="message-wrap"></div>'; ?>
						<?php endif; ?>
							<?php

							$value = ob_get_clean();
							break;
						case 'label':
							$value = '<label for="product-' . esc_attr( $grouped_product_child->get_id() ) . '" class="head-font">';
							$value .= $grouped_product_child->is_visible() ? '<a href="' . esc_url( apply_filters( 'woocommerce_grouped_product_list_link', $grouped_product_child->get_permalink(), $grouped_product_child->get_id() ) ) . '">' . $grouped_product_child->get_name() . '</a>' : $grouped_product_child->get_name();
							$value .= '</label>';
							break;
						case 'price':
							$value = $grouped_product_child->get_price_html() . wc_get_stock_html( $grouped_product_child );
							break;
						default:
							$value = '';
							break;
					}

					echo '<td class="head-font woocommerce-grouped-product-list-item__' . esc_attr( $column_id ) . '">' . apply_filters( 'woocommerce_grouped_product_list_column_' . $column_id, $value, $grouped_product_child ) . '</td>'; // WPCS: XSS ok.

					do_action( 'woocommerce_grouped_product_list_after_' . $column_id, $grouped_product_child );
				}

				echo '</tr>';
			}
			$post = $previous_post; // WPCS: override ok.
			setup_postdata( $post );
			?>
			</tbody>
		</table>
		<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>"/>

		<?php if ( $quantites_required ) : ?>

			<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

			<button type="submit"
			        class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

			<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

		<?php endif; ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
	<?php if ( $repeatable_product && $repeatable_product == 'on' ): ?>
		<?php ob_start(); ?>
		(function ($) {
		$(document).ready(function () {
		$('.date-reservation input[name=reservation_date]').datepicker({
		dateFormat: "mm/dd/yy",
		minDate: new Date(<?php echo date( 'Y' ); ?>, <?php echo date( 'm' ) - 1; ?>, <?php echo date( 'd' ); ?>),
		onSelect: function (date) {
		let quantity = $(this).closest('.woocommerce-grouped-product-list-item__quantity').find('input.qty').val();
		let parentProductId = $(this).attr('data-id');
		let childProductId = $(this).closest('.woocommerce-grouped-product-list-item').attr('id');
		childProductId = childProductId.replace('product-', '');

		let dateSplit = date.split('/');
		let dateInt = new Date(Date.UTC(dateSplit[2], dateSplit[0] - 1, dateSplit[1])).getTime();
		dateInt = dateInt / 1000;

		let cookieName = 'reserve_product_' + childProductId;
		let cookieOptions = {
		expires: 172800,
		path: '/'
		};
		let cookieValue = {
		date: dateInt,
		parent: parentProductId
		};
		cookieValue = JSON.stringify(cookieValue);
		$.ajax({
		url: ajaxurl,
		method: 'POST',
		dataType: 'json',
		data: {
		child_id: childProductId,
		parent_id: parentProductId,
		date_start: dateInt,
		quantity: quantity,
		action: 'betop_is_available',
        security: window.wp_data.betop_is_available
		},
		success: function (data) {
		if (data.available) {
		$('#product-' + data.child).removeClass('unavailable');
		$('#product-' + data.child).find('.unavailable-dates').remove();
		$('#product-' + data.child).find('input[name=reservation_date]').addClass('available');
		setCookie(cookieName, cookieValue, cookieOptions);
		}
		else {
		$('#product-' + data.child).addClass('unavailable');
		$('#product-' + data.child).find('.message-wrap').html('<div class="unavailable-dates head-font">' + data.text + '</div>');
		$('#product-' + data.child).find('input[name=reservation_date]').removeClass('available');
		}
		}
		});
		}
		});
		$('form.grouped_form').on('submit', function (e) {
		if ($(this).find('tr.unavailable').length > 0) {
		e.preventDefault();
		}
		});
		});
		}(jQuery));
		<?php  wp_add_inline_script( 'woocommerce-js', ob_get_clean() ); ?>
	<?php endif;

	}
else {

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<form class="cart grouped_form" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
	<table cellspacing="0" class="woocommerce-grouped-product-list group_table">
		<tbody>
		<?php
		$quantites_required      = false;
		$previous_post           = $post;
		$grouped_product_columns = apply_filters( 'woocommerce_grouped_product_columns', array(
			'quantity',
			'label',
			'price',
		), $product );

		foreach ( $grouped_products as $grouped_product_child ) {
			$post_object        = get_post( $grouped_product_child->get_id() );
			$quantites_required = $quantites_required || ( $grouped_product_child->is_purchasable() && ! $grouped_product_child->has_options() );
			$post               = $post_object; // WPCS: override ok.
			setup_postdata( $post );

			echo '<tr id="product-' . esc_attr( $grouped_product_child->get_id() ) . '" class="woocommerce-grouped-product-list-item ' . esc_attr( implode( ' ', wc_get_product_class( '', $grouped_product_child->get_id() ) ) ) . '">';

			// Output columns for each product.
			foreach ( $grouped_product_columns as $column_id ) {
				do_action( 'woocommerce_grouped_product_list_before_' . $column_id, $grouped_product_child );

				switch ( $column_id ) {
					case 'quantity':
						ob_start();

						if ( ! $grouped_product_child->is_purchasable() || $grouped_product_child->has_options() || ! $grouped_product_child->is_in_stock() ) {
							woocommerce_template_loop_add_to_cart();
						} elseif ( $grouped_product_child->is_sold_individually() ) {
							echo '<input type="checkbox" name="' . esc_attr( 'quantity[' . $grouped_product_child->get_id() . ']' ) . '" value="1" class="wc-grouped-product-add-to-cart-checkbox" />';
						} else {
							do_action( 'woocommerce_before_add_to_cart_quantity' );

							woocommerce_quantity_input( array(
								'input_name'  => 'quantity[' . $grouped_product_child->get_id() . ']',
								'input_value' => isset( $_POST['quantity'][ $grouped_product_child->get_id() ] ) ? wc_stock_amount( wc_clean( wp_unslash( $_POST['quantity'][ $grouped_product_child->get_id() ] ) ) ) : 0, // WPCS: CSRF ok, input var okay, sanitization ok.
								'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 0, $grouped_product_child ),
								'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $grouped_product_child->get_max_purchase_quantity(), $grouped_product_child ),
							) );

							do_action( 'woocommerce_after_add_to_cart_quantity' );
						}

						$value = ob_get_clean();
						break;
					case 'label':
						$value  = '<label for="product-' . esc_attr( $grouped_product_child->get_id() ) . '">';
						$value .= $grouped_product_child->is_visible() ? '<a href="' . esc_url( apply_filters( 'woocommerce_grouped_product_list_link', $grouped_product_child->get_permalink(), $grouped_product_child->get_id() ) ) . '">' . $grouped_product_child->get_name() . '</a>' : $grouped_product_child->get_name();
						$value .= '</label>';
						break;
					case 'price':
						$value = $grouped_product_child->get_price_html() . wc_get_stock_html( $grouped_product_child );
						break;
					default:
						$value = '';
						break;
				}

				echo '<td class="woocommerce-grouped-product-list-item__' . esc_attr( $column_id ) . '">' . apply_filters( 'woocommerce_grouped_product_list_column_' . $column_id, $value, $grouped_product_child ) . '</td>'; // WPCS: XSS ok.

				do_action( 'woocommerce_grouped_product_list_after_' . $column_id, $grouped_product_child );
			}

			echo '</tr>';
		}
		$post = $previous_post; // WPCS: override ok.
		setup_postdata( $post );
		?>
		</tbody>
	</table>

	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" />

	<?php if ( $quantites_required ) : ?>

		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<?php endif; ?>
</form>

<?php do_action( 'woocommerce_after_add_to_cart_form' );

}

<?php get_header(); ?>
<?php if (have_posts()): ?>
	<?php get_template_part('template-parts/archive', get_post_type()); ?>
<?php else: ?>
	<?php get_template_part('template-parts/content', 'none'); ?>
<?php endif; ?>

<?php get_footer(); ?>

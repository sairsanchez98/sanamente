<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package betop
 */
get_header();
$main_sidebar = 'col-lg-9';
$enable_sidebar = betop_get_option( 'blogsettings-fullwidth', 1 );
if( $enable_sidebar != 1 ) {
    $main_sidebar = 'col-lg-12';
}
?>
    <div class="container">
        <div class="row">
            <div class="<?php echo esc_attr( $main_sidebar ); ?> man_main_sidebar">

                <div id="primary-content" class="content-area">
                    <main id="main" class="site-main">


                        <?php if( have_posts() ) : ?>

                            <?php

                            while ( have_posts() ) :

                                the_post();

                                get_template_part( 'template-parts/content', 'search' );

                            endwhile;

                            the_posts_navigation();

                        else :

                            get_template_part( 'template-parts/content', 'none' );

                        endif;
                        ?>

                    </main>
                </div>

            </div>
            <?php if( $enable_sidebar == 1 ) { ?>
                <div class="col-lg-3 man_sidebar">
                    <?php dynamic_sidebar( 'sidebar-main' ); ?>
                </div>
            <?php } ?>

        </div>


    </div>


<?php

get_footer();
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<div id="smt-calendar-app">
    <div class="elementor-button-wrapper">
        <a href="#" @click="showIndCalendar" class="elementor-button-link elementor-button elementor-size-sm" role="button">
            <span class="elementor-button-content-wrapper">
                <span class="elementor-button-icon elementor-align-icon-right">
                    <i aria-hidden="true" class="far fa-calendar-alt"></i>
                </span>
                <span class="elementor-button-text">{{ textAction }} </span>
            </span>
        </a>
    </div>
    <div id="modalCalendar" class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            
            <div class="modal-content">
                <div class="card">
                    <div class="card-header">
                        <h1>Agenda tu cita</h1>
                    </div>
                    
                        <iframe id="iframe" src="#" frameborder="0" height="720"></iframe>
                    
                </div>
                
            </div>
        </div>
    </div>
   
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script>
    let app = new Vue({
        el: "#smt-calendar-app",
        data: {
            textAction: "Agendar",
            showCalendar: false
        },
        methods: {
            showIndCalendar(){
                this.showCalendar = !this.showCalendar;
                $("#iframe").attr("src" , window.location.origin + "/calendar-app/")
                $("#modalCalendar").modal("show");
            }
        }
    });
   
</script>
<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'my_custom_script_load' );
add_action( 'wp_enqueue_scripts', 'add_vuejs' );
add_action( 'wp_enqueue_scripts', 'add_momentjs' );
add_action( 'wp_enqueue_scripts', 'add_fullcalendarjs' );
#add_action( 'wp_enqueue_scripts', 'add_localejs' );

function my_theme_enqueue_styles() {

    $parent_style = 'parent-style';
    wp_enqueue_style( 'Font_Awesome', 'https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css' );
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

function my_custom_script_load() {
    wp_enqueue_script(
        'my-custom-script',
        get_stylesheet_directory_uri() . '/assets/js/custom-scripts.js',
        array( 'jquery' )
    );
}

function add_vuejs() {
    wp_enqueue_script(
        'add_vusjs',
        get_stylesheet_directory_uri() . '/assets/js/lib/vue.js'
    );
}

function add_mobiscrolljs() {
    wp_enqueue_script(
        'add_mobiscrolljs',
        get_stylesheet_directory_uri() . '/assets/js/lib/mobiscroll.jquery.min.js'
    );
}
function add_fullcalendarjs() {
    wp_enqueue_script(
        'add_fullcalendarjs',
        get_stylesheet_directory_uri() . '/assets/js/lib/fullcalendar.min.js'
    );
}
function add_momentjs() {
    wp_enqueue_script(
        'add_momentjs',
        get_stylesheet_directory_uri() . '/assets/js/lib/moment.min.js'
    );
}

function add_localejs() {
    wp_enqueue_script(
        'add_localejs',
        get_stylesheet_directory_uri() . '/assets/js/lib/locale-es.js'
    );
}

function calendar_professional() {
    ob_start();
    get_template_part( 'template-parts/block/calendar-individual' );
    return ob_get_clean();
}
add_shortcode('smt-calendar', 'calendar_professional');